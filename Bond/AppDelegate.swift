//
//  AppDelegate.swift
//  Bond
//
//  Created by Muhammad Salman on 23/02/2021.
//

import UIKit
import MFSDK
import Localize_Swift
import FirebaseMessaging
import UserNotifications
import Firebase
import GoogleSignIn

@main
class AppDelegate: UIResponder, UIApplicationDelegate , MessagingDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var vc = ParentViewController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: SharedManager.boldFont(), size: 10)!], for: .normal)
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        MFSettings.shared.configure(token: MY_FATOORAH_API_KEY, baseURL: MFBaseURL.live)
        
        

//        MFSettings.shared.configure(token: MY_FATOORAH_API_KEY, baseURL: MFBaseURL(rawValue: MY_FATOORAH_URL) ?? )

        // you can change color and title of nvgigation bar
        let them = MFTheme(navigationTintColor: .white, navigationBarTintColor: .lightGray, navigationTitle: PAY_NOW.localized(), cancelButtonTitle: CANCEL.localized())
        MFSettings.shared.setTheme(theme: them)
        
        if UserDefaults.standard.value(forKey: LANGUAGE) != nil
        {
            let language = UserDefaults.standard.value(forKey: LANGUAGE) as? String
            
            if language == "ar"
            {
                UserDefaults.standard.set(["ar"], forKey: APPLE_LANGUAGES)
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.setValue("ar", forKey: LANGUAGE)
                UserDefaults.standard.synchronize()
                SharedManager.setArabicPage(isArabic: true)
                Localize.setCurrentLanguage("ar")
                vc.setRTL()
            }
            else
            {
                UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
                UserDefaults.standard.synchronize()
                
                UserDefaults.standard.setValue("en", forKey: LANGUAGE)
                UserDefaults.standard.synchronize()
                SharedManager.setArabicPage(isArabic: false)
                Localize.setCurrentLanguage("en")
                vc.setLTR()
            }
        }
        else
        {
            UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue("en", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            SharedManager.setArabicPage(isArabic: false)
            Localize.setCurrentLanguage("en")
            vc.setLTR()
            
        }
        
       
        FirebaseApp.configure()
        
      //  GIDSignIn.sharedInstance().clientID = "804026633283-ntud7b34c86so907fu3pc7r0g6pr6og4.apps.googleusercontent.com"
       
        

       // GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
       // GIDSignIn.sharedInstance().delegate = self
        
        Messaging.messaging().delegate = self
       
        
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (isAllow, error) in
            
            if isAllow {
                
                
                Messaging.messaging().delegate = self
                SharedManager.getToken()
                
            }
            
        }
        
        application.registerForRemoteNotifications()
        
        
        
        
        
        // For Flag Loading in iOS 15
        UIImage(named: "", in: Bundle.FlagIcons, compatibleWith: nil)
        
        
        return true
    }
    
    
    
  
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        
        
        UserDefaults.standard.setValue(fcmToken, forKey: FCM_TOKEM)
        
        
        //        let dataDict:[String: String] = ["token": fcmToken]
        //        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any])
      -> Bool {
        
        if let paymentId = url[MFConstants.paymentId] {
            NotificationCenter.default.post(name: .applePayCheck, object: paymentId)
            return true
        }
        
        
          
          var handled: Bool

            handled = GIDSignIn.sharedInstance.handle(url)
            if handled {
              return true
            }

            // Handle other custom URL types.

            // If not handled by this app, return false.
            return false
     // return GIDSignIn.sharedInstance().handle(url)
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler(UNNotificationPresentationOptions.alert)
        
        //OnReceive Notification
        let userInfo = notification.request.content.userInfo
        for key in userInfo.keys {
            
            
             
        }
        
        completionHandler([])
        
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //OnTap Notification
        let userInfo = response.notification.request.content.userInfo
    
        print(userInfo)
        
  
        SharedManager.sharedInstance.userTapOnNotification = true
    
      
        
        if SharedManager.getUser().UserID != ""
        {
            SharedManager.sharedInstance.userTapOnNotification = false
            if let topViewController = UIApplication.topViewController()
            {
                if let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
                {
                    topViewController.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
       
       
        
        completionHandler()
    }
  
    
}

