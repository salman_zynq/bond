//
//  ParentViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import UIKit
import Localize_Swift



class ParentViewController: UIViewController {

    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
        if SharedManager.getUser().UserID != ""
        {
            SharedManager.createPusherConnection()
        }

        
    }
    
    func getUserIdFromUserDefualts() -> String
    {
        if let userID = UserDefaults.standard.value(forKey: USERDEFAULTS_USERID) {
            
            return userID as! String
        }
        else
        {
            return ""
        }
        
    }
    
    func setRootAfterLoggedIn()
    {
        let embeddedViewContrroller = self.storyboard?.instantiateViewController(withIdentifier: "CustomTabBarVC") as! CustomTabBarVC
        
        let appsDelegate = UIApplication.shared.delegate
        
        appsDelegate?.window??.rootViewController = nil
        appsDelegate?.window??.rootViewController = embeddedViewContrroller
    }
    

    func generateToken() {
        
        ApiManager.alamofirePostRequestNewToken(parameters: [:], isShowAI: true, mainView: self.view, successCallback: { (dict) in
            
            print(dict)
            
            let token = dict["token"] as? String
            
            UserDefaults.standard.set(token, forKey: USERDEFAULTS_TOKEN)
            UserDefaults.standard.removeObject(forKey: USERDEFAULTS_USERID)
            
            
            
        }) { (error) in
            
            AlertManager.showAlert(title:   ERROR.localized(), message: error)
            
        }
        
    }
    
    

    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    func logOutFromApp()
    {
        let params = ["UserID" : String(describing: SharedManager.getUser().UserID)]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + LOGOUT_API, method: .get, parameters: params, isShowAI:  true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true ,  successCallback: { (response) in
            
            
            
            
            
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogIn") as? UINavigationController
            {
                
                
                SharedManager.setUser(userObj: UserObject())
                UserDefaults.standard.removeObject(forKey: USERDEFAULTS_USERID)
                UserDefaults.standard.removeObject(forKey: USERDEFAULTS_TOKEN)
                
                
                let appsDelegate = UIApplication.shared.delegate
                
                appsDelegate?.window??.rootViewController = nil
                appsDelegate?.window??.rootViewController = vc
                
    
            }
            
            
            
            
            
            
            
            
        }) { (error , response) in
            
            
            AlertManager.showAlert(message: error)
            
            
        }
    }
    
    
    func changeLanague()
    {
        AlertManager.showAlertWithTwoCustomButtons(message: ARE_YOU_SURE_YOU_WANT_TO_CONTINUE.localized() , firstButtonTitle: ENGLISH_STRING, secondButtonTitle: ARABIC_STRING.localized(), firstButtonAlertStyle: .default, secondButtonAlertStyle: .default, firstButtonCallBack: {
            
            if SharedManager.getArabic()
            {
                 self.closeAppAlertForLanguageChange()
            }
           
            
            
        }) {
            
            if !SharedManager.getArabic()
            {
                 self.closeAppAlertForLanguageChange()
            }
           
            
        }
    }
    
    
    func closeAppAlertForLanguageChange()
    {
        AlertManager.showAlertWithTwoCustomButtons(title: "", message: APP_CLOSED_MESSAGE_WHEN_LANGUAGE_CHANGE.localized(), firstButtonTitle: NO_BUTTON.localized(), secondButtonTitle: CLOSE_APP.localized(), firstButtonAlertStyle: .default, secondButtonAlertStyle: .destructive, firstButtonCallBack: {
            
            
            
        }, secondButtonCallBack: {
            
            if SharedManager.getArabic()
            {
                self.setLanguage(isArabic: false)
                
            }
            else
            {
                self.setLanguage(isArabic: true)
                
            }
            
            exit(0)
            
        })
        
    }
    
    
    func setLanguage(isArabic:Bool)
    {
        if isArabic
        {
            UserDefaults.standard.set(["ar"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue("ar", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            SharedManager.setArabicPage(isArabic: true)
            Localize.setCurrentLanguage("ar")
        }
        else
        {
            UserDefaults.standard.set(["en"], forKey: APPLE_LANGUAGES)
            UserDefaults.standard.synchronize()
            
            UserDefaults.standard.setValue("en", forKey: LANGUAGE)
            UserDefaults.standard.synchronize()
            SharedManager.setArabicPage(isArabic: false)
            Localize.setCurrentLanguage("en")
        }
        
    }
    
    
    
    func setRTL()
    {
        
        
        
//        UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
//        UIView.appearance().semanticContentAttribute = .forceRightToLeft
//        UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        
        
        
    }
    
    
    func setLTR()
    {
        UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        
        
    }
    
    func makeAttributedLable(complete:String,CompFont:UIFont,CompColor:UIColor,sub:String,subFont:UIFont,SubColor:UIColor) -> NSAttributedString {
        
        let CompleteString = complete
        let specificString = sub
        let longestWordRange = (CompleteString as NSString).range(of: specificString)
        let attributedString = NSMutableAttributedString(string: CompleteString, attributes: [NSAttributedString.Key.font : CompFont, NSAttributedString.Key.foregroundColor : CompColor])
        attributedString.setAttributes([NSAttributedString.Key.font : subFont, NSAttributedString.Key.foregroundColor : SubColor], range: longestWordRange)
        return attributedString
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
       if let urlString = urlString {
           if let url = NSURL(string: urlString) {
               return UIApplication.shared.canOpenURL(url as URL)
           }
       }
       return false
   }

    
    func getMakePrivateText()
    {
        
        let parameters: [String: Any] = [ "PageID": "4"]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PAGE_DETAIL_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            
            print(response)
            if let dict = response["page_data"] as? NSDictionary
            {
                var title = ""
                var description = ""
                
                if SharedManager.getArabic()
                {
                    title = dict["TitleAr"] as? String ?? ""
                    description = dict["DescriptionAr"] as? String ?? ""
                    
                   
                }
                else
                {
                 
                    title = dict["Title"] as? String ?? ""
                    description = dict["Description"] as? String ?? ""
                    
                }
                
          
                AlertManager.showAlert(title: title.htmlToString, message: description.htmlToString, buttonText: CLOSE.localized())
               
               
            }
            
           
            
        } errorCallBack: { (error, response) in
            
         
            AlertManager.showAlert(message: error)
        }

    }
}
