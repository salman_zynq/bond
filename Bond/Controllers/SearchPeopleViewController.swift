//
//  SearchPeopleViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit
import IQKeyboardManager
class SearchPeopleViewController: ParentViewController {
    
    var isShowBackButton = false
    @IBOutlet weak var labelPeople: LabelBoldFont!
    
    @IBOutlet weak var containerNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textFieldSearch: CustomTextField!
    
    @IBOutlet weak var leftLogo: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var userArray = [UserObject]()
    
    @IBOutlet weak var widthOfLeftSide: NSLayoutConstraint!
    @IBOutlet weak var widthOfRightSide: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
      //  self.searchUser()
        
        if SharedManager.getArabic()
        {
            widthOfRightSide.constant = 0
        }
        else
        {
            widthOfLeftSide.constant = 0
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        
        if isShowBackButton
        {
            self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.isTranslucent = true
        }
        else
        {
            self.tabBarController?.tabBar.isHidden = false
            self.tabBarController?.tabBar.isTranslucent = false
        }
        
      
    }

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    
    func setUpViews()
    {
        labelPeople.text = PEOPLE.localized()
        textFieldSearch.placeholder = SEARCH.localized()
        if isShowBackButton
        {
            self.leftLogo.isHidden = true
            self.backButton.isHidden = false
            
        }
        else
        {
            self.backButton.isHidden = true
            self.leftLogo.isHidden = false
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonSearch(_ sender: Any)
    {
        if textFieldSearch.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return
        }
        
        self.searchUser()
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension SearchPeopleViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchPeopleTableViewCell
        let object = self.userArray[indexPath.row]
        cell.mainContainer.setCornersOfView(cornerRadus: 10)
        cell.containerImageView.makeViewRound()
        
        cell.userImageView.image = nil
        cell.userImageView.downLoadImageIntoImageView(url: object.Image, placeholder: nil)
        cell.labelFullName.text = object.FullName
        cell.labelUserName.text = "@" + object.UserName
        
        return cell
        
    }
}

extension SearchPeopleViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = self.userArray[indexPath.row]
        
        if object.IsSocialQr == 1
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SocialProfileViewController") as? SocialProfileViewController
            {
                vc.invitationID = object.InvitationID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            {
               
                vc.userID = object.UserID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
       
    }
}

extension SearchPeopleViewController
{
    func searchUser() {
        
    
        let parameters: [String: Any] = ["keyword": self.textFieldSearch.text!]
        
        self.view.endEditing(true)
     
        
    
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + SEARCH_USERS_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
          
            
            print(response)
            if let array = response["users"] as? NSArray
            {
                self.userArray = UserObject.parseMultipleUserData(array: array)
            }
           
            self.tableView.reloadData()
           

            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
     
        
    }
    
 
}

extension SearchPeopleViewController : UITextFieldDelegate
{


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.searchUser()
        
        return false
    }

}
