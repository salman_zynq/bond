//
//  CreateSocialAccountLinkViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 23/04/2021.
//

import UIKit
import MZFormSheetPresentationController
import FlagPhoneNumber
import SwiftUI

class CreateSocialAccountLinkViewController: ParentViewController {
    
    
    
    @IBOutlet weak var labelTermsAndConditions: HyperLinkLabel!
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    
    @IBOutlet weak var labelBondingQR: LabelBoldFont!
    @IBOutlet weak var labelEnterDetail: LabelBoldFont!
    
    @IBOutlet weak var buttonProceedNext: ButtonBold!
    
    @IBOutlet weak var mainContainer: UIView!
    
    @IBOutlet weak var containerLogo: UIView!
    
    @IBOutlet weak var containerCoverView: UIView!
    @IBOutlet weak var termsAndConditionCheckBox: SnapchatCheckbox!
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var textFieldUploadCoverImage: CustomTextField!
    
    @IBOutlet weak var textFieldUploadLogo: CustomTextField!
    
    
    
    @IBOutlet weak var textFieldFirstName: CustomTextField!
    
    @IBOutlet weak var textFieldLastName: CustomTextField!
    
    @IBOutlet weak var textFieldCompanyName: CustomTextField!
    
    
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    @IBOutlet weak var textFieldWhatsappNumber: NumberTextField!
    @IBOutlet weak var textFieldUrl: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlTwiter: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlFacebook: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlYoutube: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlInstagram: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlSnapChat: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlTikTok: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlLinkedIn: CustomTextField!
    
    @IBOutlet weak var textFieldAddress: CustomTextField!
    
    @IBOutlet weak var textFieldBirthday: CustomTextField!
    
    @IBOutlet weak var textViewBio: CustomTextView!
    
    @IBOutlet weak var textFieldTitle1: CustomTextField!
    
    @IBOutlet weak var textFieldDescription1: CustomTextField!
    
    @IBOutlet weak var textFieldTitle2: CustomTextField!
    
    @IBOutlet weak var textFieldDescription2: CustomTextField!
    
    @IBOutlet weak var textFieldTitle3: CustomTextField!
    
    @IBOutlet weak var textFieldDescription3: CustomTextField!
    
    
    @IBOutlet weak var containertextFieldTitle1: UIView!
    
    @IBOutlet weak var containertextFieldDescription1: UIView!
    
    @IBOutlet weak var containertextFieldTitle2: UIView!
    
    @IBOutlet weak var containertextFieldDescription2: UIView!
    
    @IBOutlet weak var containertextFieldTitle3: UIView!
    
    @IBOutlet weak var containertextFieldDescription3: UIView!
                
    
    @IBOutlet weak var textFieldAppleAppstoreLink: CustomTextField!
    @IBOutlet weak var textFieldGooglePlayStoreLink: CustomTextField!
    var isPickLogoImage = false
    var isPickCoverImage = false
    
    @IBOutlet weak var labelMakePrivate: HyperLinkLabel!
    
    @IBOutlet weak var makePrivateSnapChatBox: SnapchatCheckbox!
    
    var privatePassword = ""
    
    @IBOutlet weak var socialImageView: UIImageView!
    
    
    @IBOutlet weak var heightOfTermsAndConditionsView: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfAdditionalChargesView: NSLayoutConstraint!
    
    @IBOutlet weak var additionalChargesView: UIView!
    
    @IBOutlet weak var termsAndConditionsView: UIView!
    
    
    var isComingFromEdit = false
    var object = InvitationObject()
    
    var isValidPhone = false
    var dialingCode = ""
    
    var uploadLogoButton = UIButton()
    var uploadCoverButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlTwiter, tag: 1)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlFacebook, tag: 2)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlYoutube, tag: 3)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlInstagram, tag: 4)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlSnapChat, tag: 5)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlTikTok, tag: 6)
        self.setUpSocialMediaFields(textfield: textFieldSocialMediaUrlLinkedIn, tag: 7)
        self.setUpSocialMediaFields(textfield: textFieldTitle1, tag: 8)
        self.setUpSocialMediaFields(textfield: textFieldTitle2, tag: 9)
        self.setUpSocialMediaFields(textfield: textFieldTitle3, tag: 10)
        setUpData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        
        
    }
    
    func setUpData()
    {
        
        textFieldSocialMediaUrlTwiter.delegate = self
        textFieldSocialMediaUrlFacebook.delegate = self
        textFieldSocialMediaUrlSnapChat.delegate = self
        textFieldSocialMediaUrlInstagram.delegate = self
        textFieldSocialMediaUrlLinkedIn.delegate = self
        textFieldSocialMediaUrlTikTok.delegate = self
        textFieldSocialMediaUrlYoutube.delegate = self
        
        
        textFieldWhatsappNumber.setFlag(countryCode: FPNCountryCode.SA)
        
        textFieldWhatsappNumber.flagButtonSize = CGSize(width: 20, height: 20)
        
        if isComingFromEdit
        {
            //            self.isPickLogoImage = true
            //            self.isPickCoverImage = true
            
            if object.Logo != ""
            {
                self.logoImageView.downLoadImageIntoImageView(url: object.Logo, placeholder: nil)
                
                
                self.containerLogo.backgroundColor = .white
                self.containerLogo.alpha = 1.0
                
            }
            
            
            if object.Background != ""
            {
                self.coverImageView.backgroundColor = .white
                self.coverImageView.alpha = 1.0
                self.coverImageView.downLoadImageIntoImageView(url: object.Background, placeholder: nil)
            }
            
            
            termsAndConditionsView.isHidden = true
            additionalChargesView.isHidden = true
            heightOfAdditionalChargesView.constant = 0
            heightOfTermsAndConditionsView.constant = 0
            
            textFieldFirstName.text = object.FirstName
            textFieldLastName.text = object.LastName
            textFieldEmail.text = object.Email
            
            
            if object.WhatsaAppNumber != ""
            {
                print(object.WhatsaAppNumber)
                
                
                textFieldWhatsappNumber.set(phoneNumber: object.WhatsaAppNumber)
                
            }
            
            
            textFieldUrl.text = object.Website
            textFieldAddress.text = object.Address
            textFieldBirthday.text = object.Birthday
            textFieldCompanyName.text = object.Company
            textFieldSocialMediaUrlTwiter.text = object.TwitterLink
            textFieldSocialMediaUrlFacebook.text = object.FacebookLink
            textFieldSocialMediaUrlSnapChat.text = object.SnapchatLink
            textFieldSocialMediaUrlInstagram.text = object.InstagramLink
            textFieldSocialMediaUrlLinkedIn.text = object.LinkedinLink
            textFieldSocialMediaUrlTikTok.text = object.TiktokLink
            textFieldSocialMediaUrlYoutube.text = object.YoutubeLink
            textFieldAppleAppstoreLink.text = object.IosAppLink
            textFieldGooglePlayStoreLink.text = object.AndroidAppLink
            
            if object.Bio != ""
            {
                textViewBio.text = object.Bio
            }
            
            
            
            textFieldTitle1.text = object.Title1
            textFieldTitle2.text = object.Title2
            textFieldTitle3.text = object.Title3
            textFieldDescription1.text = object.LinkDescription1
            textFieldDescription2.text = object.LinkDescription2
            textFieldDescription3.text = object.LinkDescription3
            
            
        }
        else
        {
            let fullNameArr = SharedManager.getUser().FullName.components(separatedBy: " ")
            let firstName: String = fullNameArr[0]
            let lastName: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
            textFieldFirstName.text = firstName
            textFieldLastName.text = lastName
            textFieldEmail.text = SharedManager.getUser().Email
            
            
            
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    func setUpViews()
    {
        
        
        
        
        
        uploadLogoButton = UIButton(type: .custom)
        uploadLogoButton.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        uploadLogoButton.frame = CGRect(x: CGFloat(textFieldUploadLogo.frame.size.width - 8), y: CGFloat(5), width: CGFloat(50), height: CGFloat(25))
        uploadLogoButton.addTarget(self, action: #selector(self.deleteLogo), for: .touchUpInside)
        uploadLogoButton.setTitleColor( UIColor.gray, for: .normal)
        uploadLogoButton.setTitleColor(UIColor.DarkRedBrown(), for: .normal)
        uploadLogoButton.setTitle(REMOVE.localized(), for: .normal)
        uploadLogoButton.isHidden = true
        
        
     
        uploadCoverButton = UIButton(type: .custom)
        uploadCoverButton.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        uploadCoverButton.frame = CGRect(x: CGFloat(textFieldUploadCoverImage.frame.size.width - 8), y: CGFloat(5), width: CGFloat(50), height: CGFloat(25))
        uploadCoverButton.addTarget(self, action: #selector(self.deleteCover), for: .touchUpInside)
        uploadCoverButton.setTitleColor( UIColor.gray, for: .normal)
        uploadCoverButton.setTitleColor(UIColor.DarkRedBrown(), for: .normal)
        uploadCoverButton.setTitle(REMOVE.localized(), for: .normal)
        uploadCoverButton.isHidden = true
        
        
        if SharedManager.getArabic()
        {
           
            textFieldUploadLogo.leftView = uploadLogoButton
            textFieldUploadLogo.leftViewMode = .always
            
            textFieldUploadCoverImage.leftView = uploadCoverButton
            textFieldUploadCoverImage.leftViewMode = .always
        }
        else
        {
         
            textFieldUploadLogo.rightView = uploadLogoButton
            textFieldUploadLogo.rightViewMode = .always
            
            textFieldUploadCoverImage.rightView = uploadCoverButton
            textFieldUploadCoverImage.rightViewMode = .always
           
        }
        
        
        self.setTextFieldsCorner(textFieldView: containertextFieldTitle1)
        self.setTextFieldsCorner(textFieldView: containertextFieldTitle2)
        self.setTextFieldsCorner(textFieldView: containertextFieldTitle3)
        self.setTextFieldsCorner(textFieldView: containertextFieldDescription1)
        self.setTextFieldsCorner(textFieldView: containertextFieldDescription2)
        self.setTextFieldsCorner(textFieldView: containertextFieldDescription3)
        
        
        socialImageView.image = UIImage.init(named: "Group 3134")
        
        //     buttonProceedNext.setTitle(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR" + " - " + "Next", for: .normal)
        
        buttonProceedNext.setTitle(PROCEED_NEXT.localized(), for: .normal)
        
        labelMakePrivate.text = MAKE_PRIVATE.localized() + " (" + SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges + " " + "SAR" + " " + ADDITIONAL_CHARGES_APPLY.localized() + ")"
        
        
        termsAndConditionCheckBox.layer.cornerRadius = 3
        termsAndConditionCheckBox.configureBox()
        
        makePrivateSnapChatBox.layer.cornerRadius = 3
        makePrivateSnapChatBox.configureBox()
        
        
        
        if isComingFromEdit
        {
            labelCreateA.text =  EDIT.localized()
            if self.object.Bio == ""
            {
                textViewBio.text = BIOGRAPHY.localized()
                textViewBio.textColor = .lightGray
            }
            
        }
        else
        {
            labelCreateA.text  = CREATE_A.localized()
            textViewBio.text = BIOGRAPHY.localized()
            textViewBio.textColor = .lightGray
        }
        
        labelBondingQR.text = BONDING.localized()
        labelEnterDetail.text  = ENTER_DETAILS.localized()
        
        textFieldFirstName.placeholder = FIRST_NAME.localized()
        textFieldLastName.placeholder = LAST_NAAE.localized()
        textFieldCompanyName.placeholder = COMPANY.localized()
        
        
        
        
        textFieldUploadCoverImage.placeholder = UPLOAD_COVER.localized()
        textFieldUploadLogo.placeholder = UPLOAD_LOGO.localized()
        textFieldEmail.placeholder = EMAIL.localized()
        //textFieldWhatsappNumber.placeholder = WHATSAPP_NUMBER.localized()
        textFieldUrl.placeholder = URL_STRING.localized()
        textFieldSocialMediaUrlTwiter.placeholder = SOCIAL_MEDIA_URL_TWITTER.localized()
        textFieldSocialMediaUrlFacebook.placeholder = SOCIAL_MEDIA_URL_FACEBOOK.localized()
        textFieldSocialMediaUrlYoutube.placeholder = ENTER_VALID_YOUTUBE_LINK.localized()
        textFieldSocialMediaUrlInstagram.placeholder = SOCIAL_MEDIA_URL_INSTAGRAM.localized()
        textFieldSocialMediaUrlSnapChat.placeholder = SOCIAL_MEDIA_URL_SNAPCHAT.localized()
        textFieldSocialMediaUrlTikTok.placeholder = SOCIAL_MEDIA_URL_TIKTOK.localized()
        textFieldSocialMediaUrlLinkedIn.placeholder = SOCIAL_MEDIA_URL_LINKEDIN.localized()
        textFieldAddress.placeholder = GOOGLE_MAP_LOCATION_SHARE_LINK.localized()
        textFieldBirthday.placeholder = BIRTHDAY.localized()
        textFieldAppleAppstoreLink.placeholder = IOS_APPLICATION_LINK.localized()
        textFieldGooglePlayStoreLink.placeholder = ANDROID_APPLICATION_LINK.localized()
        
        
        
        textFieldTitle1.placeholder = TITLE.localized()
        textFieldDescription1.placeholder = ENTER_VALID_LINK.localized()
        
        textFieldTitle2.placeholder = TITLE.localized()
        textFieldDescription2.placeholder = ENTER_VALID_LINK.localized()
        
        
        textFieldTitle3.placeholder = TITLE.localized()
        textFieldDescription3.placeholder = ENTER_VALID_LINK.localized()
        
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        let highlightAttributed = [NSAttributedString.Key.foregroundColor: UIColor.GreenColor(),
                                   NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        labelTermsAndConditions.linkAttributeDefault = highlightAttributed as [AnyHashable : Any]
        labelTermsAndConditions.linkAttributeHighlight = attributes as [AnyHashable : Any]
        
        labelTermsAndConditions.attributedText = NSAttributedString(string: AGREE_TERMS_AND_CONDITIONS.localized(), attributes: attributes as [NSAttributedString.Key : Any])
        
        let handler = {(hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            //            self.getTermsAndConditions()
            
            //AlertManager.showAlert(message: COMING_SOON)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        labelTermsAndConditions.setLinksForSubstrings([TERMS_AND_CONDITIONS.localized()], withLinkHandler: handler)
        
        
        
    }
    
    func setTextFieldsCorner(textFieldView:UIView)
    {
        textFieldView.layer.cornerRadius = textFieldView.frame.size.height / 2
        textFieldView.layer.borderWidth = 1.0
        textFieldView.layer.borderColor = UIColor.lightGray.cgColor
       
    }
    
    
    @objc func deleteLogo()
    {

        self.textFieldUploadLogo.placeholder = UPLOAD_LOGO.localized()
        self.logoImageView.image = nil
        self.containerLogo.backgroundColor = .lightGray
        self.containerLogo.alpha = 0.6
        self.isPickLogoImage = false
        self.uploadLogoButton.isHidden = true
        
    }
    
    
    @objc func deleteCover()
    {

        self.textFieldUploadCoverImage.placeholder = UPLOAD_COVER.localized()
        self.coverImageView.image = nil
        
        self.isPickCoverImage = false
        self.coverImageView.backgroundColor = .lightGray
        self.coverImageView.alpha = 0.6
        self.uploadCoverButton.isHidden = true
        
    }
    
    @IBAction func makePrivate(_ sender: Any)
    {
        if makePrivateSnapChatBox.isChecked
        {
            self.showPassword()
        }
    }
    
    @IBAction func buttonPrivateText(_ sender: Any)
    {
        
        self.getMakePrivateText()
        
        
    }
    
    func showPassword()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRPrivatePasswordViewController") as? QRPrivatePasswordViewController
        {
            
            
            vc.delegate = self
            
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:300)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
    
    
    
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNext(_ sender: Any)
    {
        
        if checkAllValidaitonWorks()
        {
            
            //            Type=4,UserID,IsPrivate,PrivateCharges,TotalAmount,CreatedAt,ContactInfo,FirstName,LastName,Company,Email,QRUrl,TwitterLink,FacebookLink,YoutubeLink,InstagramLink,SnapchatLink,TiktokLink,LinkedinLink,Address,Birthday
            
            var mobileNumber = ""
            
            if isValidPhone
            {
                mobileNumber = textFieldWhatsappNumber.getFormattedPhoneNumber(format: .E164) ?? ""
            }
            
            //            do {
            //                let phoneNumber = try phoneNumberKit.parse(textFieldWhatsappNumber.text!)
            //                mobileNumber = phoneNumberKit.format(phoneNumber, toType: .e164)
            //            }
            //            catch {
            //                print("Generic parser error")
            //
            //            }
            
            
            var parameters: [String: Any] = ["Type" : "5", "FirstName": textFieldFirstName.text!, "LastName" : textFieldLastName.text! ,  "Company": textFieldCompanyName.text! ,"Email": textFieldEmail.text!,"TwitterLink": textFieldSocialMediaUrlTwiter.text!, "FacebookLink" : textFieldSocialMediaUrlFacebook.text! , "YoutubeLink": textFieldSocialMediaUrlYoutube.text! , "InstagramLink" : textFieldSocialMediaUrlInstagram.text! , "SnapchatLink" : textFieldSocialMediaUrlSnapChat.text! , "TiktokLink" : textFieldSocialMediaUrlTikTok.text!, "LinkedinLink" : textFieldSocialMediaUrlLinkedIn.text!  , "UserID" : SharedManager.getUser().UserID , "CreatedAt" : String(describing: convertDateIntoTimeStamp(date: Date()))  , "Website" :  textFieldUrl.text! , "Address" : textFieldAddress.text! , "Birthday" : textFieldBirthday.text!, "AndroidAppLink" : textFieldGooglePlayStoreLink.text! , "IosAppLink" : textFieldAppleAppstoreLink.text! , "WhatsappNo" : mobileNumber , "Title1" : textFieldTitle1.text! , "Title2" : textFieldTitle2.text! , "Title3" : textFieldTitle3.text! , "Description1" :  textFieldDescription1.text! , "Description2" :  textFieldDescription2.text!  , "Description3" :  textFieldDescription3.text!  ]
            
            if textViewBio.text != BIOGRAPHY.localized()
            {
                parameters["Bio"] = textViewBio.text!
            }
            else
            {
                parameters["Bio"] = ""
            }
            
            
            if isComingFromEdit
            {
                parameters["InvitationID"] = object.InvitationID
                
                
                
                var logoImage = UIImage()
                var backgroundImage = UIImage()
                
                var key = ""
                var backgroundKey = ""
                if isPickLogoImage
                {
                    logoImage = self.logoImageView.image!
                    key = "Logo"
                }
                else
                {
                    
                }
                
                
                if isPickCoverImage
                {
                    backgroundImage = self.coverImageView.image!
                    backgroundKey = "Background"
                }
                else
                {
                    
                }
                
                ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: logoImage, imageKey: key, backgroundImage: backgroundImage, backgroundImageKey: backgroundKey, qrImage: nil,  isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                    
                    
                    print(response)
                    
                    AlertManager.showAlertWithOneButton(message: SUCCESS.localized()) {
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    
                    
                    
                } errorCallBack: { (error, response) in
                    
                    AlertManager.showAlert(message: error)
                }
            }
            else
            {
                if makePrivateSnapChatBox.isChecked
                {
                    
                    parameters["IsPrivate"] = "1"
                    parameters["InvitationPassword"] = self.privatePassword
                }
                else
                {
                    parameters["IsPrivate"] = "0"
                }
                
                
                
                
                
                
                if isPickLogoImage
                {
                    
                    parameters["Logo"] = self.logoImageView.image!
                }
                else
                {
                    
                }
                
                
                
                
                
                
                
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivateAndPublicQRSettingViewController") as? PrivateAndPublicQRSettingViewController
                {
                    
                    if isPickCoverImage
                    {
                        vc.backGroundImage = self.coverImageView.image!
                    }
                    
                    vc.dict = parameters
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            
            
            
            
            
        }
        else
        {
            //            textFieldUploadCoverImage.attributedPlaceholder = NSAttributedString(
            //                string: UPLOAD_COVER.localized(),
            //                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            //            )
            //
            //            textFieldUploadLogo.attributedPlaceholder = NSAttributedString(
            //                string: UPLOAD_LOGO.localized(),
            //                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            //            )
            //
            textFieldFirstName.attributedPlaceholder = NSAttributedString(
                string: FIRST_NAME.localized(),
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            )
        }
        
        
        
        
        
        
    }
    
    
}


extension CreateSocialAccountLinkViewController
{
    
    
    @objc func showStartDate(sender: UIDatePicker)
    {
        textFieldBirthday.text =  convertDateIntoDateString(date: sender.date)
        
        
    }
    
    
    func checkAllValidaitonWorks() -> Bool
    {
        
        
        //        if !isPickCoverImage
        //        {
        //            AlertManager.showAlert(message:FILL_EMPTY_FIELDS.localized())
        //            
        //            return false
        //        }
        //        
        //        if !isPickLogoImage
        //        {
        //            AlertManager.showAlert(message:FILL_EMPTY_FIELDS.localized())
        //            
        //            return false
        //        }
        //        
        
        
        if textFieldFirstName.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
        
        
        if !textFieldWhatsappNumber.isTextFieldEmpty()
        {
            
            if !isValidPhone
            {
                
                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_MOBILE.localized())
                return false
            }
            
        }
        
        
        if !textFieldAddress.isTextFieldEmpty()
        {
            
            if  ((textFieldAddress.text?.hasPrefix("http")) != nil)
            {
                
                
            }
            else
            {
                AlertManager.showAlert(title: ERROR.localized(), message: GOOGLE_MAP_LOCATION_SHARE_LINK.localized())
                return false
            }
            
        }
        
        
        if !textFieldSocialMediaUrlYoutube.isTextFieldEmpty()
        {
            
            if  textFieldSocialMediaUrlYoutube.text!.hasPrefix("http")
            {
                
                
            }
            else
            {
                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_YOUTUBE_LINK.localized())
                return false
            }
            
        }
        
        //        if textFieldLastName.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        
        
        //        if textFieldCompanyName.isTextFieldEmpty()
        //        {
        //            
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //        
        
        
        
        
        
        
        
        //        if textFieldUrl.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //        if textFieldSocialMediaUrlTwiter.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //
        //        if textFieldSocialMediaUrlFacebook.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //
        //
        //
        //        if textFieldSocialMediaUrlYoutube.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //
        //        if textFieldSocialMediaUrlInstagram.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //
        //
        //
        //        if textFieldSocialMediaUrlSnapChat.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //        if textFieldSocialMediaUrlTikTok.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //        if textFieldSocialMediaUrlLinkedIn.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //        if textFieldAddress.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        //
        //
        //        if textFieldBirthday.isTextFieldEmpty()
        //        {
        //
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        
        //        if textViewBio.isTextViewEmpty()
        //        {
        //            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
        //            return false
        //        }
        
        
        
        if !textFieldTitle1.isTextFieldEmpty()
        {
            if textFieldDescription1.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldDescription1.attributedPlaceholder = NSAttributedString(
                    string: textFieldDescription1.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        if !textFieldDescription1.isTextFieldEmpty()
        {
            
            
          
                
                if  textFieldDescription1.text!.hasPrefix("http")
                {
                    
                    
                }
                else
                {
                    AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_LINK.localized())
                    
                    
                    textFieldDescription1.attributedPlaceholder = NSAttributedString(
                        string: textFieldDescription1.placeholder?.localized() ?? "",
                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                    )
                    
                    
                    return false
                }
                
            
            
            
            
            if textFieldTitle1.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldTitle1.attributedPlaceholder = NSAttributedString(
                    string: textFieldTitle1.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        
        if !textFieldTitle2.isTextFieldEmpty()
        {
            if textFieldDescription2.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldDescription2.attributedPlaceholder = NSAttributedString(
                    string: textFieldDescription2.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        if !textFieldDescription2.isTextFieldEmpty()
        {
            
            
            if  textFieldDescription2.text!.hasPrefix("http")
            {
                
                
            }
            else
            {
                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_LINK.localized())
                
                
                textFieldDescription2.attributedPlaceholder = NSAttributedString(
                    string: textFieldDescription2.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                
                return false
            }
            
            
            if textFieldTitle2.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldTitle2.attributedPlaceholder = NSAttributedString(
                    string: textFieldTitle2.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        
        
        if !textFieldTitle3.isTextFieldEmpty()
        {
            if textFieldDescription3.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldDescription3.attributedPlaceholder = NSAttributedString(
                    string: textFieldDescription3.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        if !textFieldDescription3.isTextFieldEmpty()
        {
            
            if  textFieldDescription3.text!.hasPrefix("http")
            {
                
                
            }
            else
            {
                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_LINK.localized())
                
                
                textFieldDescription3.attributedPlaceholder = NSAttributedString(
                    string: textFieldDescription3.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                
                return false
            }
            
            
            if textFieldTitle3.isTextFieldEmpty()
            {
                AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
                
                textFieldTitle3.attributedPlaceholder = NSAttributedString(
                    string: textFieldTitle3.placeholder?.localized() ?? "",
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
                )
                
                return false
            }
            
        }
        
        
        if !isComingFromEdit
        {
            //            
            //            if !termsAndConditionCheckBox.isChecked
            //            {
            //                AlertManager.showAlert(title: ERROR.localized(), message: ACCEPT_TERMS_AND_CONDITIONS.localized())
            //                return false
            //            }
        }
        
        if makePrivateSnapChatBox.isChecked
        {
            if privatePassword == ""
            {
                AlertManager.showAlert(title: ERROR.localized(), message: "Set Password".localized())
                return false
                
            }
        }
        
        
        
        return true
        
    }
    
    
}


extension CreateSocialAccountLinkViewController : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.isEqual(textFieldUploadLogo)
        {
            CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                
                
                self.containerLogo.backgroundColor = .white
                self.containerLogo.alpha = 1.0
                self.isPickLogoImage = true
                self.logoImageView.image = image
                self.uploadLogoButton.isHidden = false
                
                self.textFieldUploadLogo.attributedPlaceholder = NSAttributedString(
                    string: UPLOADED.localized(),
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.GreenColor()]
                )
                
            }
            
            return false
        }
        else  if textField.isEqual(textFieldUploadCoverImage)
        {
            CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                
                
                self.isPickCoverImage = true
                self.coverImageView.image = image
                self.coverImageView.backgroundColor = .white
                self.coverImageView.alpha = 1.0
                self.uploadCoverButton.isHidden = false
                
                self.textFieldUploadCoverImage.attributedPlaceholder = NSAttributedString(
                    string: UPLOADED.localized(),
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.GreenColor()]
                )
            }
            
            return false
        }
        //        else  if textField.isEqual(textFieldBirthday)
        //        {
        //
        //
        //
        //            return false
        //        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        
        if textField.isEqual(textFieldBirthday)
        {
            
            let datePickerView = UIDatePicker()
            datePickerView.datePickerMode = UIDatePicker.Mode.date
            
            if #available(iOS 13.4, *) {
                datePickerView.preferredDatePickerStyle = UIDatePickerStyle.wheels
            } else {
                // Fallback on earlier versions
            }
            
            datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -15, to: Date())!
            textFieldBirthday.inputView = datePickerView
            
            datePickerView.addTarget(self, action: #selector(self.showStartDate), for: UIControl.Event.valueChanged)
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        //For mobile numer validation
        if textField == textFieldSocialMediaUrlTwiter || textField == textFieldSocialMediaUrlFacebook || textField == textFieldSocialMediaUrlYoutube || textField == textFieldSocialMediaUrlInstagram || textField == textFieldSocialMediaUrlSnapChat || textField == textFieldSocialMediaUrlTikTok || textField == textFieldSocialMediaUrlLinkedIn   {
            
            if string == "@"
            {
                return false
            }
            //           
            //            let allowedCharacters = CharacterSet(charactersIn:"@")//Here change this characters based on your requirement
            //            let characterSet = CharacterSet(charactersIn: string)
            //            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    
    
    
}


extension CreateSocialAccountLinkViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "" || textView.text == BIOGRAPHY.localized()
        {
            textView.text = ""
            textView.textColor = .black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = BIOGRAPHY.localized()
            textView.textColor = .lightGray
        }
    }
    
}


extension CreateSocialAccountLinkViewController : PRIVATEPASSWORD
{
    
    func passwordSet(password: String) {
        
        self.privatePassword = password
    }
    
}


extension CreateSocialAccountLinkViewController : FPNTextFieldDelegate
{
    func fpnDisplayCountryList() {
        
    }
    
    
    
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        dialingCode = dialCode
        //  textFieldWhatsappNumber.text = ""
        
        
        
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        isValidPhone = isValid
        
    }
}

extension CreateSocialAccountLinkViewController
{
    func setUpSocialMediaFields(textfield : CustomTextField , tag :  Int)
    {
        
        let button = UIButton(type: .detailDisclosure)
        
       
            button.frame = CGRect(x: CGFloat(textfield.frame.size.width - 8), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        
      
        button.addTarget(self, action: #selector(self.clickOnExcalamationMark), for: .touchUpInside)
        
        
        button.tag = tag
        if #available(iOS 13.0, *) {
            button.tintColor = .systemGray3
        } else {
            button.tintColor = .lightGray
        }
        button.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        
        
        if SharedManager.getArabic()
        {
            
            textfield.leftView = button
            textfield.leftViewMode = .always
        }
        else
        {
            
            textfield.rightView = button
            textfield.rightViewMode = .always
        }
        button.setTitleColor( UIColor.gray, for: .normal)
        button.setTitleColor(UIColor.GreenColor(), for: .normal)
        
        
        
    }
    
    @objc func clickOnExcalamationMark(sender:UIButton)
    {
        print(sender.tag)
        
        if sender.tag == 1
        {
            self.getSocialMediaText(type: "Twitter")
        }
        else if sender.tag == 2
        {
            self.getSocialMediaText(type: "Facebook")
        }
        else if sender.tag == 3
        {
            self.getSocialMediaText(type: "Youtube")
        }
        else if sender.tag == 4
        {
            self.getSocialMediaText(type: "Instagram")
        }
        else if sender.tag == 5
        {
            self.getSocialMediaText(type: "Snapchat")
        }
        else if sender.tag == 6
        {
            self.getSocialMediaText(type: "Tiktok")
        }
        else if sender.tag == 7
        {
            self.getSocialMediaText(type: "Linkedin")
        }
        else if sender.tag == 8
        {
            self.getSocialMediaText(type: "Description1")
        }
        else if sender.tag == 9
        {
            self.getSocialMediaText(type: "Description2")
        }
        else if sender.tag == 10
        {
            self.getSocialMediaText(type: "Description3")
        }
    }
    
    
    func getSocialMediaText(type: String) {
        
        
        let parameters: [String: Any] = ["Type": type]
        
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_SOCIAL_MEDIA_PLACEHOLDER, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            
            if let dict = response["data"] as? NSDictionary
            {
                var descriptionText = ""
                if SharedManager.getArabic()
                {
                    descriptionText = dict["DescriptionAr"] as? String ?? ""
                }
                else
                {
                    descriptionText = dict["Description"] as? String ?? ""
                }
                
                AlertManager.showAlert(title: "", message: descriptionText, buttonText: CLOSE.localized())
            }
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
        
    }
    
}


