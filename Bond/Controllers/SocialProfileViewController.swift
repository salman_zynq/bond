//
//  SocialProfileViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 08/05/2021.
//

import UIKit
import SafariServices
class SocialProfileViewController: ParentViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var labelScreenTitle: LabelBoldFont!
    var userObject = UserObject()
    var invitationObject = InvitationObject()
    var invitationID = ""
    var qrImageView = UIImageView()
    
    var unreadMessageCount = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        labelScreenTitle.text = PROFILE.localized()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        
        getUserData(invitationID: invitationID)
        
        
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonChatOrEdit(_ sender: UIButton)
    {
        
        if  sender.tag == 12 || sender.tag == 13 || sender.tag == 14 || sender.tag == 15 || sender.tag == 16 || sender.tag == 17 || sender.tag == 18 || sender.tag == 20 || sender.tag == 21 || sender.tag == 22 || sender.tag == 23 || sender.tag == 24
        {
            var urlString = self.invitationObject.TwitterLink
            
            if sender.tag == 13
            {
                urlString = self.invitationObject.FacebookLink
            }
            else if sender.tag == 14
            {
                urlString = self.invitationObject.InstagramLink
            }
            else if sender.tag == 15
            {
                urlString = self.invitationObject.TiktokLink
            }
            else if sender.tag == 16
            {
                urlString = self.invitationObject.LinkedinLink
            }
            else if sender.tag == 17
            {
                urlString = self.invitationObject.SnapchatLink
            }
            else if sender.tag == 18
            {
                urlString = self.invitationObject.YoutubeLink
            }
            else if sender.tag == 20
            {
                urlString = self.invitationObject.AndroidAppLink
            }
            else if sender.tag == 21
            {
                urlString = self.invitationObject.IosAppLink
            }
            else if sender.tag == 22
            {
                urlString = self.invitationObject.LinkDescription1
            }
            else if sender.tag == 23
            {
                urlString = self.invitationObject.LinkDescription2
            }
            else if sender.tag == 24
            {
                urlString = self.invitationObject.LinkDescription3
            }
            
            
            guard let url = URL(string: urlString) else {
                return //be safe
            }
            
            if verifyUrl(urlString: urlString)
            {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            else
            {
                var finalUrl = ""
                if sender.tag == 12
                {
                    finalUrl = "https://twitter.com/" + self.invitationObject.TwitterLink
                    
                }
                else if sender.tag == 13
                {
                    finalUrl = "https://www.facebook.com/" + self.invitationObject.FacebookLink
                }
                else if sender.tag == 14
                {
                    finalUrl = "https://www.instagram.com/" + self.invitationObject.InstagramLink
                }
                else if sender.tag == 15
                {
                    finalUrl = "http://www.tiktok.com/" + self.invitationObject.TiktokLink
                }
                else if sender.tag == 16
                {
                    finalUrl = "https://www.linkedin.com/in/" + self.invitationObject.LinkedinLink
                }
                else if sender.tag == 17
                {
                    finalUrl = "https://www.snapchat.com/add/" + self.invitationObject.SnapchatLink
                }
                else if sender.tag == 18
                {
                
                    //finalUrl = "https://www.youtube.com/user/" + self.invitationObject.YoutubeLink
                    finalUrl =  self.invitationObject.YoutubeLink
                }
                else
                {
                    AlertManager.showAlert(message: INVALID_USERNAME_OR_URL.localized())
                    return
                }
                
                guard let url = URL(string: finalUrl) else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            
            
        }
        else if sender.tag == 19
        {
            // Open Whatsapp application
            let phoneNumber =  self.invitationObject.WhatsaAppNumber // you need to change this number
            let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL)
                }
            } else {
                
                let appURL = URL(string: "https://wa.me/\(phoneNumber)")!
                if UIApplication.shared.canOpenURL(appURL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(appURL)
                    }
                }
            }
        }
        else if sender.tag == 11
        {
            if self.invitationObject.Address.hasPrefix("http")
            {
                
                let svc = SFSafariViewController(url: URL(string: self.invitationObject.Address)!)
                present(svc, animated: true, completion: nil)
            }
        }
        else if sender.tag == 8
        {
            
            
            
            var finalUrl = self.invitationObject.Website
            
            if self.invitationObject.Website.hasPrefix("https://") || self.invitationObject.Website.hasPrefix("http://")
            {
                
            }
            else
            {
                finalUrl = "http://" +  finalUrl
            }
            
            guard let url = URL(string: finalUrl) else {
                AlertManager.showAlert(message: INVALID_URL.localized())
                return //be safe
            }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
        else if sender.tag == 5
        {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPeopleViewController") as? SearchPeopleViewController
            {
                
                vc.isShowBackButton = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            print("People view controller")
        }
        else if sender.tag == 6
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatRoomsViewController") as? ChatRoomsViewController
            {
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            print("Click on chat rooms")
        }
        else
        {
            print(sender.tag)
            if  self.invitationObject.UserID == SharedManager.getUser().UserID
            {
                
                
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateSocialAccountLinkViewController") as? CreateSocialAccountLinkViewController
                {
                    vc.object = invitationObject
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            else
            {
                if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                    
                    vc.receiverID = self.userObject.UserID
                    vc.senderUserName = self.userObject.FullName
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        
    }
    @IBAction func buttonFollowers(_ sender: Any)
    {
        if  self.invitationObject.UserID == SharedManager.getUser().UserID
        {
            
            
        }
        else
        {
            if self.userObject.IsPrivate == "1"
            {
                return
            }
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowerAndFollowingViewController") as? FollowerAndFollowingViewController
        {
            vc.userID = self.userObject.UserID
            vc.isFollower = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonFollowing(_ sender: Any)
    {
        if  self.invitationObject.UserID == SharedManager.getUser().UserID
        {
            
            
        }
        else
        {
            if self.userObject.IsPrivate == "1"
            {
                return
            }
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowerAndFollowingViewController") as? FollowerAndFollowingViewController
        {
            vc.userID = self.userObject.UserID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonFollowOrUnFollow(_ sender: UIButton)
    {
        if sender.tag == 5
        {
            print("Goto Home Screen")
            
            ActionSheetManager.showActionSheetForDowloadQRandCopyLink(title: "", message: SELECT_AN_OPTION.localized()) {
                
                //Download QR
                
                
                SharedManager.showHUD()
                
                print(IMAGE_BASE_URL + self.invitationObject.QrImage)
                self.qrImageView.downLoadQRImageAndReturnResult(url: IMAGE_BASE_URL + self.invitationObject.QrImage, placeholder: nil) { (image) in
                    
                    
                    SharedManager.hideHud()
                    self.writeImage(image: image)
                    
                } errorResult: { (error) in
                    
                    SharedManager.hideHud()
                    AlertManager.showAlert(message: error)
                }
                
                
            } copyLinkCallBack: {
                //Link Copy
                
                UIPasteboard.general.string = IMAGE_BASE_URL + "index/profile/" + self.invitationObject.InvitationID
                AlertManager.showAlert(message: COPIED.localized())
            }
            
            
            
            //            self.tabBarController?.selectedIndex = 0
            //            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.followAndUnfollow()
        }
        
    }
    
    
    func followAndUnfollow()
    {
        
        var parameters = [String : Any]()
        
        parameters["UserID"] = SharedManager.getUser().UserID
        parameters["Following"] = self.userObject.UserID
        
        if self.userObject.IsFollowing == 1
        {
            parameters["Follow"] = "0"
        }
        
        
        print(parameters)
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + FOLLOW_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.userObject = UserObject.parseData(userDictionary: response)
            
            self.tableView.reloadData()
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
    }
    
    
    
    func writeImage(image: UIImage) {
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.finishWriteImage), nil)
    }
    
    @objc private func finishWriteImage(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if (error != nil) {
            // Something wrong happened.
            print("error occurred: \(String(describing: error))")
            AlertManager.showAlert(message: String(describing: error))
        } else {
            // Everything is alright.
            print("saved success!")
            AlertManager.showAlert(message: QR_SUCCESSFULLY_SAVED.localized())
        }
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        guard let url = URL(string: self.invitationObject.Website) else {
            
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    @IBAction func buttonLocation(_ sender: Any)
    {
        if self.invitationObject.Address.hasPrefix("http")
        {
            
            let svc = SFSafariViewController(url: URL(string: self.invitationObject.Address)!)
            present(svc, animated: true, completion: nil)
        }
    }
    
    
}

extension SocialProfileViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if invitationObject.UserID == ""
        {
            return 0
        }
        return 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! ProfileTableViewCell
            
            
            cell.topContainer.clipsToBounds = true
            cell.topContainer.layer.cornerRadius = 20
            DispatchQueue.main.async {
                cell.topContainer.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            }
            
            
            cell.containerImageView.makeViewRound()
            
            cell.labelFullName.text = self.invitationObject.FullName
            cell.labelUserName.text = "@" + self.invitationObject.UserName
            
            cell.userImageView.downLoadImageIntoImageView(url: self.invitationObject.Logo, placeholder: nil)
            cell.coverImage.downLoadImageIntoImageView(url: self.invitationObject.Background, placeholder: nil)
            return cell
            
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            
            cell.labelBio.isHidden = false
            cell.viewButtonLocation.isHidden = true
            cell.labelBio.text = self.invitationObject.Bio
            cell.labelAboutHeading.text = BIO.localized()
            
            
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            cell.labelBio.isHidden = false
            cell.viewButtonLocation.isHidden = true
            cell.labelBio.text = self.invitationObject.Birthday
            cell.labelAboutHeading.text = BIRTHDAY.localized()
            
            return cell
        }
  
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            
            cell.labelBio.isHidden = false
            cell.viewButtonLocation.isHidden = true
            
            cell.labelBio.text = self.invitationObject.Company
            cell.labelAboutHeading.text = COMPANY.localized()
            
            
            return cell
        }
        
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ProfileTableViewCell
            
            cell.buttonChatOrEdit.tag = indexPath.row
            cell.buttonFollow.tag = indexPath.row
            
            cell.buttonChatOrEdit.isHidden = false
            cell.viewChatorEdit.isHidden = false
            cell.instagramBackgroundImageView.isHidden = true
            
            
            cell.viewChatorEdit.backgroundColor = .DARKGREEN()
            cell.viewFollow.backgroundColor = .DARKGREEN()
            
            if  invitationObject.UserID == SharedManager.getUser().UserID
            {
                cell.viewFollow.isHidden = true
                cell.buttonChatOrEdit.setTitle(EDIT_MY_INFORMATION.localized(), for: .normal)
            }
            else
            {
                if self.userObject.IsFollowing == 1
                {
                    cell.buttonFollow.setTitle(UNFOLLOW.localized(), for: .normal)
//                    DispatchQueue.main.async {
                        cell.viewFollow.backgroundColor = .white
                        cell.viewFollow.layer.borderColor = UIColor.DARKGREEN().cgColor
                        cell.viewFollow.layer.borderWidth = 1.0
                        cell.buttonFollow.setTitleColor(UIColor.GreenColor(), for: .normal)
                    //}
                }
                else
                {
                    cell.buttonFollow.setTitle(FOLLOW.localized(), for: .normal)
                  //  DispatchQueue.main.async {
                        cell.viewFollow.backgroundColor = .DARKGREEN()
                        cell.buttonFollow.setTitleColor(.white, for: .normal)
                  //  }
                }
                
                cell.viewFollow.isHidden = false
                
                cell.buttonChatOrEdit.setTitle(MESSAGE.localized(), for: .normal)
                
              
                
            }
            return cell
        }
        else if indexPath.row == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ProfileTableViewCell
            
            cell.buttonChatOrEdit.isHidden = false
            cell.viewChatorEdit.isHidden = false
            cell.viewFollow.isHidden = false
            cell.instagramBackgroundImageView.isHidden = true
            
            cell.buttonChatOrEdit.tag = indexPath.row
            cell.buttonFollow.tag = indexPath.row
            
            cell.buttonFollow.setTitle(GET_QR.localized(), for: .normal)
            cell.buttonChatOrEdit.setTitle(PEOPLE.localized(), for: .normal)
            
            cell.bottonContainer.clipsToBounds = true
            cell.bottonContainer.layer.cornerRadius = 0
            
          
            cell.viewChatorEdit.backgroundColor = .DARKGREEN()
            cell.viewFollow.backgroundColor = .DARKGREEN()
            
            return cell
        }
        else if indexPath.row == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ProfileTableViewCell
            
            cell.buttonChatOrEdit.isHidden = false
            cell.viewChatorEdit.isHidden = false
            cell.instagramBackgroundImageView.isHidden = true
            
            cell.buttonChatOrEdit.tag = indexPath.row
            cell.buttonFollow.tag = indexPath.row
            
            cell.viewFollow.isHidden = true
            
            if unreadMessageCount == "0" || unreadMessageCount == ""
            {
                cell.buttonChatOrEdit.setTitle(CHATS.localized(), for: .normal)
            }
            else
            {
                cell.buttonChatOrEdit.setTitle(CHATS.localized() + " (" + unreadMessageCount + ")", for: .normal)
            }
            
            
            cell.bottonContainer.clipsToBounds = true
            cell.bottonContainer.layer.cornerRadius = 0
            
            cell.viewChatorEdit.backgroundColor = .DARKGREEN()
            
            return cell
        }
        
        else if indexPath.row == 7
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "followCell", for: indexPath) as! ProfileTableViewCell
            
            
            
            cell.labelFollowers.text = self.userObject.TotalFollower
            cell.labelFollowings.text = self.userObject.TotalFollowing
            cell.labelFollowingHeading.text = FOLLOWING.localized()
            cell.labelFollowersHeading.text = FOLLOWERS.localized()
            
            return cell
        }
        
        else if indexPath.row == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ProfileTableViewCell
            
            
            
            cell.buttonChatOrEdit.tag = indexPath.row
            cell.buttonFollow.tag = indexPath.row
            
            cell.buttonChatOrEdit.isHidden = false
            cell.viewChatorEdit.isHidden = false
            cell.viewFollow.isHidden = true
            cell.instagramBackgroundImageView.isHidden = true
           // DispatchQueue.main.async {
                cell.viewChatorEdit.backgroundColor = .DARKGREEN()
           // }
            cell.buttonChatOrEdit.setTitle(VISIT_WEBSITE.localized(), for: .normal)
            
            cell.bottonContainer.clipsToBounds = true
            cell.bottonContainer.layer.cornerRadius = 0
            
            return cell
        }
        
        else if indexPath.row == 9
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            cell.labelBio.isHidden = false
            cell.viewButtonLocation.isHidden = true
            cell.labelAboutHeading.isHidden = true
            
            cell.labelBio.text = "_________________________"
            
            
            return cell
            
        }
        
        else if indexPath.row == 10
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            cell.labelBio.isHidden = false
            cell.viewButtonLocation.isHidden = true
            cell.labelAboutHeading.isHidden = true
            
            cell.labelBio.text = SOCIAL_ACCOUNTS.localized()
            
   
            
            return cell
        }
        
        
        else
        {
            
            
            
            if indexPath.row == 24
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "lastSocialMediaButtonCell", for: indexPath) as! ProfileTableViewCell
                
                
                
                cell.buttonChatOrEdit.setTitleColor(.white, for: .normal)
                cell.bottonContainer.clipsToBounds = true
                cell.buttonChatOrEdit.tag = indexPath.row
                cell.buttonChatOrEdit.isHidden = false
                cell.viewChatorEdit.isHidden = false
                cell.instagramBackgroundImageView.isHidden = true
                
         
                cell.buttonChatOrEdit.setTitle(invitationObject.Title3, for: .normal)
               
                    
                cell.viewChatorEdit.backgroundColor = .DARKGREEN()
            
                
                    
                    
                cell.bottonContainer.clipsToBounds = true
                cell.bottonContainer.layer.cornerRadius = 20
           
                DispatchQueue.main.async
                {
                    cell.bottonContainer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                    
                }
                
             
                
                if invitationObject.Title3 == ""
                {
                    cell.buttonChatOrEdit.isHidden = true
                    cell.viewChatorEdit.isHidden = true
                }
                return cell
                
            }
            
        
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "socialMediaButtonCell", for: indexPath) as! ProfileTableViewCell
            
            
            cell.buttonChatOrEdit.setTitleColor(.white, for: .normal)
            cell.bottonContainer.clipsToBounds = true
            cell.buttonChatOrEdit.tag = indexPath.row
            cell.buttonChatOrEdit.isHidden = false
            cell.viewChatorEdit.isHidden = false
            cell.instagramBackgroundImageView.isHidden = true
            
            if indexPath.row == 11
            {
                
                cell.buttonChatOrEdit.setTitle(VIEW_LOCATION_ON_MAP.localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor =  UIColor.init(red: 50/255.0, green: 116.0/255.0, blue: 224.0/255.0, alpha: 1.0)
             
            }
            
            else if indexPath.row == 12
            {
                
                cell.buttonChatOrEdit.setTitle(FOLLOW_ON_TWITTER.localized(), for: .normal)
               
//                DispatchQueue.main.async
//                {
                    cell.viewChatorEdit.backgroundColor = .clear
                    cell.viewChatorEdit.backgroundColor = UIColor.init(red: 72/255.0, green: 167/255.0, blue: 230/255.0, alpha: 1.0)
               // }
            }
            else if indexPath.row == 13
            {
                cell.buttonChatOrEdit.setTitle(FOLLOW_ON_FACEBOOK.localized(), for: .normal)
                
//                DispatchQueue.main.async
//                {
              
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = UIColor.init(red: 63/255.0, green: 88/255.0, blue: 147/255.0, alpha: 1.0)
               // }
            }
            else if indexPath.row == 14
            {
                cell.buttonChatOrEdit.setTitle(FOLLOW_ON_INSTAGRAM.localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.instagramBackgroundImageView.isHidden = false
                cell.instagramBackgroundImageView.image = UIImage.init(named: "instagrambackground")
                cell.instagramBackgroundImageView.contentMode = .scaleToFill
            
                
              
            }
            else if indexPath.row == 15
            {
                cell.buttonChatOrEdit.setTitle(FOLLOW_ON_TIKTOK.localized(), for: .normal)
               // DispatchQueue.main.async {
                    cell.viewChatorEdit.backgroundColor = .clear
                    cell.viewChatorEdit.backgroundColor = UIColor.init(red: 1.0/255.0, green: 1.0/255.0, blue: 1.0/255.0, alpha: 1.0)
                //}
            }
            else if indexPath.row == 16
            {
                cell.buttonChatOrEdit.setTitle(CONNECT_ON_LINKEDIN.localized(), for: .normal)
//                DispatchQueue.main.async {
                    cell.viewChatorEdit.backgroundColor = .clear
                    cell.viewChatorEdit.backgroundColor = UIColor.init(red: 51/255.0, green: 117/255.0, blue: 165/255.0, alpha: 1.0)
                //}
            }
            else if indexPath.row == 17
            {
                cell.buttonChatOrEdit.setTitle(FOLLOW_ON_SNAPCHAT.localized(), for: .normal)
//                DispatchQueue.main.async {
                    
                    cell.viewChatorEdit.backgroundColor = .clear
                    cell.buttonChatOrEdit.setTitleColor(.black, for: .normal)
                    cell.viewChatorEdit.backgroundColor = UIColor.init(red: 255.0/255.0, green: 252/255.0, blue: 0.0/255.0, alpha: 1.0)
               // }
            }
            else if indexPath.row == 18
            {
                
                cell.buttonChatOrEdit.setTitle(SUBSCRIBE_ON_YOUTUBE.localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = UIColor.init(red: 233/255.0, green: 50/255.0, blue: 35/255.0, alpha: 1.0)
                    
            }
            else if indexPath.row == 19
            {
                
                cell.buttonChatOrEdit.setTitle(CONTACT_ON_WHATSAPP.localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = UIColor.init(red: 103/255.0, green: 206/255.0, blue: 92/255.0, alpha: 1.0)
     
            }
            else if indexPath.row == 20
            {
                
                cell.buttonChatOrEdit.setTitle("".localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = .black
                cell.instagramBackgroundImageView.isHidden = false
                cell.instagramBackgroundImageView.image = UIImage.init(named: "android")
                cell.instagramBackgroundImageView.contentMode = .scaleAspectFit
     
            }
            else if indexPath.row == 21
            {
                
                cell.buttonChatOrEdit.setTitle("".localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = .black
                cell.instagramBackgroundImageView.isHidden = false
                cell.instagramBackgroundImageView.image = UIImage.init(named: "apple")
                cell.instagramBackgroundImageView.contentMode = .scaleAspectFit
     
            }
            else if indexPath.row == 22
            {
                
                cell.buttonChatOrEdit.setTitle(invitationObject.Title1, for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = .DARKGREEN()
     
            }
            
            else if indexPath.row == 23
            {
                cell.buttonChatOrEdit.setTitle(invitationObject.Title2.localized(), for: .normal)
                cell.viewChatorEdit.backgroundColor = .clear
                cell.viewChatorEdit.backgroundColor = .DARKGREEN()
                
               
     
            }
            
            
            return cell
            
           
            
           
        }
    }
}

extension SocialProfileViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 24
        {
            return 100
        }
        else if indexPath.row == 1
        {
            
            if self.invitationObject.Bio == ""
            {
                return 0
            }
            
        }
        
        else if indexPath.row == 2
        {
            
            if self.invitationObject.Birthday == ""
            {
                return 0
            }
            
        }
    
        else if indexPath.row == 3
        {
            
            if self.invitationObject.Company == ""
            {
                return 0
            }
            
        }
        
        else if indexPath.row == 5
        {
            
            if self.invitationObject.UserID != SharedManager.getUser().UserID
            {
                return 0
            }
            
        }
        
        else if indexPath.row == 6
        {
            
            if self.invitationObject.UserID != SharedManager.getUser().UserID
            {
                return 0
            }
            
        }
        
        
        else if indexPath.row == 7
        {
            return 120
        }
        
        else if indexPath.row == 8
        {
            if invitationObject.Website == ""
            {
                return 0
            }
        }
        
        
        else if indexPath.row == 9
        {
            if invitationObject.Website == ""
            {
                return 0
            }
            return 50
        }
        
        else if indexPath.row == 10
        {
            if invitationObject.TwitterLink == "" && invitationObject.FacebookLink == "" && invitationObject.InstagramLink == "" && invitationObject.TiktokLink == "" && invitationObject.LinkedinLink == "" && invitationObject.SnapchatLink == "" && invitationObject.YoutubeLink == "" && invitationObject.AndroidAppLink == "" && invitationObject.IosAppLink == "" && invitationObject.Address == "" && invitationObject.Title1 == "" && invitationObject.Title2 == "" && invitationObject.Title3 == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 11
        {
            if invitationObject.Address == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 12
        {
            if invitationObject.TwitterLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 13
        {
            if invitationObject.FacebookLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 14
        {
            if invitationObject.InstagramLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 15
        {
            if invitationObject.TiktokLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 16
        {
            if invitationObject.LinkedinLink == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 17
        {
            if invitationObject.SnapchatLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 18
        {
            if invitationObject.YoutubeLink == ""
            {
                return 0
            }
        }
        else if indexPath.row == 19
        {
            if invitationObject.WhatsaAppNumber == ""
            {
                return 0
            }
        }
        else if indexPath.row == 20
        {
            if invitationObject.AndroidAppLink == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 21
        {
            if invitationObject.IosAppLink == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 22
        {
            if invitationObject.Title1 == ""
            {
                return 0
            }
        }
        
        else if indexPath.row == 23
        {
            if invitationObject.Title2 == ""
            {
                return 0
            }
        }
        
        
        return UITableView.automaticDimension
    }
}



extension SocialProfileViewController
{
    func getUserData(invitationID: String) {
        
        
        var parameters = [String: Any]()
        // parameters["UserID"] = SharedManager.getUser().UserID
        parameters["InvitationID"] = invitationID
        
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_INVITATION, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            let array = InvitationObject.parseMultipleData(userDictionary: response)
            if array.count > 0
            {
                self.invitationObject = array.first ?? InvitationObject()
                
                
            }
            
            
            
            self.getProfileData(userID: self.invitationObject.UserID)
            //  self.tableView.reloadData()
            
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
        
    }
    
    
    
    
    func getProfileData(userID: String) {
        
        
        var parameters = [String: Any]()
        
        if userID == "" || userID == SharedManager.getUser().UserID
        {
            parameters["UserID"] = SharedManager.getUser().UserID
        }
        else
        {
            parameters["UserID"] = SharedManager.getUser().UserID
            parameters["OtherUserID"] = userID
        }
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_USER_DETAILS, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            self.userObject = UserObject.parseData(userDictionary: response)
            
            
            self.getUnReadMessagesCount(userID: self.userObject.UserID)
            self.tableView.reloadData()
            
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
        
    }
    
    
    func getUnReadMessagesCount(userID: String)
    {
        
        var parameters = [String: Any]()
        
        if userID == SharedManager.getUser().UserID
        {
            parameters["UserID"] = SharedManager.getUser().UserID
        }
        else
        {
            return
        }
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + CHECK_IF_UNREAD_MESSAGE, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response["UnreadMessageCount"])
           
            if let unreadMessageCount = response["UnreadMessageCount"] as? Int
            {
                self.unreadMessageCount = String(describing: unreadMessageCount)
            }
            self.tableView.reloadData()
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
    }

    
    
}



