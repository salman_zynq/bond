//
//  CreateSMSViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/04/2021.
//

import UIKit

class CreateSMSViewController: ParentViewController {
    
    @IBOutlet weak var termsAndConditionsView: UIView!
    @IBOutlet weak var labelEnterDetail: LabelBoldFont!
    @IBOutlet weak var labelUrlQR: LabelBoldFont!
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    @IBOutlet weak var smsImageView: UIImageView!
    @IBOutlet weak var buttonProceedNext: ButtonBold!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var textFieldMobileNumber: CustomTextField!
    @IBOutlet weak var termsAndConditionCheckBox: SnapchatCheckbox!
    @IBOutlet weak var labelTermsAndConditions: HyperLinkLabel!
    
    var isComingFromEdit = false
    var object = InvitationObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        
        if isComingFromEdit
        {
            self.textFieldMobileNumber.text = object.Website
            termsAndConditionsView.isHidden = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    func setUpViews()
    {
        smsImageView.image = UIImage.init(named: "Group 3150")
        
//        buttonProceedNext.setTitle(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR" + " - " + "Next", for: .normal)
        
        if isComingFromEdit
        {
            labelCreateA.text =  EDIT_A.localized()
        }
        else
        {
            labelCreateA.text  = CREATE_A.localized()
        }
      
        labelUrlQR.text = URL_STRING.localized()
        labelEnterDetail.text = ENTER_DETAILS.localized()
        textFieldMobileNumber.placeholder = URL_STRING.localized()
        
       
        
       
        buttonProceedNext.setTitle(PROCEED_NEXT.localized(), for: .normal)
        
        termsAndConditionCheckBox.layer.cornerRadius = 3
        termsAndConditionCheckBox.configureBox()
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        let highlightAttributed = [NSAttributedString.Key.foregroundColor: UIColor.GreenColor(),
                                   NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        labelTermsAndConditions.linkAttributeDefault = highlightAttributed as [AnyHashable : Any]
        labelTermsAndConditions.linkAttributeHighlight = attributes as [AnyHashable : Any]
        
        labelTermsAndConditions.attributedText = NSAttributedString(string: AGREE_TERMS_AND_CONDITIONS.localized(), attributes: attributes as [NSAttributedString.Key : Any])
        
        let handler = {(hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
//            self.getTermsAndConditions()
            
            //AlertManager.showAlert(message: COMING_SOON)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        labelTermsAndConditions.setLinksForSubstrings([TERMS_AND_CONDITIONS.localized()], withLinkHandler: handler)
        
     
        
    }
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNext(_ sender: Any)
    {
        if textFieldMobileNumber.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            
            textFieldMobileNumber.attributedPlaceholder = NSAttributedString(
                string: URL_STRING.localized(),
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            )
            return
            
            
        }
        
        if !isComingFromEdit
        {
//            if !termsAndConditionCheckBox.isChecked
//            {
//                AlertManager.showAlert(title: ERROR.localized(), message: ACCEPT_TERMS_AND_CONDITIONS.localized())
//                return
//            }
        }
        
        createSMSInvitation()
    }
    
  
    func createSMSInvitation()
    {
        
        var finalUrl = textFieldMobileNumber.text!
        if textFieldMobileNumber.text!.hasPrefix("https://") || textFieldMobileNumber.text!.hasPrefix("http://")
        {
            
        }
        else
        {
            finalUrl = "http://" +  finalUrl
        }
        
        var parameters: [String: Any] = ["Type" : "2", "Website": finalUrl,  "UserID" : SharedManager.getUser().UserID , "CreatedAt" : String(describing: convertDateIntoTimeStamp(date: Date()))]
        
        
        if isComingFromEdit
        {
            parameters["InvitationID"] = object.InvitationID
            ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: nil, imageKey: "", backgroundImage: nil, backgroundImageKey: "", qrImage: nil, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                
                
                print(response)
                
                AlertManager.showAlertWithOneButton(message: SUCCESS.localized()) {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
                
                
                
                
            } errorCallBack: { (error, response) in
                
                AlertManager.showAlert(message: error)
            }
        }
        else
        {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivateAndPublicQRSettingViewController") as? PrivateAndPublicQRSettingViewController
            {
        
            
                vc.dict = parameters
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
     

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
