//
//  ContactUsViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 15/10/2021.
//

import UIKit
import MessageUI

class ContactUsViewController: ParentViewController {

    @IBOutlet weak var buttonMailTo: ButtonBold!
    @IBOutlet weak var labelText: LabelRegularFont!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getContactUs()
        self.labelTitle.text = ""
        self.labelText.text = ""
        self.buttonMailTo.setTitle("", for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true

        
    }
    
    @IBAction func buttonMailTo(_ sender: Any) {
        
        self.openEmailComposer()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension ContactUsViewController : MFMailComposeViewControllerDelegate
{
    func openEmailComposer()
    {
        let recipientEmail = "support@bondqr.com"
        let subject = ""
        let body = ""
        
        // Show default mail composer
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipientEmail])
            mail.setSubject(subject)
            mail.setMessageBody(body, isHTML: false)
            
            present(mail, animated: true)
            
            // Show third party email composer if default Mail app is not present
        } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: "", body: "") {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension ContactUsViewController
{
    
    func getContactUs()
    {
        
        let parameters: [String: Any] = [ "PageID": "7"]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PAGE_DETAIL_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
           
            print(response)
            if let dict = response["page_data"] as? NSDictionary
            {
                var title = ""
                var description = ""
                
                if SharedManager.getArabic()
                {
                    title = dict["TitleAr"] as? String ?? ""
                    description = dict["DescriptionAr"] as? String ?? ""
                    
                   
                }
                else
                {
                 
                    title = dict["Title"] as? String ?? ""
                    description = dict["Description"] as? String ?? ""
                    
                }
                
          
                self.labelTitle.text = title.htmlToString
                
                let attributedString = description.htmlToString
                
                self.labelText.text = attributedString
                
                self.buttonMailTo.setTitle(dict["Email"] as? String ?? "", for: .normal)
                
               
               
            }
            
           
            
        } errorCallBack: { (error, response) in
            
         
            AlertManager.showAlert(message: error)
        }

    }
    
}
