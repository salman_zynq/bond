//
//  CreatCardViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 12/03/2021.
//

import UIKit
import MZFormSheetPresentationController

class CreatCardViewController: ParentViewController {
    
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    
    @IBOutlet weak var eventInvitation: LabelBoldFont!
    
    @IBOutlet weak var labelSelectBackground: LabelBoldFont!
    
    @IBOutlet weak var buttonProceedNext: ButtonBold!
    
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var containerBackgroundImage: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var textFieldSelectBackgroundColor: CustomTextField!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    var backgroundArray = [BackgroundImageObject]()
    
    var selectedIndex = -1
    
    var hexaColor = ""
    
    var isComingFromEdit = false
    var object = InvitationObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
//        buttonProceedNext.setTitle(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR" + " - " + "Next", for: .normal)
        
        buttonProceedNext.setTitle(PROCEED_NEXT.localized(), for: .normal)
        
        if isComingFromEdit
        {
            labelCreateA.text =  EDIT_A.localized()
        }
        else
        {
            labelCreateA.text  = CREATE_A.localized()
        }
        
        eventInvitation.text = EVENT_INVITATION.localized()
        labelSelectBackground.text = SELECT_BACKGROUND.localized()
        textFieldSelectBackgroundColor.placeholder = SELECT_BACKGROUND_COLOR.localized()
        
        containerBackgroundImage.layer.cornerRadius = 15
        containerBackgroundImage.clipsToBounds = true
        
        let flowLayout = UICollectionViewFlowLayout()
        _ = collectionView.bounds.width
        flowLayout.itemSize = CGSize(width: 90, height: 90)
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 8
        flowLayout.minimumInteritemSpacing = 0
        self.collectionView.setCollectionViewLayout(flowLayout, animated: false)
        
        getBackgroundImages()
        
        
        textFieldSelectBackgroundColor.addLeftAndRightView(image: UIImage.init(named: "colorpickerlogo")!)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func proceedNext(_ sender: Any)
    {
        
        if hexaColor == "" && self.backgroundImageView.image == nil
        {
            AlertManager.showAlert(message: "Select Background Color / Image")
            return
        }
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateWeddingFormViewController") as? CreateWeddingFormViewController
        {
            if selectedIndex != -1
            {
                vc.backGroundImage = self.backgroundImageView.image ?? UIImage()
            }
            else
            {
                vc.backGroundImage = self.containerBackgroundImage.asImage()
            }
           
            vc.isComingFromEdit = isComingFromEdit
            vc.object = self.object
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    func showColorPicker()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColorPickerController") as? ColorPickerController
        {
            
          
            vc.delegate = self
           
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:600)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
    
    
   

}

extension CreatCardViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.backgroundArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BackgroundImageCollectionViewCell
        
        
        if indexPath.row == 0
        {
            cell.coverImageView.image = UIImage.init(named: "addImage")
            return cell
        }
        else
        {
            let object = backgroundArray[indexPath.row - 1]
            
            cell.mainContainer.layer.cornerRadius = 5
            cell.mainContainer.clipsToBounds = true
            
            
            cell.mainContainer.layer.borderColor = UIColor.white.cgColor
            cell.mainContainer.layer.borderWidth = 0.0
            
            if self.selectedIndex == indexPath.row
            {
                cell.mainContainer.layer.borderColor = UIColor.GreenColor().cgColor
                cell.mainContainer.layer.borderWidth = 2.0
            }
            
            cell.coverImageView.image = nil
            cell.coverImageView.downLoadImageIntoImageView(url: object.BackgroundImage , placeholder: nil)
            let view = cell.viewWithTag(10)
            view?.layer.cornerRadius = 8
            view?.clipsToBounds = true
            return cell
        }
       
    }
}

extension CreatCardViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        
        
        if indexPath.row == 0
        {
            CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                
                
               // self.isPickImage = true
                self.containerBackgroundImage.backgroundColor = .white
                self.hexaColor = ""
                self.selectedIndex = -1
                self.backgroundImageView.isHidden = false
                self.collectionView.reloadData()
                self.backgroundImageView.image = image
            }
        }
        else
        {
            
            self.containerBackgroundImage.backgroundColor = .white
            self.backgroundImageView.isHidden = false
            self.selectedIndex = indexPath.row 
            backgroundImageView.downLoadImageIntoImageView(url: backgroundArray[indexPath.row - 1].BackgroundImage, placeholder: nil)
            self.hexaColor = ""
            self.collectionView.reloadData()
        }
        
        
       
        
    }
}


extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}


extension CreatCardViewController
{
    func getBackgroundImages() {
        
    
        self.view.endEditing(true)

        
        let parameters: [String: Any] = ["UserID": SharedManager.getUser().UserID]

        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_BACKGROUND_IMAGES, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
          
            
            print(response)
     
            self.backgroundArray = BackgroundImageObject.parseData(response: response)
            self.collectionView.reloadData()
            
            if self.backgroundArray.count > 0
            {
                self.selectedIndex = 1
                self.backgroundImageView.downLoadImageIntoImageView(url: self.backgroundArray.first?.BackgroundImage ?? "", placeholder: nil)
            }
           
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }

    }
}


extension CreatCardViewController : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.showColorPicker()
        return false
    }
}

extension CreatCardViewController : COLORPICKER
{
    func colorSelected(colorHexa:String , color:UIColor) {
    
        print(colorHexa)
        self.hexaColor = colorHexa
        self.selectedIndex = -1
        self.collectionView.reloadData()
        self.backgroundImageView.image = nil
        self.backgroundImageView.isHidden = true
        self.containerBackgroundImage.backgroundColor = color
        //self.navigationController?.popViewController(animated: true)
    }
}
