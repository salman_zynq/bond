//
//  AboutViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 18/03/2021.
//

import UIKit

class AboutViewController: ParentViewController {
    
    @IBOutlet weak var labelDescription: LabelBoldFont!
    
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var aboutImage: UIImageView!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    @IBOutlet weak var innerContainer: UIView!
    
    var isGuest = false
    
    
    var pageId = "2"
    
    var isQRPageDetail = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        
        
        
        innerContainer.layer.cornerRadius = 20
        
        innerContainer.layer.shadowColor = UIColor.GreenColor().cgColor
        innerContainer.layer.shadowOpacity = 1
        innerContainer.layer.shadowOffset = CGSize.zero
        innerContainer.layer.shadowRadius = 20
        
        
        mainContainer.isHidden = true
        getAboutUs()
        
     
       

    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        if isGuest
        {
            self.setRootAfterLoggedIn()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    
}


extension AboutViewController
{
    
    func getAboutUs()
    {
        
        let parameters: [String: Any] = [ "PageID": pageId]
        
        var url = BASE_URL + GET_PAGE_DETAIL_API
        
        if isQRPageDetail
        {
            url = BASE_URL + GET_QR_PAGE_DETAIL_API
        }
        ApiManager.getOrPostMethod(URLString: url, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            self.mainContainer.isHidden = false
            print(response)
            if let dict = response["page_data"] as? NSDictionary
            {
                var title = ""
                var description = ""
                
                if SharedManager.getArabic()
                {
                    title = dict["TitleAr"] as? String ?? ""
                    description = dict["DescriptionAr"] as? String ?? ""
                    
                   
                }
                else
                {
                 
                    title = dict["Title"] as? String ?? ""
                    description = dict["Description"] as? String ?? ""
                    
                }
                
                
                self.aboutImage.layer.cornerRadius = 12
                self.aboutImage.clipsToBounds = true
                
                self.aboutImage.downLoadImageIntoImageView(url: dict["Image"] as? String ?? "", placeholder: nil)
                self.labelTitle.text = title.htmlToString
                
                let attributedString = description.htmlToString
                
                self.labelDescription.text = attributedString
               
               
            }
            
           
            
        } errorCallBack: { (error, response) in
            
            self.mainContainer.isHidden = false
            AlertManager.showAlert(message: error)
        }

    }
    
}
