//
//  CreateVCardViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 20/04/2021.
//

import UIKit
import MZFormSheetPresentationController
import FlagPhoneNumber

class CreateVCardViewController: ParentViewController {
    
    @IBOutlet weak var labelTermsAndConditions: HyperLinkLabel!
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    
    @IBOutlet weak var labelVCardQR: LabelBoldFont!
    @IBOutlet weak var labelEnterDetail: LabelBoldFont!
    @IBOutlet weak var buttonProceedNext: ButtonBold!
    
    @IBOutlet weak var mainContainer: UIView!
    
    @IBOutlet weak var termsAndConditionCheckBox: SnapchatCheckbox!
    
    @IBOutlet weak var textFieldFirstName: CustomTextField!
    
    @IBOutlet weak var textFieldLastName: CustomTextField!
    
    @IBOutlet weak var textFieldCompanyName: CustomTextField!
    
    @IBOutlet weak var textFieldMobile: NumberTextField!
    
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    @IBOutlet weak var textFieldUrl: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlTwiter: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlFacebook: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlYoutube: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlInstagram: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlSnapChat: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlTikTok: CustomTextField!
    
    @IBOutlet weak var textFieldSocialMediaUrlLinkedIn: CustomTextField!
    
    @IBOutlet weak var textFieldAddress: CustomTextField!
    
    @IBOutlet weak var textFieldBirthday: CustomTextField!
    
    @IBOutlet weak var labelMakePrivate: HyperLinkLabel!
    
    @IBOutlet weak var makePrivateSnapChatBox: SnapchatCheckbox!
    
    var privatePassword = ""
    
    @IBOutlet weak var vCardImageView: UIImageView!
    
    @IBOutlet weak var heightOfTermsAndConditionsView: NSLayoutConstraint!
    
    @IBOutlet weak var heightOfAdditionalChargesView: NSLayoutConstraint!
    
    @IBOutlet weak var additionalChargesView: UIView!
    
    @IBOutlet weak var termsAndConditionsView: UIView!
    
    
    var isComingFromEdit = false
    var object = InvitationObject()
  
    
    
    var isValidPhone = false
    var dialingCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        if isComingFromEdit
        {
            
            termsAndConditionsView.isHidden = true
            additionalChargesView.isHidden = true
            heightOfAdditionalChargesView.constant = 0
            heightOfTermsAndConditionsView.constant = 0
            
            textFieldFirstName.text = object.FirstName
            textFieldLastName.text = object.LastName
            textFieldEmail.text = object.Email
            textFieldUrl.text = object.Website
            
            if object.ContactInfo != ""
            {
                
                textFieldMobile.set(phoneNumber: object.ContactInfo)

            }
            
            
            textFieldAddress.text = object.Address
            textFieldBirthday.text = object.Birthday
            textFieldCompanyName.text = object.Company
            textFieldSocialMediaUrlTwiter.text = object.TwitterLink
            textFieldSocialMediaUrlFacebook.text = object.FacebookLink
            textFieldSocialMediaUrlSnapChat.text = object.SnapchatLink
            textFieldSocialMediaUrlInstagram.text = object.InstagramLink
            textFieldSocialMediaUrlLinkedIn.text = object.LinkedinLink
            textFieldSocialMediaUrlTikTok.text = object.TiktokLink
            textFieldSocialMediaUrlYoutube.text = object.YoutubeLink
           
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    func setUpViews()
    {
        
        vCardImageView.image = UIImage.init(named: "Group 3135")
        
//        buttonProceedNext.setTitle(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR" + " - " + "Next", for: .normal)
        
        buttonProceedNext.setTitle(PROCEED_NEXT.localized(), for: .normal)
        
        
        labelMakePrivate.text = MAKE_PRIVATE.localized() + " (" + SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges + " " + "SAR" + " " + ADDITIONAL_CHARGES_APPLY.localized() + ")"
        
        
        termsAndConditionCheckBox.layer.cornerRadius = 3
        termsAndConditionCheckBox.configureBox()
        
        makePrivateSnapChatBox.layer.cornerRadius = 3
        makePrivateSnapChatBox.configureBox()
        
        
        
        if isComingFromEdit
        {
            labelCreateA.text =  EDIT_A.localized()
        }
        else
        {
            labelCreateA.text  = CREATE_A.localized()
            

            let fullNameArr = SharedManager.getUser().FullName.components(separatedBy: " ")
            let firstName: String = fullNameArr[0]
            let lastName: String? = fullNameArr.count > 1 ? fullNameArr[1] : nil
            textFieldFirstName.text = firstName
            textFieldLastName.text = lastName
            textFieldEmail.text = SharedManager.getUser().Email
            textFieldMobile.text = SharedManager.getUser().Mobile
        }
      
        labelVCardQR.text = VCARD_QR.localized()
        labelEnterDetail.text  = ENTER_DETAILS.localized()
        
        
       
        
        textFieldFirstName.placeholder = FIRST_NAME.localized()
        textFieldLastName.placeholder = LAST_NAAE.localized()
        textFieldCompanyName.placeholder = COMPANY.localized()
        textFieldMobile.placeholder = MOBILE_NUMBER_PLACEHOLDER.localized()
        textFieldEmail.placeholder = EMAIL.localized()
        textFieldUrl.placeholder = URL_STRING.localized()
        textFieldSocialMediaUrlTwiter.placeholder = SOCIAL_MEDIA_URL_TWITTER.localized()
        textFieldSocialMediaUrlFacebook.placeholder = SOCIAL_MEDIA_URL_FACEBOOK.localized()
        textFieldSocialMediaUrlYoutube.placeholder = SOCIAL_MEDIA_URL_YOUTUBE.localized()
        textFieldSocialMediaUrlInstagram.placeholder = SOCIAL_MEDIA_URL_INSTAGRAM.localized()
        textFieldSocialMediaUrlSnapChat.placeholder = SOCIAL_MEDIA_URL_SNAPCHAT.localized()
        textFieldSocialMediaUrlTikTok.placeholder = SOCIAL_MEDIA_URL_TIKTOK.localized()
        textFieldSocialMediaUrlLinkedIn.placeholder = SOCIAL_MEDIA_URL_LINKEDIN.localized()
        textFieldAddress.placeholder = ADDRESS.localized()
        textFieldBirthday.placeholder = BIRTHDAY.localized()
        
        
        
        
        textFieldMobile.setFlag(countryCode: FPNCountryCode.SA)
        
        textFieldMobile.flagButtonSize = CGSize(width: 20, height: 20)
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        let highlightAttributed = [NSAttributedString.Key.foregroundColor: UIColor.GreenColor(),
                                   NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        labelTermsAndConditions.linkAttributeDefault = highlightAttributed as [AnyHashable : Any]
        labelTermsAndConditions.linkAttributeHighlight = attributes as [AnyHashable : Any]
        
        labelTermsAndConditions.attributedText = NSAttributedString(string: AGREE_TERMS_AND_CONDITIONS.localized(), attributes: attributes as [NSAttributedString.Key : Any])
        
        let handler = {(hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
//            self.getTermsAndConditions()
            
            //AlertManager.showAlert(message: COMING_SOON)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        labelTermsAndConditions.setLinksForSubstrings([TERMS_AND_CONDITIONS.localized()], withLinkHandler: handler)
       
        
        
    }
    
    @IBAction func makePrivate(_ sender: Any)
    {
        if makePrivateSnapChatBox.isChecked
        {
            self.showPassword()
        }
    }
    
    
    @IBAction func buttonPrivateText(_ sender: Any) {
        
        
        self.getMakePrivateText()
     
    }
    
    
    
    func showPassword()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRPrivatePasswordViewController") as? QRPrivatePasswordViewController
        {
            
          
            vc.delegate = self
           
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:300)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func buttonNext(_ sender: Any)
    {
        
        if checkAllValidaitonWorks()
        {
           
//            Type=4,UserID,IsPrivate,PrivateCharges,TotalAmount,CreatedAt,ContactInfo,FirstName,LastName,Company,Email,QRUrl,TwitterLink,FacebookLink,YoutubeLink,InstagramLink,SnapchatLink,TiktokLink,LinkedinLink,Address,Birthday
            
            var mobileNumber = ""
            
            if isValidPhone
            {
                mobileNumber = textFieldMobile.getFormattedPhoneNumber(format: .E164) ?? ""
            }
           
    
            var parameters: [String: Any] = ["Type" : "4", "FirstName": textFieldFirstName.text!, "LastName" : textFieldLastName.text! ,  "Company": textFieldCompanyName.text! ,"Email": textFieldEmail.text!,"TwitterLink": textFieldSocialMediaUrlTwiter.text!, "FacebookLink" : textFieldSocialMediaUrlFacebook.text! , "YoutubeLink": textFieldSocialMediaUrlYoutube.text! , "InstagramLink" : textFieldSocialMediaUrlInstagram.text! , "SnapchatLink" : textFieldSocialMediaUrlSnapChat.text! , "TiktokLink" : textFieldSocialMediaUrlTikTok.text!, "LinkedinLink" : textFieldSocialMediaUrlLinkedIn.text! , "UserID" : SharedManager.getUser().UserID , "CreatedAt" : String(describing: convertDateIntoTimeStamp(date: Date())) , "Address" : textFieldAddress.text! , "Birthday" : textFieldBirthday.text! , "ContactInfo" : mobileNumber  , "Website" : textFieldUrl.text!]
            
        
            
            if isComingFromEdit
            {
                parameters["InvitationID"] = object.InvitationID
                
               
                
                ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: nil, imageKey: "", backgroundImage: nil, backgroundImageKey: "", qrImage: nil, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                    
                    
                    print(response)
                    
                    AlertManager.showAlertWithOneButton(message: SUCCESS.localized()) {
                        
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                    
                    
                    
                    
                } errorCallBack: { (error, response) in
                    
                    AlertManager.showAlert(message: error)
                }
            }
            else
            {
                if makePrivateSnapChatBox.isChecked
                {
                    
                    parameters["IsPrivate"] = "1"
                    parameters["InvitationPassword"] = self.privatePassword
                }
                else
                {
                    parameters["IsPrivate"] = "0"
                }
                
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivateAndPublicQRSettingViewController") as? PrivateAndPublicQRSettingViewController
                {
            
                
                    vc.dict = parameters
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
       
         
            
          
            
        }
        else
        {
            textFieldFirstName.attributedPlaceholder = NSAttributedString(
                string: FIRST_NAME.localized(),
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            )
            
            textFieldMobile.attributedPlaceholder = NSAttributedString(
                string: MOBILE_NUMBER.localized(),
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
            )
            
        }
        
        
        
        
       
    }
   

}


extension CreateVCardViewController
{
    func checkAllValidaitonWorks() -> Bool
    {
        
      
        
        if textFieldFirstName.isTextFieldEmpty()
        {

            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
//        if textFieldLastName.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//
//
//
//        if textFieldCompanyName.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//
      
    
        
        if !textFieldMobile.isTextFieldEmpty()
        {
            
            if !isValidPhone
            {

                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_MOBILE.localized())
                return false
            }
           
        }
        
        
//        if textFieldMobile.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
   
        
//        if textFieldEmail.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//
        
//        
//        if textFieldUrl.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//        if textFieldSocialMediaUrlTwiter.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//        
//        
//        if textFieldSocialMediaUrlFacebook.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//      
//        
//        
//        
//        if textFieldSocialMediaUrlYoutube.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//        
//        
//        if textFieldSocialMediaUrlInstagram.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//      
//        
//        
//        
//        if textFieldSocialMediaUrlSnapChat.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//   
//        
//        if textFieldSocialMediaUrlTikTok.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
//        
//        if textFieldSocialMediaUrlLinkedIn.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
//        if textFieldAddress.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
        
//        if textFieldBirthday.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
        if !isComingFromEdit
        {
//            if !termsAndConditionCheckBox.isChecked
//            {
//                AlertManager.showAlert(title: ERROR.localized(), message: ACCEPT_TERMS_AND_CONDITIONS.localized())
//                return false
//            }
            
        }
      
        if makePrivateSnapChatBox.isChecked
        {
            if privatePassword == ""
            {
                AlertManager.showAlert(title: ERROR.localized(), message: "Set Password".localized())
                return false
                
            }
        }
     
        
        
      return true
        
    }
    
   
}

extension CreateVCardViewController : PRIVATEPASSWORD
{
    
    func passwordSet(password: String) {
        
        self.privatePassword = password
    }
   
}


extension CreateVCardViewController : FPNTextFieldDelegate
{
    func fpnDisplayCountryList() {
        
    }
    
    
    
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        dialingCode = dialCode
        //textFieldMobile.text = ""
        
        
        
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        isValidPhone = isValid
        
    }
}
