//
//  HomeVC.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit



enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad   // iPad style UI (also includes macOS Catalyst)
}


class HomeVC: ParentViewController {
    
    
    var qrCodeLink = ""
    
    @IBOutlet weak var tableView: UITableView!
    var imageArray = ["Group 3134" , "Group 3130" , "Group 3135" , "Group 3150" , "Group 3243"]
    var titleArray = [BOND , EVENTS , VCARD , URL_STRING , APPS_STORE]
    var descriptionArray = [LETS_GET_BONDING_BY , INVITATION  , CREATE_YOUR_OWN_CONTACT , URL_QR_DESCRIPTION , ONE_QR_FOR_YOUR_APP_IN_BOTH_STORES.localized()]
    
    var categoryArray = [CategoryObject]()
    var qrDesignPricingArray = [PricingObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
      
       
        self.getCategories()
        self.getPricing()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
        if SharedManager.getUser().UserID != ""
        {
            if SharedManager.getUser().UserName == ""
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as? EditViewController
                {
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
            }
        }
        
        
        if SharedManager.sharedInstance.userTapOnNotification == true
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            {
                SharedManager.sharedInstance.userTapOnNotification = false
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    
        
        
        
     
       
    }

    @IBAction func buttonScanner(_ sender: Any)
    {
        ActionSheetManager.showActionSheetForQrRead(title: "", message: ARE_YOU_SURE_YOU_WANT_TO_CONTINUE.localized()) {
            
            self.initateScanner()
            
        } photoCallBack: {
            
         
            CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                
                self.readQRFromImage(qrCodeImage: image)
                
            }
            
        }
    }
    
    
    
    func getPricing()
    {
        
    
     
        let parameters = [String : Any]()
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PRICING_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            
            let pricingArray = PricingObject.parseData(response: response)
            

            self.qrDesignPricingArray = pricingArray
//            if pricingArray.count > 0
//            {
//                SharedManager.sharedInstance.selectedPageDesignPricingArray = pricingArray.first ?? PricingObject()
//            }
//            
          
          
           
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }
  
    
    func getPricingObject(qrType:Int)
    {
        if let object = self.qrDesignPricingArray.first(where: {$0.QrType == qrType})
        {
            print(object)
            
            SharedManager.sharedInstance.selectedQrDesignPricingArray = object
            
            
        } else {
           // item could not be found
        }
        
    }

}


extension HomeVC : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HomeTableViewCell
            cell.labelName.text = SharedManager.getUser().FullName
            cell.labelHello.text = HELLO.localized()
            cell.labelCreateAQRFor.text = CREATE_A_QR_FOR.localized()
           
            return cell
        }
        
        else
        {
            var cell = HomeTableViewCell()
            
            if indexPath.row == 1
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "bondingCell", for: indexPath) as! HomeTableViewCell
                cell.mainContainer.layer.borderWidth = 4.0
                cell.mainContainer.layer.borderColor = UIColor(red: 172/255.0, green: 164/255.0, blue: 127.0/255.0, alpha: 1.0).cgColor
                cell.mainContainer.layer.cornerRadius = 20
                cell.mainContainer.clipsToBounds = true
              
            }
            
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! HomeTableViewCell
            }
            
            
             
           
    
          
//
//            let object = self.categoryArray[indexPath.row - 1]
//            cell.labelTitle.text = object.Title
//            cell.labelSubTitle.text = object.SubTitle
//            cell.logoImage.contentMode = .scaleAspectFit
//            = nil
//            cell.logoImage.downLoadImageIntoImageView(url: object.ActivityImage, placeholder: nil)
            
            
          
            cell.logoImage.image  = UIImage.init(named: imageArray[indexPath.row - 1])
            
         
            cell.labelTitle.text = titleArray[indexPath.row - 1].localized()
            
            
            
            cell.labelSubTitle.text = descriptionArray[indexPath.row - 1].localized()
            
            
           
            return cell
        }
    }
}

extension HomeVC : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if SharedManager.getUser().UserID == "" ||  SharedManager.getUser().UserID == "0"
        {
            SharedManager.showLoginScreen()
            return 
        }
  
        
        if indexPath.row == 1
        {
            self.getPricingObject(qrType: 5)
            if SharedManager.getUser().IsSocialQr == 1
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SocialProfileViewController") as? SocialProfileViewController
                {
                    vc.invitationID = SharedManager.getUser().InvitationID
                    self.navigationController?.pushViewController(vc, animated: true)
                }


            }
            else
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateSocialAccountLinkViewController") as? CreateSocialAccountLinkViewController
                {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
          
        }
        else  if indexPath.row == 2
        {
            
            self.getPricingObject(qrType: 1)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatCardViewController") as? CreatCardViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
          
        }
        else if indexPath.row == 3
        {
            self.getPricingObject(qrType: 4)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateVCardViewController") as? CreateVCardViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.row == 4
        {
            self.getPricingObject(qrType: 2)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateSMSViewController") as? CreateSMSViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.row == 5
        {
            self.getPricingObject(qrType: 3)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateAppStoreLinkViewController") as? CreateAppStoreLinkViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension HomeVC
{
    func getCategories() {
        
    
        self.view.endEditing(true)

        
        let parameters: [String: Any] = ["UserID": SharedManager.getUser().UserID]

        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_ACTIVITIES, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
          
            
            print(response)
     
            self.categoryArray = CategoryObject.parseData(response: response)
            self.tableView.reloadData()
           
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }

    }
}



extension HomeVC
{
    func initateScanner()
    {
        let scanner = QRCodeScannerController()
        scanner.delegate = self
        self.present(scanner, animated: true, completion: nil)
        
       
    }
    
    
    func readQRFromImage(qrCodeImage : UIImage)
    {
        guard
            
            let detector: CIDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy:CIDetectorAccuracyHigh]),
            let ciImage: CIImage = CIImage(image:qrCodeImage),
            let features = detector.features(in: ciImage) as? [CIQRCodeFeature]
        else {
            print("Something went wrong")
            return
        }
        var qrCodeLink = ""
        features.forEach { feature in
            if let messageString = feature.messageString {
                qrCodeLink += messageString
            }
        }
        if qrCodeLink.isEmpty {
            print("qrCodeLink is empty!")
            self.perform(#selector(showErrorAlert), with: self, afterDelay: 0.3)
            
        } else {
          
            self.qrCodeLink = qrCodeLink
            self.perform(#selector(showSuccessAlert), with: self, afterDelay: 0.3)
        }
    }
    
    @objc func showErrorAlert()
    {
        AlertManager.showAlert(message: "Invalid QR".localized())
    }
    
    @objc func showSuccessAlert()
    {
        
       
            
            guard let url = URL(string: self.qrCodeLink) else { return }
            UIApplication.shared.open(url)
            
        
     
    }
}


extension HomeVC : QRScannerCodeDelegate
{
    
    func qrScanner(_ controller: UIViewController, scanDidComplete result: String) {
        print("result:\(result)")
        
        self.qrCodeLink = result
        
        self.perform(#selector(showSuccessAlert), with: self, afterDelay: 0.3)
        
    }

    func qrScannerDidFail(_ controller: UIViewController, error: String) {
        print("error:\(error)")
        self.perform(#selector(showErrorAlert), with: self, afterDelay: 0.3)
    }

    func qrScannerDidCancel(_ controller: UIViewController) {
        print("SwiftQRScanner did cancel")
    }
    
  
}
