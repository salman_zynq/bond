//
//  MyQRViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit
import MFSDK


class MyQRViewController: ParentViewController {
    
    var qrImageView = UIImageView()
    
    @IBOutlet weak var labelMyQR: LabelBoldFont!
    
    

    @IBOutlet weak var tableView: UITableView!
    
    
    var qrArray = [InvitationObject]()
    
    var paymentMethods: [MFPaymentMethod]?
    var selectedPaymentMethodIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

       setUpViews()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        self.getQR()
     
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
    }

    
    func setUpViews()
    {
        labelMyQR.text = MY_QRS.localized()
    }
    
    func getQR()
    {
        let parameter = ["UserID" : SharedManager.getUser().UserID ] as [String : Any]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_INVITATION, method: .get, parameters: parameter, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.qrArray = InvitationObject.parseMultipleData(userDictionary: response)
            self.tableView.reloadData()
          //  self.getQRScanCount()
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }
    
    
    func writeImage(image: UIImage) {
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.finishWriteImage), nil)
    }
    
    @objc private func finishWriteImage(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if (error != nil) {
            // Something wrong happened.
            print("error occurred: \(String(describing: error))")
            AlertManager.showAlert(message: String(describing: error))
        } else {
            // Everything is alright.
            print("saved success!")
            AlertManager.showAlert(message: QR_SUCCESSFULLY_SAVED.localized())
        }
    }
    
    @IBAction func buttonDelete(_ sender: UIButton)
    {
        let object = self.qrArray[sender.tag]
        
        ActionSheetManager.showActionSheetForQrEditOrDelete(title: ALERT.localized(), message: SELECT_AN_OPTION.localized() , invoiceID : object.InvoiceID ) {
            
            //Download QR IMAGE
            
            
            SharedManager.showHUD()
            
            self.qrImageView.downLoadQRImageAndReturnResult(url: IMAGE_BASE_URL + object.QrImage, placeholder: nil) { (image) in
                
                
                SharedManager.hideHud()
                self.writeImage(image: image)
                
            } errorResult: { (error) in
                
                SharedManager.hideHud()
                AlertManager.showAlert(message: error)
            }
            
       
            
            
        } deleteCallBack: {
            AlertManager.showAlertWithTwoCustomButtons(title: ALERT.localized(), message: ARE_YOU_SURE_YOU_WANT_TO_CONTINUE.localized(), firstButtonTitle: CANCEL.localized(), secondButtonTitle: YES.localized(), firstButtonAlertStyle: .default, secondButtonAlertStyle: .destructive) {
                
            } secondButtonCallBack: {
            
                //Delete api called
                self.deleteQr(tag: sender.tag)
            }
        } editCallBack: {
            
            let object = self.qrArray[sender.tag]
            
            if object.InvitationType == "1"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreatCardViewController") as? CreatCardViewController
                {
                    
                    vc.object = object
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
            else  if object.InvitationType == "2"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateSMSViewController") as? CreateSMSViewController
                {
                    vc.object = object
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else  if object.InvitationType == "3"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateAppStoreLinkViewController") as? CreateAppStoreLinkViewController
                {
                    vc.object = object
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else  if object.InvitationType == "4"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateVCardViewController") as? CreateVCardViewController
                {
                    vc.object = object
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else  if object.InvitationType == "5"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CreateSocialAccountLinkViewController") as? CreateSocialAccountLinkViewController
                {
                    vc.object = object
                    vc.isComingFromEdit = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
        } shareCallBack: {
            
            let object = self.qrArray[sender.tag]
            
            let myWebsite = NSURL(string:IMAGE_BASE_URL + object.QrImage)
            let shareAll = [myWebsite]
            let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        } updateQRPrice: {
            
            
            //UpdatePrice
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayWeddingInvitationViewController") as? PayWeddingInvitationViewController
            {
                vc.isComingForUpdatePrice = true
                vc.isPaidVersion = "1"
                vc.invitationObject = object
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }

        
        
    

        

        
 

    }
    
    
    func deleteQr(tag : Int)
    {
        let object = self.qrArray[tag]
        let parameter = ["UserID" : SharedManager.getUser().UserID , "InvitationID" : object.InvitationID ] as [String : Any]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + DELETE_QR, method: .post, parameters: parameter, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.qrArray.remove(at: tag)
            self.tableView.reloadData()
            
            
            AlertManager.showAlert(message: response["message"] as? String ?? "N/A")
          
           
            if object.InvitationType == "5"
            {
                self.getUserData(userID: SharedManager.getUser().UserID)
            }
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
    }
    
    
    func getUserData(userID: String) {
        
    
        let parameters: [String: Any] = ["UserID": userID]
        
     
     
        
    
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_USER_DETAILS, method: .get, parameters: parameters, isShowAI: false, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
          
        
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
        

            
            
        }) { (error , response) in
            
           // AlertManager.showAlert(message: error)
        }
        
     
        
    }
 

}


extension MyQRViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.qrArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyQRTableViewCell
        
        let object = self.qrArray[indexPath.row]
        
        cell.buttonDelete.tag = indexPath.row
        cell.mainContainer.setCornersOfView(cornerRadus: 10)
        
        cell.qrImageView.image = nil
        cell.qrImageView.downLoadImageIntoImageView(url: object.QrImage, placeholder: nil , isQRImage: false)
        
        if object.InvitationType == "1"
        {
            cell.labelQRTitle.text = object.Title
        }
        else if object.InvitationType == "2"
        {
            cell.labelQRTitle.text = object.Website
        }
        else if object.InvitationType == "3"
        {
            switch UIDevice.current.userInterfaceIdiom {
               case .phone:
                   
                cell.labelQRTitle.text = object.IosAppLink
               case .pad:
                   
                cell.labelQRTitle.text = object.IosAppLink

            case .unspecified:
                
                cell.labelQRTitle.text =  object.IosAppLink
                
            case .tv:
                
                cell.labelQRTitle.text =  object.IosAppLink
                
            case .carPlay:
                
                cell.labelQRTitle.text =  object.IosAppLink
                
            case .mac:
                
                cell.labelQRTitle.text =  object.IosAppLink
                
            @unknown default:
                   // Uh, oh! What could it be?
                    cell.labelQRTitle.text =  object.AndroidAppLink
               }
           
        }
        else if object.InvitationType == "4"
        {
          
            cell.labelQRTitle.text = object.FirstName + " " + object.LastName
           
        }
        else if object.InvitationType == "5"
        {
            
            cell.labelQRTitle.text = object.FirstName + " " + object.LastName
            
           
        }
        
        cell.labelCreatedAt.text = CREATED_ON.localized() + " " + convertTimeStampToShortDate(timeStamp: object.CreatedAt)
        
        
        if object.IsPrivate == "1"
        {
            cell.labelQRType.text = PRIVATE.localized()
        }
        else
        {
            cell.labelQRType.text = PUBLIC.localized()
        }
        
        return cell
        
    }
}


extension MyQRViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = self.qrArray[indexPath.row]
        if SharedManager.getUser().IsAppLive == 0
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRDetailViewController") as? QRDetailViewController
            {
                vc.object = object
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            if SharedManager.getUser().CanCreateFreeQr == "1"
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRDetailViewController") as? QRDetailViewController
                {
                    vc.object = object
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else if object.InvoiceID != ""
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRDetailViewController") as? QRDetailViewController
                {
                    vc.object = object
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                AlertManager.showAlert(message: STATISTICS_NOT_AVAILABLE_FOR_FREE_QR.localized())
            }
        }
       
       
    }
}

