//
//  FinalResultWeddingInvitationsViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit

class FinalResultWeddingInvitationsViewController: ParentViewController {
    
    @IBOutlet weak var labelShare: LabelRegularFont!
    @IBOutlet weak var labelQrGeneratedMessage: LabelBoldFont!
    @IBOutlet weak var labelCongratulations: LabelBoldFont!
    
    var object = InvitationObject()
    
    var qrID = ""
    var imageUrl = ""
    
    @IBOutlet weak var buttonDone: ButtonBold!
    @IBOutlet weak var containerButton: UIView!
    @IBOutlet weak var innerContainer: UIView!
    
    
    @IBOutlet weak var qrImageView: UIImageView!
    var dict = [String : Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        innerContainer.layer.cornerRadius = 20
        
        innerContainer.layer.shadowColor = UIColor.GreenColor().cgColor
        innerContainer.layer.shadowOpacity = 1
        innerContainer.layer.shadowOffset = CGSize.zero
        innerContainer.layer.shadowRadius = 20
        
        containerButton.setBackgroundColorBorderColorRadiusOfView(borderColor: UIColor.GreenColor(), borderWidth: 2.0, cornerRadus: 10, backgroundColor: .white)
        buttonDone.setTitleColor(UIColor.GreenColor(), for: .normal)
        
        
      
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
               //call any function
            self.showHud()
           }
        
        self.qrImageView.downLoadQRImageAndReturnResult(url: imageUrl, placeholder: nil) { (image) in
            
         
            
           
            SharedManager.hideHud()
            self.updateUrl()
            
        } errorResult: { (error) in
            
            SharedManager.hideHud()
            AlertManager.showAlert(message: error)
        }
        
        
       
        
       
        setUpViews()
        
    }
    
    
    func showHud()
    {
        SharedManager.showHUD()
    }
    
    func setUpViews()
    {
        labelCongratulations.text = CONGRATULATIONS.localized()
        labelQrGeneratedMessage.text = YOUR_QR_HAS_BEEN_GENERATED_SUCCESSFULLY.localized()
        buttonDone.setTitle(DONE.localized(), for: .normal)
        labelShare.text = SHARE.localized()
    }
    @IBAction func buttonShare(_ sender: Any)
    {
        
        let myWebsite = NSURL(string:IMAGE_BASE_URL + object.QrImage)
        let shareAll = [ qrImageView.image , myWebsite]
        let activityViewController = UIActivityViewController(activityItems: shareAll as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func updateUrl()
    {
        self.showHud()
        
        let parameter = ["QRUrl" :  imageUrl , "InvitationID" : object.InvitationID , "UserID" : SharedManager.getUser().UserID , "QRID" : qrID ] as [String : Any]
        
        
        let pathExtention = URL(string: self.imageUrl)?.pathExtension
       
        ApiManager.getOrPostMethodWithMultiPartsForUpdateProfile(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameter, isShowAI: false, mainView: self.view, image: self.qrImageView.image, imageKey: "QrImage", imageExtension: pathExtention ?? "jpg" , isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            print(response)
        
            SharedManager.hideHud()
            
            self.getUserData()
            
        } errorCallBack: { (error, response) in
            
            if error != ""
            {
                AlertManager.showAlert(message: error)
            }
        }

      
        
    }
    
    
    func getUserData() {
        
    
        self.showHud()
        
        let parameters: [String: Any] = ["UserID": SharedManager.getUser().UserID]
        
    
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_USER_DETAILS, method: .get, parameters: parameters, isShowAI: false, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
    
            
            SharedManager.hideHud()
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
       

            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
     
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
