//
//  PrivateAndPublicQRSettingViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit
import MZFormSheetPresentationController

class PrivateAndPublicQRSettingViewController: ParentViewController
{
    
    
    
    var formSheetController = MZFormSheetPresentationViewController()
    var mzController = UIViewController()
    
    
    
    @IBOutlet weak var viewQRBackgroundColor: TextFieldView!
    @IBOutlet weak var labelDesignQR: LabelBoldFont!
    @IBOutlet weak var viewQrImageBackground: TextFieldView!
    @IBOutlet weak var buttonNext: ButtonBold!
    
    var isPaidVersion = "0"

    @IBOutlet weak var containerQRImag: UIView!
    
    var backGroundImage = UIImage()
    
    var dict = [String : Any]()
    
    var isSelectColor = false
    
    
    @IBOutlet weak var textFieldSelectBackgroundImage: CustomTextField!
    
    @IBOutlet weak var textFieldSelectBackgroundColor: CustomTextField!
    
    @IBOutlet weak var qrImageView: UIImageView!
    

    
    @IBOutlet weak var outerFreeVersion: TextFieldView!
    @IBOutlet weak var outerPaidVersion: TextFieldView!
    @IBOutlet weak var labelFreeVersion: LabelBoldFont!
    @IBOutlet weak var labelPaidVersion: LabelBoldFont!
    @IBOutlet weak var freeVersionCircle: UIView!
    @IBOutlet weak var paidVersionCircle: UIView!
    @IBOutlet weak var innerViewFreeVersion: UIView!
    
    
    @IBOutlet weak var textFieldCellType: CustomPickerTextField!
    
    @IBOutlet weak var textFieldRotationType: CustomPickerTextField!
    @IBOutlet weak var textFieldMarkerType: CustomPickerTextField!
    @IBOutlet weak var innerViewPaidVersion: UIView!

    @IBOutlet weak var outerContainer: UIView!
    
    var isPickImage = false
    @IBOutlet weak var previewQRStyleImage: UIImageView!
    
    
    var cellTypeInt = 1
    var merkerTypeInt = 1
    var rotationType = "0"
    
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    var pathExtention = "jpg"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        DispatchQueue.main.async {
            
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1 || SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 3
            {
                self.setSelectionsButtons(tag: 2)
            }
            else
            {
                self.setSelectionsButtons()
            }
           
        }
        
        
        self.containerQRImag.backgroundColor = .white
        self.qrImageView.image = self.containerQRImag.asImage()
        
        self.setPickersValue()
        setUpViews()
    }
    
    
    func setPickersValue()
    {
       
        
        textFieldSelectBackgroundColor.addLeftAndRightView(image: UIImage.init(named: "colorpickerlogo")!)
        
//        buttonNext.setTitle(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR" + " - " + "Create QR", for: .normal)
//
        
        
        print(SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType)
        if  SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 3 || SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1
        {
//            self.viewQrImageBackground.isHidden = false
            stackView.isHidden = true
            stackViewHeight.constant = 0
            isPaidVersion = "1"
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1
            {
                viewQrImageBackground.isHidden = false
                
            }
            else
            {
                viewQrImageBackground.isHidden = true
            }
            
            
        }
        
        if SharedManager.getUser().IsAppLive == 0
        {
            stackView.isHidden = true
            stackViewHeight.constant = 0
            isPaidVersion = "0"
        }
        
        
        
        if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1
        {
            logoImageView.image = UIImage.init(named: "Group 3130")
        }
        else if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 2
        {
            logoImageView.image = UIImage.init(named: "Group 3150")
        }
        else if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 3
        {
            logoImageView.image = UIImage.init(named: "Group 3243")
        }
        else if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 4
        {
            logoImageView.image = UIImage.init(named: "Group 3135")
        }
        else if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 5
        {
            logoImageView.image = UIImage.init(named: "Group 3134")
        }
    
      
        textFieldCellType.pickerType = .string(data: [SQUARED.localized() , ROUNDED.localized()])
        textFieldCellType.pickerRow.font = UIFont(name: SharedManager.mediumFont(), size: 14)
        
        textFieldCellType.toolbar.barTintColor = .darkGray
        textFieldCellType.toolbar.tintColor = .darkGray
        textFieldCellType.valueDidSelected = { (index) in
           
            let value = index as! Int
            self.cellTypeInt = (value + 1)
            
            self.setUpPreviewQR()
        }
        
        
        textFieldMarkerType.pickerType = .string(data: [SQUARED.localized() , ROUNDED.localized()])
        textFieldMarkerType.pickerRow.font = UIFont(name: SharedManager.mediumFont(), size: 14)
        
        textFieldMarkerType.toolbar.barTintColor = .darkGray
        textFieldMarkerType.toolbar.tintColor = .darkGray
        textFieldMarkerType.valueDidSelected = { (index) in
           
            let value = index as! Int
            self.merkerTypeInt = (value + 1)
            
            self.setUpPreviewQR()
            
            
        }
        
        
        let rotationArray = ["0" , "90" , "180" , "270"]
        
        textFieldRotationType.pickerType = .string(data: rotationArray)
        textFieldRotationType.pickerRow.font = UIFont(name: SharedManager.mediumFont(), size: 14)
        
        textFieldRotationType.toolbar.barTintColor = .darkGray
        textFieldRotationType.toolbar.tintColor = .darkGray
        textFieldRotationType.valueDidSelected = { (index) in
           
            self.rotationType = rotationArray[index as! Int]
            
            
        }
        
        textFieldCellType.text = SQUARED.localized()
        textFieldMarkerType.text = SQUARED.localized()
        textFieldRotationType.text = "0"
        self.setUpPreviewQR()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        outerContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    
    func setUpViews()
    {
        labelDesignQR.text = DESIGN_THE_QR.localized()
        textFieldSelectBackgroundImage.placeholder = UPLOAD_BACKGROUND_IMAGE.localized()
        textFieldSelectBackgroundColor.placeholder = SELECT_BACKGROUND_COLOR.localized()
        buttonNext.setTitle(CREATE_QR.localized(), for: .normal)
        labelFreeVersion.text = FREE_VERSION.localized()
        labelPaidVersion.text = PREMIUM.localized()
    }
    
    
    func setUpPreviewQR()
    {
        
        if  textFieldCellType.text == SQUARED.localized() &&  textFieldMarkerType.text == SQUARED.localized()
        {
           
            
            DispatchQueue.main.async {
                
                self.previewQRStyleImage.image = UIImage.init(named: "Square square")
            }
        }
        else if  textFieldCellType.text == SQUARED.localized() &&  textFieldMarkerType.text == ROUNDED.localized()
        {
            
            
            DispatchQueue.main.async {
                
                self.previewQRStyleImage.image = UIImage.init(named: "Square rounded")
            }
        }
        else if  textFieldCellType.text == ROUNDED.localized() &&  textFieldMarkerType.text == SQUARED.localized()
        {
            
            
            DispatchQueue.main.async {
                
                self.previewQRStyleImage.image = UIImage.init(named: "Rounded squared")
            }
        }
        
        else if  textFieldCellType.text == ROUNDED.localized() &&  textFieldMarkerType.text == ROUNDED.localized()
        {
            DispatchQueue.main.async {
                
                self.previewQRStyleImage.image = UIImage.init(named: "Rounded rounded")
            }
           
        }
    }
    
    @IBAction func buttonSelectionSeekerAndProvidor(_ sender: UIButton) {
        
        
        setSelectionsButtons(tag: sender.tag)
    }
    
    
    
    
    func setSelectionsButtons(tag : Int = 1)
    {
        
        outerFreeVersion.backgroundColor = .clear
        outerPaidVersion.backgroundColor = .clear
        innerViewFreeVersion.backgroundColor = .clear
        innerViewPaidVersion.backgroundColor = .clear
        labelFreeVersion.textColor = .GreenColor()
        labelPaidVersion.textColor = .GreenColor()
        
        innerViewFreeVersion.makeViewRound()
        innerViewPaidVersion.makeViewRound()
        
        freeVersionCircle.setBackgroundColorBorderColorRadiusOfView(borderColor: UIColor.GreenColor(), borderWidth: 0.5, cornerRadus: freeVersionCircle.frame.size.height / 2, backgroundColor: .white)
        paidVersionCircle.setBackgroundColorBorderColorRadiusOfView(borderColor: UIColor.GreenColor(), borderWidth: 0.5, cornerRadus: paidVersionCircle.frame.size.height / 2, backgroundColor: .white)
        
        
        UIView.animate(withDuration: 0.5) {
            
            if tag == 1
            {
                self.viewQrImageBackground.isHidden = true
                self.isPaidVersion = "0"
                self.labelFreeVersion.textColor = .white
                self.outerFreeVersion.backgroundColor = UIColor.GreenColor()
                self.innerViewFreeVersion.backgroundColor = UIColor.Ferozi()
                self.isPickImage = false
                self.qrImageView.image = nil
                self.containerQRImag.backgroundColor = .white
                self.qrImageView.image = self.containerQRImag.asImage()
                self.viewQRBackgroundColor.isHidden = false
            }
            else
            {
              
                self.viewQRBackgroundColor.isHidden = true
                self.viewQrImageBackground.isHidden = false
                self.isPaidVersion = "1"
                self.labelPaidVersion.textColor = .white
                self.outerPaidVersion.backgroundColor = UIColor.GreenColor()
                self.innerViewPaidVersion.backgroundColor = UIColor.Ferozi()
            }
        }
        
        
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func buttonGenerateQR(_ sender: Any) {
        
        
//        http://api.visualead.com/v3/generate?api_key=284ddfc0-fa52-11e1-a21f-0800200c9a66&image=http://blog.visualead.com/wp-content/uploads/2012/09/Top_image11.png&action=url&content[url]=http://blog.visualead.com&output_type=0
        
        print(self.qrImageView.image)
        if self.qrImageView.image == nil
        {
            AlertManager.showAlert(message: SELECT_IMAGE)
            return
        }
        
        
        if textFieldCellType.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            
        }
        
        if textFieldMarkerType.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            
        }
        
        
//        if textFieldRotationType.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            
//        }
        
        
        
      
        
        var size = self.qrImageView.image!.size.height
        print(size)
        
        if self.isPickImage == false
        {
            size = 480
        }
        
        
//        var url = QR_API + "?api_key=" + VISUAL_LEAD_API_KEY + "&image=" + convertImageToBase64String(img: self.qrImageView.image!) + "&action=url" + "&content[url]=http://www.qr.schopfen.com/cms"  + "&output_type=0" + "&cells_type=1" + "&markers_type=2"
        
//        var url = QR_API + "?api_key=" + VISUAL_LEAD_API_KEY + "&image=asd"  + "&action=url" + "&content[url]=http://www.qr.schopfen.com/cms"  + "&output_type=0" + "&cells_type=1" + "&markers_type=2"
//
//
//        url = url + "&qr_size=" + String(describing: size) + "&qr_gravity=center"
//
//        print(url)
        
        
  
        
        let base64EncodedImage = convertImageToBase64String(img: self.qrImageView.image! , extensionOfImage: pathExtention)
        
    
        
        var parameter = ["api_key" :  VISUAL_LEAD_API_KEY , "image" : base64EncodedImage ,  "action" : "url"  , "output_type" : 1 , "cells_type" : self.cellTypeInt , "markers_type" : self.merkerTypeInt , "qr_gravity" : "center" ] as [String : Any]
        
        parameter["qr_size"] = size
      
//        parameter["qr_x"] = "10"
//        parameter["qr_y"] = "10"
   
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PayWeddingInvitationViewController") as? PayWeddingInvitationViewController
        {
            
            
            vc.isPaidVersion = isPaidVersion
            vc.backGroundImage = self.backGroundImage
            vc.dict = self.dict
            vc.qrDict = parameter
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
        
      

        
        
      
        
    }
    
    func convertImageToBase64String (img: UIImage , extensionOfImage:String = "jpg") -> String
    {
        
        print("Extension Of Image is \(extensionOfImage)")
        if extensionOfImage == "png"
        {
            let imageData = img.pngData()
            let base64String = imageData?.base64EncodedString() ?? ""

            return base64String
        }
   
        return img.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
    }
    
    @IBAction func buttonDetail(_ sender: Any)
    {
        var pageId = ""
        
        if self.isPaidVersion == "0"
        {
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 5
            {
                // Social
                pageId = "10"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 4
            {
                // Vcard
                pageId = "8"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 3
            {
                // Appstore
                pageId = "6"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 2
            {
                // SMS
                pageId = "4"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1
            {
                // Wedding
                pageId = "2"
            }
            
            
        }
        else
        {
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 5
            {
                // Social
                pageId = "9"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 4
            {
                // Vcard
                pageId = "7"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 3
            {
                // Appstore
                pageId = "5"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 2
            {
                // SMS
                pageId = "3"
            }
            if SharedManager.sharedInstance.selectedQrDesignPricingArray.QrType == 1
            {
                // Wedding
                pageId = "1"
            }
            
            
            
            
        }
        
     
        
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController
            {
                vc.isQRPageDetail = true
                vc.pageId = pageId
                self.navigationController?.pushViewController(vc, animated: true)
            }
    }
    
    
    

    

    
    func showColorPicker()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColorPickerController") as? ColorPickerController
        {
            
          
            vc.delegate = self
           
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:600)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
   

}


extension PrivateAndPublicQRSettingViewController : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.isEqual(textFieldSelectBackgroundImage)
        {
            if isPaidVersion == "1"
            {
                CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                    
                  
                    
                    self.isPickImage = true
                    self.qrImageView.image = image
                
                    self.pathExtention =  url?.pathExtension ?? "png"
                    self.containerQRImag.backgroundColor = .white
                }
            }
            
            
            return false
        }
        
        if textField.isEqual(textFieldSelectBackgroundColor)
        {
//            if isPaidVersion == "1"
//            {
                self.showColorPicker()
           // }
            
            return false
        }
        
        return true
    }
}


extension PrivateAndPublicQRSettingViewController : COLORPICKER
{
    func colorSelected(colorHexa:String , color:UIColor) {
    
     
        self.isPickImage = false
        
        self.containerQRImag.backgroundColor = color
        self.qrImageView.image = nil
        self.qrImageView.image = self.containerQRImag.asImage()
        self.containerQRImag.backgroundColor = .white
        
        //self.navigationController?.popViewController(animated: true)
    }
}
