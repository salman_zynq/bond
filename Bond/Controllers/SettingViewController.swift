//
//  SettingViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit


class SettingViewController: ParentViewController {
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var labelMore: LabelBoldFont!
    var titleArray = [VIEW_MY_PROFILE.localized() , EDIT_MY_INFORMATION.localized() , NOTIFICATIONS.localized() , CHANGE_PASSWORD.localized() , CHANGE_LANGUAGE.localized()  , ABOUT_BOND.localized() , CONTACT_US.localized() , LOGOUT.localized() ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelMore.text = MORE.localized()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    
}

extension SettingViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SettingTableViewCell
        cell.labelTitle.text = titleArray[indexPath.row]
        
        if SharedManager.getUser().UserID == "" || SharedManager.getUser().UserID == "0"
        {
            if indexPath.row == (titleArray.count - 1)
            {
                cell.labelTitle.text = LOGIN.localized()
            }
        }
        return cell
        
    }
}

extension SettingViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if SharedManager.getUser().UserID == "" || SharedManager.getUser().UserID == "0"
        {
            if indexPath.row == 0 ||  indexPath.row == 1 ||  indexPath.row == 2 ||  indexPath.row == 3
            {
                return 0
            }
        }
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            
            if SharedManager.getUser().IsSocialQr == 1
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SocialProfileViewController") as? SocialProfileViewController
                {
                    vc.invitationID = SharedManager.getUser().InvitationID
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else
            {
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
                {
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
        }
        else if indexPath.row == 1
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as? EditViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.row == 2
        {
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
        else if indexPath.row == 3
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.row == 4
        {
            self.changeLanague()
        }
        
        else if indexPath.row == 5
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.row == 6
        {
            //Contact US
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
        else if indexPath.row == 7
        {
            if SharedManager.getUser().UserID == "" || SharedManager.getUser().UserID == "0"
            {
                if indexPath.row == (titleArray.count - 1)
                {
                    SharedManager.showLoginScreen()
                }
                
            }
            else
            {
                AlertManager.showAlertWithTwoCustomButtons(message: ARE_YOU_SURE_YOU_WANT_TO_CONTINUE.localized(), firstButtonTitle: CANCEL.localized(), secondButtonTitle: LOGOUT.localized(), firstButtonAlertStyle: .destructive, secondButtonAlertStyle: .default, firstButtonCallBack: {
                    
                }) {
                    
                    
                    self.logOutFromApp()
                    
                    
                }
            }
            
            
        }
    }
}



