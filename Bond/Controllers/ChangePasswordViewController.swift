//
//  ChangePasswordViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 18/03/2021.
//

import UIKit

class ChangePasswordViewController: ParentViewController {
    
   
    
    // textfields
    
    @IBOutlet weak var textFieldCurrentPassword: CustomTextField!
    
    @IBOutlet weak var textFieldNewPassword: CustomTextField!
    
    @IBOutlet weak var textFieldConfirmPassword: CustomTextField!
    
    @IBOutlet weak var buttonViewUpdate: ButtonView!
    
    
    @IBOutlet weak var buttonViewDiscard: ButtonCancelView!
    // buttons
    @IBOutlet weak var buttonDiscard: ButtonBold!
    @IBOutlet weak var buttonUpdate: ButtonBold!
    
    @IBOutlet weak var labelTitle: LabelBoldFont!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    
    func setUpView(){

       
        
        
        labelTitle.text = CHANGE_PASSWORD.localized()
      
        textFieldCurrentPassword.placeholder = CURRENT_PASSWORD.localized()
        textFieldNewPassword.placeholder = NEW_PASSWORD.localized()
        textFieldConfirmPassword.placeholder = CONFIRM_PASSWORD.localized()
  
        buttonUpdate.setTitle(UPDATE.localized(), for: .normal)
        buttonDiscard.setTitle(DISCARD.localized(), for: .normal)
        
    }
    
    @IBAction func buttonUpdate(_ sender: Any)
    {
        if textFieldCurrentPassword.isTextFieldEmpty() ||  textFieldNewPassword.isTextFieldEmpty()  || textFieldConfirmPassword.isTextFieldEmpty()
        {
                            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return
        }
        
        if  textFieldNewPassword.text == textFieldCurrentPassword.text
        {
                            
            AlertManager.showAlert(title: ERROR.localized(), message: CURRENT_AND_NEWS_PASSWORD_SAME.localized())
            return
            
        }
               
               
//        if  !textFieldNewPassword.isValidPassword()
//        {
//                            
//            AlertManager.showAlert(title: ERROR.localized(), message: PASSWORD_STRINGS.localized())
//            return
//            
//        }
        
        if textFieldNewPassword.text != textFieldConfirmPassword.text
        {
            AlertManager.showAlert(title: ERROR.localized(), message: PASSWORD_MISMATCH.localized())
            return
        }
        
        self.changePassword()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension ChangePasswordViewController
{
    func changePassword()
    {
       
       
        let params = ["UserID" : String(describing: SharedManager.getUser().UserID) , "NewPassword" : textFieldNewPassword.text! , "OldPassword" : textFieldCurrentPassword.text!]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + CHANGE_PASSWORD_API, method: .post, parameters: params, isShowAI: true, mainView: self.view, isLanguageKeySend: true, isHeaderNeeded: true ,  successCallback: { (response) in
            
            print(response)
            AlertManager.showAlertWithOneButton(title: SUCCESS.localized(), message: response["message"] as? String ?? "N/A", buttonText: LOGOUT.localized()) {
                
                
                
            }
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
            
        }
    }
}
