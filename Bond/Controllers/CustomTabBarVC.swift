//
//  CustomTabBarVC.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit

class CustomTabBarVC: UITabBarController , UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        if #available(iOS 15.0, *) {
           let appearance = UITabBarAppearance()
            
           appearance.configureWithOpaqueBackground()
           appearance.backgroundColor =  UIColor.TabBarBackgroundColor()
            self.tabBar.tintColor = UIColor.init(red: 30.0/255.0, green: 77.0/255.0, blue: 79.0/255.0, alpha: 1.0)
            appearance.selectionIndicatorTintColor = UIColor.init(red: 30.0/255.0, green: 77.0/255.0, blue: 79.0/255.0, alpha: 1.0)
            
            let tabBarItemAppearance = UITabBarItemAppearance()
            tabBarItemAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
           // appearance.selected.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
         
            appearance.stackedLayoutAppearance = tabBarItemAppearance
            
           self.tabBar.standardAppearance = appearance
            self.tabBar.scrollEdgeAppearance = self.tabBar.standardAppearance
            
            let normalAttrs: [NSAttributedString.Key: Any] = [.paragraphStyle: NSParagraphStyle.default]
            let selectedAttrs: [NSAttributedString.Key: Any] = [.paragraphStyle: NSParagraphStyle.default]
            
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = normalAttrs
            appearance.stackedLayoutAppearance.selected.titleTextAttributes = selectedAttrs
            
            
            appearance.inlineLayoutAppearance.normal.titleTextAttributes = normalAttrs
            appearance.inlineLayoutAppearance.selected.titleTextAttributes = selectedAttrs
            
            
            appearance.compactInlineLayoutAppearance.normal.titleTextAttributes = normalAttrs
            appearance.compactInlineLayoutAppearance.selected.titleTextAttributes = selectedAttrs
//
//            appearance.compactInlineLayoutAppearance.normal.iconColor = .lightText
//            appearance.compactInlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.lightText]
//
//            appearance.inlineLayoutAppearance.normal.iconColor = .lightText
//            appearance.inlineLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.lightText]
//
//            appearance.stackedLayoutAppearance.normal.iconColor = .lightText
//            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor : UIColor.lightText]
//
        
       
           
        }
        else
        {
            UITabBar.appearance().barTintColor = UIColor.TabBarBackgroundColor()
            //self.tabBar.barTintColor = UIColor.GreenColor()
            tabBar.isTranslucent = false
            
            tabBar.tintColor = UIColor.init(red: 30.0/255.0, green: 77.0/255.0, blue: 79.0/255.0, alpha: 1.0)
            tabBar.unselectedItemTintColor = .white
        }
        
        
        
        
        
        setTabBarItems()
        
        if SharedManager.getUser().IsAppLive == 0
        {
            self.viewControllers?.remove(at: 2)
        }
    }
    
    func setTabBarItems(){

          let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.image = UIImage(named: "home")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = UIImage(named: "homeselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
  
        myTabBarItem1.title = HOME.localized()
       
//          myTabBarItem1.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)

          let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
        myTabBarItem2.image = UIImage(named: "qrtab")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem2.selectedImage = UIImage(named: "qrselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
      
        myTabBarItem2.title = MY_QRS.localized()
       //   myTabBarItem2.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)


          let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = UIImage(named: "pricing")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = UIImage(named: "pricingselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        
        myTabBarItem3.title = PRICING.localized()
        //  myTabBarItem3.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)

          let myTabBarItem4 = (self.tabBar.items?[3])! as UITabBarItem
        myTabBarItem4.image = UIImage(named: "contact")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.selectedImage = UIImage(named: "contactselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem4.title = PEOPLE.localized()
//          myTabBarItem4.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let myTabBarItem5 = (self.tabBar.items?[4])! as UITabBarItem
        myTabBarItem5.image = UIImage(named: "setting")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem5.selectedImage = UIImage(named: "settingselected")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem5.title = MORE.localized()

     }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
       
        let selectedIndex = tabBarController.viewControllers?.firstIndex(of: viewController)!
        
        if selectedIndex == 1 || selectedIndex == 2 || selectedIndex == 3
        {
            if SharedManager.getUser().UserID == "" ||  SharedManager.getUser().UserID == "0"
            {
                SharedManager.showLoginScreen()
                return false
            }
            
        }
            return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
