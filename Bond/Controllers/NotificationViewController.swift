//
//  NotificationViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class NotificationViewController: ParentViewController {

    @IBOutlet weak var containerNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var notificationArray = [NotificationObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerNavBar.backgroundColor = .NAVBARCOLOR()
        self.getNotifications()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }

    
    func getNotifications()
    {
        let parameter = ["UserID" : SharedManager.getUser().UserID ] as [String : Any]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_NOTIFICATIONS, method: .get, parameters: parameter, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.notificationArray = NotificationObject.parseMultipleData(userDictionary: response)
            self.tableView.reloadData()
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension NotificationViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationTableViewCell
        cell.mainContainer.setupShadow()
        cell.containerImageView.makeViewRound()
        cell.viewReadUnRead.makeViewRound()
        let object = self.notificationArray[indexPath.row]
        
        cell.userImageView.downLoadImageIntoImageView(url: object.UserImage, placeholder: nil)
        cell.labelFullName.text = object.FullName
        cell.labelNotificationText.text = object.NotificationText
        
        cell.labelCreatedAt.text = converTimeStampIntoDate(timeStamp: object.CreatedAt)
//        if object.IsRead == "1"
//        {
//            cell.viewReadUnRead.isHidden = true
//        }
//        else
//        {
//            cell.viewReadUnRead.isHidden = false
//        }
        return cell
        
    }
}

extension NotificationViewController : UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = self.notificationArray[indexPath.row]
        
        if object.IsSocialQr == 1
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SocialProfileViewController") as? SocialProfileViewController
            {
                vc.invitationID = object.InvitationID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            {
                vc.userID = object.UserID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
}
