//
//  QRDetailViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 22/04/2021.
//

import UIKit

class QRDetailViewController: ParentViewController {

    @IBOutlet weak var labelQRTitle: LabelBoldFont!
    @IBOutlet weak var tableView: UITableView!
    
    var object = InvitationObject()
    var qrDataObject = QRDataObject()
    
    var qrDetailArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelQRTitle.text = object.Title
        
        getQRScanCount()

        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    
    func getQRScanCount()
    {
     
       
        let parameter = ["api_key" : VISUAL_LEAD_API_KEY , "id" : object.QRID ] as [String : Any]
        ApiManager.getOrPostMethodForGenerateQR(URLString: QR_API_TRACK, method: .get, parameters: parameter, isShowAI: true, mainView: self.view, isReturnErrorInSuccessBlock: false) { (response) in
            
            print(response)
            self.qrDataObject = QRDataObject.parseData(response: response)
            self.tableView.reloadData()
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }

    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension QRDetailViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else if section == 1
        {
            return qrDataObject.cityArray.count
        }
      
        return qrDataObject.dateCount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "basicInfoCell") as! QRDetailsTableViewCell
            
            cell.qrImageView.image = nil
            cell.qrImageView.downLoadImageIntoImageView(url: object.QrImage, placeholder: nil , isQRImage: false)
            
            cell.totalScanCount.text = String(describing: qrDataObject.totalCount)
            cell.iOSScanCount.text = "iOS" + ": " + String(describing: qrDataObject.iPhoneCount)
            cell.androidScanCount.text = "Android" + ": " + String(describing: qrDataObject.androidCount)
            
            
            if qrDataObject.dateCount.count == 0 && qrDataObject.cityArray.count == 0
            {
                cell.topCellView.roundCornerOfView(corners: [.topLeft , .topRight , .bottomLeft , .bottomRight], radius: 10)
            }
            else
            {
                cell.topCellView.roundCornerOfView(corners: [.topLeft , .topRight], radius: 10)
            }
           
            
            
            return cell
        }
        else if indexPath.section == 1
        {
            
            if qrDataObject.dateCount.count == 0
            {
                if indexPath.row == qrDataObject.cityArray.count - 1
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "lastScanInfoCell") as! QRDetailsTableViewCell
                    cell.labelDeviceName.text =  qrDataObject.cityArray[indexPath.row].city
                   
                    cell.bottomCellView.clipsToBounds = true
                    cell.bottomCellView.layer.cornerRadius = 10
                    
                    if #available(iOS 11.0, *) {
                        cell.bottomCellView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ]
                    } else {
                        // Fallback on earlier versions
                    }
                    
                    return cell
                }
               
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "scanInfoCell") as! QRDetailsTableViewCell
            cell.labelDeviceName.text = qrDataObject.cityArray[indexPath.row].city
            return cell
            

        }
        else
        {
          
            
            
            if indexPath.row == qrDataObject.dateCount.count - 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "lastScanInfoCell") as! QRDetailsTableViewCell
               
                let keyValue = Array(qrDataObject.dateCount)[indexPath.row].key
                let dateCount = Array(qrDataObject.dateCount)[indexPath.row].value
                cell.labelDeviceName.text = keyValue + ": " + String(describing: dateCount)
                
                cell.bottomCellView.clipsToBounds = true
                cell.bottomCellView.layer.cornerRadius = 10
                
                if #available(iOS 11.0, *) {
                    cell.bottomCellView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ]
                } else {
                    // Fallback on earlier versions
                }
                
               // cell.bottomCellView.roundCornerOfView(corners: [.bottomLeft , .bottomRight], radius: 10)
                
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "scanInfoCell") as! QRDetailsTableViewCell

           
            let keyValue = Array(qrDataObject.dateCount)[indexPath.row].key
            let dateCount = Array(qrDataObject.dateCount)[indexPath.row].value
            cell.labelDeviceName.text = keyValue + ": " + String(describing: dateCount)
            return cell
        }
    }
}


extension QRDetailViewController : UITableViewDelegate
{
 
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0
        {
            return UIView()
        }
        else
        {
            if section == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! QRDetailsTableViewCell
                cell.labelHeader.text = "Cities"
                return cell.contentView
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! QRDetailsTableViewCell
                cell.labelHeader.text = "Scanned Date"
                return cell.contentView
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 0
        }
        else if section == 1
        {
            if qrDataObject.cityArray.count == 0
            {
                return 0
            }
            return 40
        }
        else if section == 2
        {
            if qrDataObject.dateCount.count == 0
            {
                return 0
            }
            return 40
        }
        return 0
    }
}
