//
//  TermsAndConditionsViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 18/03/2021.
//

import UIKit

class TermsAndConditionsViewController: ParentViewController {

    var titleString = ""
    var descriptionString = ""
    @IBOutlet weak var labelDescription: LabelBoldFont!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    @IBOutlet weak var innerContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        innerContainer.layer.cornerRadius = 20
        
        innerContainer.layer.shadowColor = UIColor.GreenColor().cgColor
        innerContainer.layer.shadowOpacity = 1
        innerContainer.layer.shadowOffset = CGSize.zero
        innerContainer.layer.shadowRadius = 20
        
        labelTitle.text = titleString.htmlToString
        labelDescription.text = descriptionString.htmlToString
        
        getTermsAndConditions()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}


extension TermsAndConditionsViewController
{
    
    func getTermsAndConditions()
    {
        
        let parameters: [String: Any] = [ "PageID": "1"]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PAGE_DETAIL_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
           
            print(response)
            if let dict = response["page_data"] as? NSDictionary
            {
                var title = ""
                var description = ""
                
                if SharedManager.getArabic()
                {
                    title = dict["TitleAr"] as? String ?? ""
                    description = dict["DescriptionAr"] as? String ?? ""
                    
                   
                }
                else
                {
                 
                    title = dict["Title"] as? String ?? ""
                    description = dict["Description"] as? String ?? ""
                    
                }
                
          
                self.labelTitle.text = title.htmlToString
                
                let attributedString = description.htmlToString
                
                self.labelDescription.text = attributedString
               
               
            }
            
           
            
        } errorCallBack: { (error, response) in
            
         
            AlertManager.showAlert(message: error)
        }

    }
    
}
