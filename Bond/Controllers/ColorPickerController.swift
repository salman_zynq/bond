//
//  ColorPickerController.swift
//  Bond
//
//  Created by Muhammad Salman on 07/05/2021.
//

import UIKit
import Colorful


protocol COLORPICKER
{
    func colorSelected(colorHexa:String , color:UIColor)
    
   
}


class ColorPickerController: UIViewController {

    var delegate: COLORPICKER? = nil
    
    
    @IBOutlet weak var colorPicker: ColorPicker!
    
    var colorSpace: HRColorSpace = .sRGB
    
    var hexValue = ""
    var color = UIColor()
    
    var isColorSelect = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        colorPicker.addTarget(self, action: #selector(self.handleColorChanged(picker:)), for: .valueChanged)
        colorPicker.set(color: UIColor(displayP3Red: 1.0, green: 1.0, blue: 0, alpha: 1), colorSpace: colorSpace)
     
        updateColorSpaceText()
        
        handleColorChanged(picker: colorPicker)
    }
    
    @objc func handleColorChanged(picker: ColorPicker) {
        print(picker.color.description)
        
        isColorSelect = true
        
        var rgbRedValue: CGFloat = 0, rgbGreenValue: CGFloat = 0, rgbBlueValue: CGFloat = 0, a: CGFloat = 0
        picker.color.getRed(&rgbRedValue, green: &rgbGreenValue, blue: &rgbBlueValue, alpha: &a)
        print("R value is \(rgbRedValue)")
        print("G value is \(rgbGreenValue)")
        print("B value is \(rgbBlueValue)")
        
        
//        let hexString = picker.color.toHexString()
     //   print("Hexa String is \(hexString)")
        
        let red  = rgbRedValue * 255.0
        let green = rgbGreenValue * 255.0
        let blue = rgbBlueValue * 255.0
        
        
        print("R value is \(red)")
        print("G value is \(green)")
        print("B value is \(blue)")
        
        hexValue = String(format:"%02X", Int(red)) + String(format:"%02X", Int(green)) + String(format:"%02X", Int(blue))
        
    //    hexValue =  String(format: "#%02x%02x%02x", red,  green , blue)
//        hexValue = String(format:"%02X", Int(rgbRedValue)) + String(format:"%02X", Int(rgbGreenValue)) + String(format:"%02X", Int(rgbBlueValue))
        
        
        print("the hexa value is \(hexValue)")
        
        
        self.color = picker.color
        
        
        
    }
    
    
    func updateColorSpaceText() {
        switch colorSpace {
        case .extendedSRGB: break
           // colorSpaceLabel.text = "Extended sRGB"
        case .sRGB: break
            //colorSpaceLabel.text = "sRGB"
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonCancel(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonDone(_ sender: Any) {
        
        if !isColorSelect
        {
            AlertManager.showAlert(message: "Select Color")
            return
        }
        
        self.delegate?.colorSelected(colorHexa: hexValue , color: self.color)
        self.dismiss(animated: true, completion: nil)
    }
}
