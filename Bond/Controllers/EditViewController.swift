//
//  EditViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit
import PhoneNumberKit
import FlagPhoneNumber

class EditViewController: ParentViewController {
    
    @IBOutlet weak var containerImageView: UIView!
    
  
    @IBOutlet weak var labelIsPrivate: LabelBoldFont!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    
    
    @IBOutlet weak var userImageView: UIImageView!
    
    
    @IBOutlet weak var textFieldName: CustomTextField!
    
    
    @IBOutlet weak var textFieldUserName: CustomTextField!
    
    
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    @IBOutlet weak var textFieldPhoneNumber: NumberTextField!
    
    @IBOutlet weak var textViewDescription: CustomTextView!
    
    @IBOutlet weak var buttonUpdate: ButtonBold!
    
    @IBOutlet weak var buttonDiscard: ButtonBold!
    
    @IBOutlet weak var isPrivateSnapChatCheckBox: SnapchatCheckbox!
    
    
    var isPickImage = false
    var iconClick = true
  
    
    var isValidPhone = false
    var dialingCode = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
      
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true

        
    }
    
    func setUpView(){

        isPrivateSnapChatCheckBox.configureBox()
        
        
        labelTitle.text = EDIT_MY_INFORMATION.localized()
        labelIsPrivate.text = IS_PRIVATE_PROFILE.localized()

      

       
  
        buttonUpdate.setTitle(UPDATE.localized(), for: .normal)
        buttonDiscard.setTitle(DISCARD.localized(), for: .normal)

        
      
        containerImageView.makeViewRound()
        
        textFieldName.attributedPlaceholder = NSAttributedString(
            string: YOUR_NAME.localized(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
        )
        
        textFieldEmail.attributedPlaceholder = NSAttributedString(
            string: EMAIL_ADDRESS.localized(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
        )
        
        textFieldUserName.attributedPlaceholder = NSAttributedString(
            string: USERNAME.localized(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
        )
        
        
        textFieldPhoneNumber.attributedPlaceholder = NSAttributedString(
            string: MOBILE_NUMBER.localized(),
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.red]
        )
        
    
        
       
        
        textFieldPhoneNumber.setFlag(countryCode: FPNCountryCode.SA)
        
        textFieldPhoneNumber.flagButtonSize = CGSize(width: 20, height: 20)
        
        if SharedManager.getUser().FullName != ""
        {
            textFieldName.text = SharedManager.getUser().FullName
        }
        
        if SharedManager.getUser().Email != ""
        {
            textFieldEmail.text = SharedManager.getUser().Email
        }
        
        if SharedManager.getUser().UserName != ""
        {
            textFieldUserName.text = SharedManager.getUser().UserName
        }
        
      
    
        
         
        if SharedManager.getUser().Mobile != ""
        {
//            textFieldPhoneNumber.text =  PartialFormatter().formatPartial(SharedManager.getUser().Mobile)
//            textFieldPhoneNumber.updateFlag()
        }
        
        textViewDescription.text = SharedManager.getUser().Bio
        
        
        if SharedManager.getUser().Bio == ""
        {
            textViewDescription.text = BIOGRAPHY.localized()
            textViewDescription.textColor = .lightGray
        }
     
        textFieldName.isUserInteractionEnabled = true
        
        if textFieldUserName.isTextFieldEmpty()
        {
            textFieldUserName.isUserInteractionEnabled = true
        }
        else
        {
            textFieldUserName.isUserInteractionEnabled = false
        }
        
        textFieldEmail.isUserInteractionEnabled = false
       
        self.userImageView.downLoadImageIntoImageView(url: SharedManager.getUser().Image, placeholder: nil)
        
        
        if SharedManager.getUser().IsPrivate == "1"
        {
            
            //isPrivateSnapChatCheckBox.isChecked = true
            isPrivateSnapChatCheckBox.setChecked(false, animated: true)
        }
        else
        {
           // isPrivateSnapChatCheckBox.setChecked(true, animated: true)
            
            
        }
    }

    

    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func buttonCamera(_ sender: Any) {
        
        
        CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
            
            
            self.isPickImage = true
            self.userImageView.image = image
        }
    }
    

    @IBAction func buttonUpdate(_ sender: Any) {
        
        if checkAllValidaitonWorks()
        {
            self.updateApiCalled()
        }
        else
        {

        }
        
    }
}


extension EditViewController
{
    func checkAllValidaitonWorks() -> Bool
    {
        
        if textFieldName.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
        
    
        if textFieldUserName.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }

        
        
      
        if !textFieldPhoneNumber.isTextFieldEmpty()
        {
            
            if !isValidPhone
            {

                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_MOBILE.localized())
                return false
            }
           
        }
        
      
        
  
        
        
//        if textViewDescription.isTextViewEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//        
    
        
        
      return true
        
    }
    
   
}

extension EditViewController
{
    func updateApiCalled()
    {

        var mobileNumber = ""
       
        
        if isValidPhone
        {
            mobileNumber = textFieldPhoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
        }
        
       // mobileNumber = textFieldPhoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
        
      
        
        
        var parameters: [String: Any] = [ "FullName": textFieldName.text! ,  "UserID" : SharedManager.getUser().UserID,"DeviceType" : DEVICETYPE , "DeviceToken": SharedManager.getFCMTokenFromUserDefaults() , "OS" : SharedManager.getOSInfo()  , "Mobile"  : mobileNumber , "Username" : textFieldUserName.text!]
        
        if textViewDescription.text != BIOGRAPHY.localized()
        {
            parameters["Bio"] = textViewDescription.text!
        }
        else
        {
            parameters["Bio"] = ""
        }
        
        if isPrivateSnapChatCheckBox.isChecked
        {
            parameters["IsPrivate"] = "1"
        }
        else
        {
            parameters["IsPrivate"] = "0"
        }
    
        
        var imageKey = ""
        var userImage = UIImage()
        
        if isPickImage
        {
            imageKey = "Image"
            userImage = self.userImageView.image!
        }
        
        
        ApiManager.getOrPostMethodWithMultiPartsForUpdateProfile(URLString: BASE_URL+UPDATE_PROFILE_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: userImage, imageKey: imageKey, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: true) { (response) in
            
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)


            AlertManager.showAlertWithOneButton(title: "", message: response["message"] as? String ?? "N/A", buttonText: CLOSE.localized()) {
                self.navigationController?.popViewController(animated: true)
            }
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

        
    }
}



extension EditViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "" || textView.text == BIOGRAPHY.localized()
        {
            textView.text = ""
            textView.textColor = .black
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = BIOGRAPHY.localized()
            textView.textColor = .lightGray
        }
    }
    
}


extension EditViewController : FPNTextFieldDelegate
{
    func fpnDisplayCountryList() {
        
    }
    
    
    
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        dialingCode = dialCode
       // textFieldPhoneNumber.text = ""
        
        
        
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        isValidPhone = isValid
        
    }
}
