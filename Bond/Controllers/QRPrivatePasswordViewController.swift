//
//  QRPrivatePasswordViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 07/05/2021.
//

import UIKit



protocol PRIVATEPASSWORD
{
    func passwordSet(password : String)
    
   
}


class QRPrivatePasswordViewController: ParentViewController {

    
    @IBOutlet weak var buttonDone: ButtonBold!
    var delegate: PRIVATEPASSWORD? = nil
    
    @IBOutlet weak var textFieldPassword: CustomTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buttonDone.setTitle(DONE.localized(), for: .normal)
        textFieldPassword.placeholder = PASSWORD.localized()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonDone(_ sender: Any)
    {
        if textFieldPassword.isTextFieldEmpty()
        {
            AlertManager.showAlert(message: FILL_EMPTY_FIELDS.localized())
            return
        }

        self.delegate?.passwordSet(password: textFieldPassword.text!)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
