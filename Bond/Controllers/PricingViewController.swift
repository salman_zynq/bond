//
//  PricingViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class PricingViewController: ParentViewController {
    
    
    @IBOutlet weak var labelPackges: LabelBoldFont!
    
    @IBOutlet weak var tableView: UITableView!
    
    var pricingArray = [PricingObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.getPricing()
        setUpViews()
        
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        } else {
            // Fallback on earlier versions
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
    }
    
    
    func setUpViews()
    {
        labelPackges.text = PRICING.localized()
        //        textFieldSearch.placeholder = SEARCH.localized()
    }
    
    func getPricing()
    {
        
        
        
        let parameters = [String : Any]()
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PRICING_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            
            
            self.pricingArray = PricingObject.parseData(response: response)
            
            self.tableView.reloadData()
            
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
    }
    
}


extension PricingViewController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return pricingArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return pricingArray[section].freeOptionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let imageCross = UIImage.init(named: "cross")
        let imageTick = UIImage.init(named: "tick")
        
        
        var cell = PricingTableViewCell()
        
        
        
        
        
        if indexPath.section == (self.pricingArray.count - 1)
        {
            
           
            if indexPath.row == (pricingArray[indexPath.section].freeOptionArray.count - 1)
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "lastCell", for: indexPath) as! PricingTableViewCell
                
                cell.bottomContainerView.clipsToBounds = true
                cell.bottomContainerView.layer.cornerRadius = 20
                
                DispatchQueue.main.async {
                    cell.bottomContainerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ]
                }
            }
            else
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PricingTableViewCell
            }
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PricingTableViewCell
        }
        
        let object = pricingArray[indexPath.section].freeOptionArray[indexPath.row]
        let premiumObject = pricingArray[indexPath.section].premiumOptionArray[indexPath.row]
        
        print(object.Title)
        cell.labelTitle.text = object.Title
        
        cell.imageFree.image = imageCross
        cell.imagePaid.image = imageTick
        
        if object.IsFree == 1
        {
            cell.imageFree.image = imageTick
        }
        
        
        if premiumObject.IsPaid == 0
        {
            cell.imagePaid.image = imageCross
        }
        
        
        cell.freeView.isHidden = false
        

        let mainObject = pricingArray[indexPath.section]
        if mainObject.isHideFreeOption
        {
            cell.freeView.isHidden = true
        }

        
        
        return cell
        
        
        
        
        
        
        
    }
}

extension PricingViewController : UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 100
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! PricingTableViewCell
        
        let object = pricingArray[section]
        if section == 0
        {
            
            headerCell.headerContainerView.clipsToBounds = true
            headerCell.headerContainerView.layer.cornerRadius = 20
            DispatchQueue.main.async {
                headerCell.headerContainerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            }
            
            
            
        }
        else
        {
            
            
        }
        headerCell.labelHeaderTitle.text = object.Title
        
        headerCell.labelFeatures.text = FEATURES.localized()
        
        
        headerCell.labelPaid.attributedText = makeAttributedLable(complete: PREMIUM.localized() + " - " + object.Price + " " + "SAR" + " " + "(annual)", CompFont: UIFont.init(name: SharedManager.boldFont(), size: 15)!, CompColor: .black, sub:  object.Price + " " + "SAR" + " " + "(annual)", subFont: UIFont.init(name: SharedManager.boldFont(), size: 12)!, SubColor: .black)
        
        headerCell.labelFree.text = FREE.localized()
        
        headerCell.freeView.isHidden = false
        
        if object.isHideFreeOption
        {
            headerCell.freeView.isHidden = true
        }
        
        return headerCell
    }
}



