//
//  CreateWeddingFormViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 18/03/2021.
//

import UIKit
import MZFormSheetPresentationController
import FlagPhoneNumber


class CreateWeddingFormViewController: ParentViewController {
    
    
    @IBOutlet weak var labelEventInvitation: LabelBoldFont!
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    
    @IBOutlet weak var labelEnterDetails: LabelBoldFont!
    
    var backGroundImage = UIImage()
    
    @IBOutlet weak var mainContainer: UIView!
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var containerLogo: UIView!
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var labelHostName: LabelBoldFont!
    
    @IBOutlet weak var labelDescription: LabelBoldFont!
    
    
    @IBOutlet weak var textFieldUploadLogo: CustomTextField!
    
    @IBOutlet weak var textFieldTitleofInvitation: CustomTextField!
    

    @IBOutlet weak var textFieldSelectTextColor: CustomTextField!
    
    @IBOutlet weak var textFieldHostName: CustomTextField!
    
    
    @IBOutlet weak var textViewDescription: CustomTextView!
    
    @IBOutlet weak var textFieldContactInformation: NumberTextField!
    
    @IBOutlet weak var textFieldAudioLink: CustomTextField!
    
    @IBOutlet weak var textFieldYoutubeLink: CustomTextField!
    
    
    @IBOutlet weak var textFieldGoogleMap: CustomTextField!
    
    var isPickImage = false
    
    
    @IBOutlet weak var buttonProceedNext: ButtonBold!
    
    var textColorHexa = "000000"
    
    var isComingFromEdit = false
    var object = InvitationObject()
    
 
    
    
    var isValidPhone = false
    var dialingCode = ""
    
    var uploadLogoButton = UIButton()
    var uploadCoverButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        coverImageView.image = backGroundImage
        
        setUpViews()
        setUpData()
    
        
//        
//        textFieldTitleofInvitation.text = "Title"
//        textFieldHostName.text = "Host"
//        textFieldGoogleMap.text = "www.google.com"
//        textFieldAudioLink.text = "www.google.com"
//        textFieldContactInformation.text = "+966123123"
//        textViewDescription.text = "This is dummy test"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    func setUpData()
    {
        if isComingFromEdit
        {
            
            
            self.containerLogo.backgroundColor = .white
            self.containerLogo.alpha = 1.0
            self.isPickImage = true
        
            self.logoImage.downLoadImageIntoImageView(url: object.Logo, placeholder: nil)

            textFieldTitleofInvitation.text = object.Title
            textFieldHostName.text = object.HostName
            textFieldSelectTextColor.text = object.TextColor
            textViewDescription.text = object.InvitationDescription
            
            if object.ContactInfo != ""
            {
                
                textFieldContactInformation.set(phoneNumber: object.ContactInfo)

            }
         
            textFieldAudioLink.text = object.AudioLink
            textFieldYoutubeLink.text = object.YoutubeLink
            textFieldGoogleMap.text = object.GoogleLink
            textViewDescription.textColor = .black
            
            self.labelHostName.text = object.HostName
            self.labelDescription.text = object.InvitationDescription
            self.textColorHexa = object.TextColor
            
            
            
        
        }
    }
    
    
    func setUpViews()
    {
        
        
        
        
        uploadLogoButton = UIButton(type: .custom)
        uploadLogoButton.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        uploadLogoButton.frame = CGRect(x: CGFloat(textFieldUploadLogo.frame.size.width - 8), y: CGFloat(5), width: CGFloat(50), height: CGFloat(25))
        uploadLogoButton.addTarget(self, action: #selector(self.deleteLogo), for: .touchUpInside)
        uploadLogoButton.setTitleColor( UIColor.gray, for: .normal)
        uploadLogoButton.setTitleColor(UIColor.DarkRedBrown(), for: .normal)
        uploadLogoButton.setTitle(REMOVE.localized(), for: .normal)
        uploadLogoButton.isHidden = true
        
        
     

        
        
        if SharedManager.getArabic()
        {
           
            textFieldUploadLogo.leftView = uploadLogoButton
            textFieldUploadLogo.leftViewMode = .always
            
          
        }
        else
        {
         
            textFieldUploadLogo.rightView = uploadLogoButton
            textFieldUploadLogo.rightViewMode = .always
           
           
        }
        
        
        
        
        buttonProceedNext.setTitle(PROCEED_NEXT.localized(), for: .normal)
        
        if isComingFromEdit
        {
            labelCreateA.text =  EDIT_A.localized()
        }
        else
        {
            labelCreateA.text  = CREATE_A.localized()
        }
       
        labelEventInvitation.text = EVENT_INVITATION.localized()
        labelEnterDetails.text = ENTER_DETAILS.localized()
        textViewDescription.text = ENTER_INVITATION_DESCRIPTION_VENUE.localized()
        textViewDescription.textColor = .lightGray
        
        
        textFieldUploadLogo.placeholder = UPLOAD_LOGO.localized()
        textFieldTitleofInvitation.placeholder = TITLE_OF_INVITATION.localized()
        textFieldHostName.placeholder = ENTER_HOST_NAME.localized()
        
      //  textFieldContactInformation.placeholder = ENTER_CONTACT_INFORMAATION.localized()
        textFieldAudioLink.placeholder = AUIDO_MP3.localized()
        textFieldYoutubeLink.placeholder = YOUTUBE_LINK.localized()
        textFieldGoogleMap.placeholder = GOOGLE_MAP_LOCATION_SHARE_LINK.localized()
        
        textFieldSelectTextColor.placeholder = SELECT_TEXT_COLOR.localized()
        

        
        
        textFieldSelectTextColor.addLeftAndRightView(image: UIImage.init(named: "colorpickerlogo")!)
        
        
        textFieldContactInformation.setFlag(countryCode: FPNCountryCode.SA)
        
        textFieldContactInformation.flagButtonSize = CGSize(width: 20, height: 20)
      
        
    }
    
    
    
    
    @objc func deleteLogo()
    {

        self.textFieldUploadLogo.placeholder = UPLOAD_LOGO.localized()
        self.logoImage.image = nil
        self.containerLogo.backgroundColor = .black
        self.containerLogo.alpha = 0.6
        self.isPickImage = false
        self.uploadLogoButton.isHidden = true
        
    }
    
    
  
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func buttonProceedNext(_ sender: Any) {
        
        if checkAllValidaitonWorks()
        {
           
           // , "PrivateCharges" : "2" , "TotalAmount" : "200" , "IsPrivate" : "1"
            
            var mobileNumber = ""
            
            if isValidPhone
            {
                mobileNumber = textFieldContactInformation.getFormattedPhoneNumber(format: .E164) ?? ""
            }
           

            
            var parameters: [String: Any] = ["Type" : "1", "Title": textFieldTitleofInvitation.text! ,"HostName": textFieldHostName.text!,"ContactInfo": mobileNumber, "YoutubeLink" : textFieldYoutubeLink.text! , "AudioLink" : textFieldAudioLink.text! , "GoogleLink": textFieldGoogleMap.text! , "PrivateCharges" : SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges , "TotalAmount" : SharedManager.sharedInstance.selectedQrDesignPricingArray.Price , "UserID" : SharedManager.getUser().UserID , "CreatedAt" : String(describing: convertDateIntoTimeStamp(date: Date())) , "TextColor" : self.textColorHexa]
            
                parameters["IsBackgroundColor"] = "0"

            if textViewDescription.text != ENTER_INVITATION_DESCRIPTION_VENUE.localized()
            {
                parameters["Description"] = textViewDescription.text!
            }
            else
            {
                parameters["Description"] = ""
            }
            
            if isComingFromEdit
            {
                parameters["InvitationID"] = object.InvitationID
                
                var image = UIImage()
                var key = ""
                if isPickImage
                {
                    image = self.logoImage.image!
                    key = "Logo"
                }
                else
                {
                    
                }
                ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: image, imageKey: key, backgroundImage: self.backGroundImage, backgroundImageKey: "Background", qrImage: nil, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                    
                    
                    print(response)
                    
                    AlertManager.showAlertWithOneButton(message: SUCCESS.localized()) {
                        
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    
                    
                    
                    
                    
                } errorCallBack: { (error, response) in
                    
                    AlertManager.showAlert(message: error)
                }
            }
            else
            {
                if isPickImage
                {
                    parameters["Logo"] = self.logoImage.image!
                }
                
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FinalizeWeddingInvitationViewController") as? FinalizeWeddingInvitationViewController
                {
                    vc.backGroundImage = self.backGroundImage
                
                    vc.dict = parameters
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
          
        }
       
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
  

}

extension CreateWeddingFormViewController : UITextViewDelegate
{
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == ENTER_INVITATION_DESCRIPTION_VENUE.localized()
        {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
    
        self.labelDescription.text = textView.text + "\n" + textFieldContactInformation.text!
    }
}

extension CreateWeddingFormViewController : UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.isEqual(textFieldUploadLogo)
        {
            CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
                
                
                self.containerLogo.backgroundColor = .white
                self.containerLogo.alpha = 1.0
                self.isPickImage = true
                self.logoImage.image = image
        
                
                self.uploadLogoButton.isHidden = false
                self.textFieldUploadLogo.attributedPlaceholder = NSAttributedString(
                    string: UPLOADED.localized(),
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.GreenColor()]
                )
            }
            
            return false
        }
        else if textField.isEqual(textFieldSelectTextColor)
        {
            self.showColorPicker()
            return false
        }
  
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.isEqual(textFieldHostName)
        {
            self.labelHostName.text = textField.text
        }
        else if textField.isEqual(textFieldContactInformation)
        {
           
            self.labelDescription.text = textViewDescription.text + "\n" + textField.text!
        }
    }
}


extension CreateWeddingFormViewController
{
    func checkAllValidaitonWorks() -> Bool
    {
        
//        if !isPickImage
//        {
//            AlertManager.showAlert(message:FILL_EMPTY_FIELDS.localized())
//
//            return false
//        }
//
//        if textFieldTitleofInvitation.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//
//        if textFieldHostName.isTextFieldEmpty()
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
        
        
//        if textViewDescription.isTextViewEmpty() || text
//        {
//
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
//
      
        
   
        
        if !textFieldContactInformation.isTextFieldEmpty()
        {
            
            if !isValidPhone
            {

                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_MOBILE.localized())
                return false
            }
           
        }
        
      
        
   
        
//        if textFieldGoogleMap.isTextFieldEmpty()
//        {
//            
//            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
//            return false
//        }
        
     
        
        
      return true
        
    }
    
    func showColorPicker()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ColorPickerController") as? ColorPickerController
        {
            
          
            vc.delegate = self
           
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:600)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
    
   
}



extension CreateWeddingFormViewController : COLORPICKER
{
    func colorSelected(colorHexa:String , color:UIColor) {
    
        print(colorHexa)
       
        self.textFieldSelectTextColor.text = colorHexa
        self.textColorHexa =  colorHexa
     
        
        self.labelHostName.textColor  = color
        self.labelDescription.textColor  = color
     
        //self.navigationController?.popViewController(animated: true)
    }
}


extension CreateWeddingFormViewController : FPNTextFieldDelegate
{
    func fpnDisplayCountryList() {
        
    }
    
    
    
    
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        
        dialingCode = dialCode
        textFieldContactInformation.text = ""
        
        
        
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        
        isValidPhone = isValid
        
    }
}
