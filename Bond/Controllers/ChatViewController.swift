//
//  ChatViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit
import IHKeyboardAvoiding
import GrowingTextView
import IQKeyboardManager

class ChatViewController: ParentViewController {
    
    @IBOutlet weak var containerNavBar: UIView!
    
    
    @IBOutlet weak var labelTitle: LabelBoldFont!
    
 
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var textViewMessage: CustomMessageTextView!
  
    
    @IBOutlet weak var buttonSend: ButtonBold!
    
    
    var messagesArray = [MessageObject]()
    
    
    var receiverID = ""
    var senderUserName = ""
    var ChatID: String = ""
    
    @IBOutlet weak var containerGrowingView: UIView!
    @IBOutlet weak var view_height: NSLayoutConstraint!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerNavBar.backgroundColor = .NAVBARCOLOR()
        containerGrowingView.backgroundColor = .NAVBARCOLOR()
        self.startChat()

        textViewMessage.delegate = self
        textViewMessage.trimWhiteSpaceWhenEndEditing = false
        textViewMessage.font = UIFont.init(name: SharedManager.regularFont(), size: 14)
        textViewMessage.maxHeight = 200
        textViewMessage.minHeight = 80
        textViewMessage.placeholder = "Write a message".localized()
        buttonSend.setTitle(SEND.localized(), for: .normal)
       
        self.labelTitle.text = self.senderUserName
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.chatMessageReceive(notification:)), name: Notification.Name(CHAT_MESSAGE_PUSHER), object: nil)
       // setTextView()
        
        // Do any additional setup after loading the view.
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        IQKeyboardManager.shared().isEnabled = false
    }
    
    
    

   
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared().isEnabled = true
      
      
    
    }
    
    @IBAction func buttonSendMessage(_ sender: Any)
    {
        
        if textViewMessage.isTextViewEmpty()
        {
            AlertManager.showAlert(message: FILL_EMPTY_FIELDS.localized())
            return
            
        }
        self.sendMessage()
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @objc func chatMessageReceive(notification: Notification)
    {
        
        if let response = notification.object as? NSDictionary
        {
            
            print(response)
           
            if let msgData = response["ChatMessages"] as? NSDictionary
            {
              
                self.messagesArray.append(MessageObject.parseSingleData(response: response))
                self.tableView.reloadData()
                self.scrollToBottom()
                
                
            }

        }
        
    }
    
    func scrollToBottom()
    {
        DispatchQueue.main.async {
            
            if self.messagesArray.count > 0
            {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                    var indexPath = IndexPath(row: self.messagesArray.count-1 , section: 0)
//                    if self.isMoreCommentAvailable
//                    {
//                        indexPath = IndexPath(row: (self.commentArray.count) , section: 0)
//                    }
                    
                    
                    
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
                
            }
            
            
        }
    }
}

extension ChatViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = ChatTableViewCell()
        
        let object = self.messagesArray[indexPath.row]
   
        if object.SenderID == getUserIdFromUserDefualts() {
         
            cell = tableView.dequeueReusableCell(withIdentifier: "minecell") as! ChatTableViewCell
          
            cell.labelMessage.text = object.Message
            cell.labelDate.text = convertTimeStampToDate(timeStamp: object.CreatedAt)
            cell.containerImageView.makeViewRound()
            cell.containerMessageView.shadowViewWithCustomCornerRadius(cornerRadius: 10)
            cell.userImageView.downLoadImageIntoImageView(url: object.Image, placeholder: nil)
            
           
        }
        else
        {
            cell = self.tableView.dequeueReusableCell(withIdentifier: "othercell") as! ChatTableViewCell
            cell.labelMessage.text = object.Message
            cell.labelDate.text =  convertTimeStampToDate(timeStamp: object.CreatedAt)
            cell.containerImageView.makeViewRound()
            cell.containerMessageView.shadowViewWithCustomCornerRadius(cornerRadius: 10)
            cell.userImageView.downLoadImageIntoImageView(url: object.Image, placeholder: nil)
        }
        
        
   
   
        
        return cell
        
    }
    
}

extension ChatViewController : UITableViewDelegate
{
    
}



extension ChatViewController
{
    func startChat() {
        
       // UserID(this is Sender id),ReceiverID
        
        let url = BASE_URL + START_CHAT
        
        let parameters: [String: Any] = ["UserID": SharedManager.getUser().UserID, "ReceiverID": receiverID  ]
        
//        messagesDictionary.removeAll()
//        self.keysArray.removeAll()
        ApiManager.getOrPostMethod(URLString: url, method: .post, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.ChatID = response["ChatID"] as? String ?? ""
            self.messagesArray = MessageObject.parseData(response: response)
            self.tableView.reloadData()
            SharedManager.sharedInstance.openChatRoomId = self.ChatID
            self.scrollToBottom()
            
            self.markAllMessageRead()
//            if let allMessages = response["ChatMessages"] as? NSArray
//            {
//                for item in allMessages {
//
//                    if let singleItemData = item as? NSDictionary
//                    {
//                        let (key,array) = MessageObject.parseChatRoomData(response: singleItemData)
//
//                        self.keysArray.append(key)
//                        self.messagesDictionary[key] = array
//                    }
//
//
//                }
//            }
//
//            self.tableView.reloadData()
//
//            self.scrollToBottom()
//
//            if self.messagesDictionary.count > 0
//            {
//                 self.markAllMessagesRead()
//            }
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

      
        
    }
    
    
    func markAllMessageRead() {
        
       // UserID(this is Sender id),ReceiverID
        
        let url = BASE_URL + MARK_ALL_MESSAGE_READ
        
        let parameters: [String: Any] = ["ChatID": self.ChatID, "UserID": SharedManager.getUser().UserID]

        ApiManager.getOrPostMethod(URLString: url, method: .post, parameters: parameters, isShowAI: false, mainView: self.view) { (response) in
            
            print(response)

            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

      
        
    }
    
    
    func sendMessage() {
        
       // UserID(this is Sender id),ReceiverID
        
        self.view.endEditing(true)
        let url = BASE_URL + SEND_MESSAGE
        
      
        let parameters: [String: Any] = ["UserID": SharedManager.getUser().UserID, "ReceiverID": receiverID , "SenderUserName" : SharedManager.getUser().FullName , "ChatID" : self.ChatID , "Message" : textViewMessage.text! , "CreatedAt" : convertDateIntoTimeStamp(date: Date())  ]
        

        ApiManager.getOrPostMethod(URLString: url, method: .post, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.messagesArray.append(MessageObject.parseSingleData(response: response))
            self.tableView.reloadData()
            self.textViewMessage.text = ""
            self.scrollToBottom()
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

      
        
    }
}

extension ChatViewController : GrowingTextViewDelegate
{
    
    // TextView Delegate
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat)
    {
        print("the height is \(height)")
        UIView.animate(withDuration: 0.2)
        {
            self.view.layoutIfNeeded()
            
            self.view_height.constant = height
            
        }
    }
    
  
}

extension ChatViewController : UITextViewDelegate
{
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        
        
        
        
       
        
        KeyboardAvoiding.avoidingView = self.containerGrowingView
        
        
        
//        if textView.text == WRITE_A_MESSAGE.localized()
//        {
//            textView.text = ""
//            textView.textColor = UIColor.black
        
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
    }
}

