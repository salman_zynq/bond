//
//  PayWeddingInvitationViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit
import MFSDK
class PayWeddingInvitationViewController: ParentViewController {
    
    @IBOutlet weak var buttonPayNow: ButtonBold!
    @IBOutlet weak var labelFinalizeQR: LabelBoldFont!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var textFieldExpiryYear: CustomTextField!
    @IBOutlet weak var payNowStackView: UIStackView!
    @IBOutlet weak var textFieldCVV: CustomTextField!
    @IBOutlet weak var textFieldExpiry: CustomTextField!
    @IBOutlet weak var textFieldCardNumber: CustomTextField!
    @IBOutlet weak var textFieldCardHolderName: CustomTextField!
    @IBOutlet weak var finalizeWeddingInvitation: UIImageView!
    var isPaidVersion = "0"
    
    @IBOutlet weak var labelTitleCardDesign: LabelBoldFont!
    
    @IBOutlet weak var labelTitleTotalAmount: LabelBoldFont!
    @IBOutlet weak var labelQRDesignTitle: LabelBoldFont!
    @IBOutlet weak var labelTitlePrivacy: LabelBoldFont!
    
    
    
    @IBOutlet weak var labelTotalAmount: LabelRegularFont!
    @IBOutlet weak var labelQrDesignPrice: LabelRegularFont!
    @IBOutlet weak var labelPrivatePrice: LabelRegularFont!
    @IBOutlet weak var labelCardDesignPrice: LabelRegularFont!
    
    @IBOutlet weak var cardIImageView: UIImageView!
    @IBOutlet weak var labelPayNow: LabelBoldFont!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var qrDict = [String : Any]()
    var dict = [String : Any]()
    var backGroundImage = UIImage()
    var qrImage = UIImage()
    
    var paymentMethods: [MFPaymentMethod]?
    var selectedPaymentMethodIndex = -1
    
    var invitationObject = InvitationObject()
    
    var isComingForUpdatePrice = false
    
    @IBOutlet weak var outerContainer: UIView!
    
    var isPrivate = "0"
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        setUpViews()
        
        
        if isComingForUpdatePrice
        {
            
            initiatePayment()
            payNowStackView.isHidden = true
            
           
             
            labelQrDesignPrice.isHidden = true
            labelTotalAmount.isHidden = true
            labelPrivatePrice.isHidden = true
            labelTitlePrivacy.isHidden = true
            labelTitleTotalAmount.isHidden = true
            labelQRDesignTitle.isHidden = true
            
            labelCardDesignPrice.text = invitationObject.UpgradePrice + " " + "SAR".localized()
            labelTitleCardDesign.text = UPGRADE_PRICE.localized()
            
                if invitationObject.InvitationType == "1"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3130")
                }
                else if invitationObject.InvitationType == "2"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3150")
                }
                else if invitationObject.InvitationType == "3"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3243")
                }
                else if invitationObject.InvitationType == "4"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3135")
                }
                else if invitationObject.InvitationType == "5"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3134")
                }
                
                
            
            
        }
        
        else
        {
            
            var privateCharges = SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges
            if let isPrivateValue = dict["IsPrivate"] as? String
            {
                isPrivate = isPrivateValue
                if isPrivateValue == "1"
                {
                    labelPrivatePrice.text = SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges + " " + "SAR"
                }
                else
                {
                    labelPrivatePrice.text = "0" + " " + "SAR"
                    privateCharges = "0"
                }
            }
            else
            {
                isPrivate = "0"
                labelPrivatePrice.text = "0" + " " + "SAR"
                privateCharges = "0"
            }
            
            if isPaidVersion == "0" && isPrivate == "0"
            {
                payNowStackView.isHidden = true
                cardIImageView.isHidden = true
                labelPayNow.isHidden = true
                mainView.isHidden = true
                self.createInvitationApiCalled()
                
             
            }
            else if SharedManager.getUser().IsAppLive == 0 || SharedManager.getUser().CanCreateFreeQr == "1"
            {
                payNowStackView.isHidden = true
                cardIImageView.isHidden = true
                labelPayNow.isHidden = true
                mainView.isHidden = true
                self.createInvitationApiCalled()
                
            }
            else
            {
                initiatePayment()
                payNowStackView.isHidden = true
            }
            
            
         
            
            
           
            
            if isPaidVersion == "1"
            {
                
                labelQrDesignPrice.text = SharedManager.sharedInstance.selectedQrDesignPricingArray.Price + " " + "SAR"
                
                let totalAmount = (Double(SharedManager.sharedInstance.selectedQrDesignPricingArray.Price) ?? 0.0) +   (Double(privateCharges) ?? 0.0)
                
                labelTotalAmount.text = String(format: "%.2f", totalAmount) + " " + "SAR"
                
                
            }
            else
            {
                labelQrDesignPrice.text = "0" + " " + "SAR"
                let totalAmount = 0.0 +   (Double(privateCharges) ?? 0.0)
                
                labelTotalAmount.text = String(format: "%.2f", totalAmount) + " " + "SAR"
                
              
            }
            
         
          
            
          
         
          
            
            if let type = dict["Type"] as? String
            {
             
                if type == "1"
                {
                    labelCardDesignPrice.text = ""
                }
                else
                {
                    labelCardDesignPrice.isHidden = true
                    labelTitleCardDesign.isHidden = true
                }
                
                if type == "1"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3130")
                }
                else if type == "2"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3150")
                }
                else if type == "3"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3243")
                }
                else if type == "4"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3135")
                }
                else if type == "5"
                {
                    finalizeWeddingInvitation.image = UIImage.init(named: "Group 3134")
                }
                
                
            }
        }
   
        
        
       
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        outerContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        self.clearNavigationColor()
    }
    func setUpViews()
    {
        labelFinalizeQR.text = FINALIZE.localized()
        labelTitleCardDesign.text = CARD_DESIGN.localized() + ":"
        labelTitlePrivacy.text = PRIVACY.localized() + ":"
        labelTitleTotalAmount.text = TOTAL_AMOUNT.localized() + ":"
        labelQRDesignTitle.text = QR_DESIGN.localized() + ":"
        labelPayNow.text = PAY_NOW.localized()
        buttonPayNow.setTitle(PAY_NOW.localized(), for: .normal)
        
        
    
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func buttonPayNow(_ sender: Any) {
        
        
        self.view.endEditing(true)
        
        
      //For AppStore Submisstion
        
        if SharedManager.getUser().IsAppLive == 0
        {
            clickOnPayButton()
            return
            
        }
       
        
        if isPaidVersion == "1"  && isPrivate == "0" && payNowStackView.isHidden == false
        {
            if textFieldCardNumber.isTextFieldEmpty() || textFieldCVV.isTextFieldEmpty() || textFieldExpiry.isTextFieldEmpty() || textFieldCardHolderName.isTextFieldEmpty() || textFieldExpiryYear.isTextFieldEmpty()
            {
                AlertManager.showAlert(message: FILL_EMPTY_FIELDS.localized() + "Card Number: 5123450000000008")
                return
            }
        }
        
        if selectedPaymentMethodIndex == -1
        {
            AlertManager.showAlert(message: "Select payment method")
            return
        }
        
        
        clickOnPayButton()
        
      
        
  
        
        
        
    }
    
    
    
    func createInvitationApiCalled(invoiceID:String = "")
    {
        
        if isComingForUpdatePrice
        {
            var parameters: [String: Any] = ["InvoiceID" : invoiceID,  "UserID" : SharedManager.getUser().UserID , "CreatedAt" : String(describing: convertDateIntoTimeStamp(date: Date())), "Type" : invitationObject.InvitationType]
            parameters["InvitationID"] = invitationObject.InvitationID
            
            ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + UPDATE_INVITATION, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: nil, imageKey: "", backgroundImage: nil, backgroundImageKey: "", qrImage: nil, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                
                
                print(response)
                
                AlertManager.showAlertWithOneButton(message: SUCCESS.localized()) {
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
                
                
                
                
            } errorCallBack: { (error, response) in
                
                AlertManager.showAlert(message: error)
            }
        }
        else
        {
            var qrType = ""
            if let type = dict["Type"] as? String
            {
                qrType = type
            }
            
            var logoKey = ""
            var backgroundKey = ""
            var logo = UIImage()
            if qrType == "1"
            {
                logoKey = "Logo"
                backgroundKey = "Background"
                
                if let image = dict["Logo"] as? UIImage
                {
                    logo = image
                    dict.removeValue(forKey: "Logo")
                }
                
                
                
            }
            else if qrType == "2" || qrType == "3" || qrType == "4"
            {
                
            }
            else if qrType == "5"
            {
                logoKey = "Logo"
               
                
                if let image = dict["Logo"] as? UIImage
                {
                    logo = image
                    dict.removeValue(forKey: "Logo")
                }
                
                if self.backGroundImage.size.width != 0
                {
                    backgroundKey = "Background"
                }
                else
                {
                    backgroundKey = ""
                }
                
                
                
               
                
            }
            
            if SharedManager.getUser().CanCreateFreeQr == "1"
            {
                dict["InvoiceID"] = "1"
            }
            else
            {
                dict["InvoiceID"] = invoiceID
            }
            
            
           
            
            
          
            
          
            
            ApiManager.getOrPostMethodWithMultiPartsForCreateInvitations(URLString: BASE_URL + CREATE_INVITATION, method: .post, parameters: dict, isShowAI: true, mainView: self.view, image: logo, imageKey: logoKey, backgroundImage: backGroundImage, backgroundImageKey: backgroundKey, qrImage: nil, qrImageKey: "" , isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
                
                
                print(response)
                
                self.invitationObject = InvitationObject.parseSingleData(userDictionary: response)
                
                self.perform(#selector(self.createOR), with: self, afterDelay: 0.5)
                
                
                
                
                
            } errorCallBack: { (error, response) in
                
                AlertManager.showAlert(message: error)
            }
        }
        
  
    }
    @objc func createOR()
    {
        var tempQRDict = self.qrDict
        var contentUrl =  IMAGE_BASE_URL
        if invitationObject.InvitationType == "5"
        {
            contentUrl = contentUrl + "index/profile/" + invitationObject.InvitationID
        }
        else if invitationObject.InvitationType == "4"
        {
            contentUrl = contentUrl + "index/vcard/" + invitationObject.InvitationID
        }
        else
        {
            contentUrl = contentUrl + "index/invitation/" + invitationObject.InvitationID
           
           
        }
        tempQRDict["content[url]"] = contentUrl
      //  tempQRDict["image"] = self.invitationObject.QrImage
        
        ApiManager.getOrPostMethodForGenerateQR(URLString: QR_API, method: .post, parameters: tempQRDict, isShowAI: true, mainView: self.view) { (response) in
            
           
            print(response)
            let imageUrl = response["image_url"] as? String
            let qrID = response["id"] as? String
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FinalResultWeddingInvitationsViewController") as? FinalResultWeddingInvitationsViewController
            {
                vc.object = self.invitationObject
                vc.imageUrl = imageUrl ?? ""
                vc.qrID = qrID ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        } errorCallBack: { (error, response) in
            
           // AlertManager.showAlert(message: error)
            self.createOR()
        }
    }
  
    
    
    
    
    
}


extension PayWeddingInvitationViewController
{
    func clearNavigationColor()
    {
        self.navigationController?.isNavigationBarHidden = true
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .clear
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
    }
    
    func colorNavigationColor()
    {
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = .lightGray
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
    }
    
    
    func clickOnPayButton()
    {
        
        
      
        
        if let paymentMethods = paymentMethods, !paymentMethods.isEmpty {
           
                
                if paymentMethods[selectedPaymentMethodIndex].paymentMethodCode == MFPaymentMethodCode.applePay.rawValue {
                    executeApplePayPayment(paymentMethodId: paymentMethods[selectedPaymentMethodIndex].paymentMethodId)
                } else if paymentMethods[selectedPaymentMethodIndex].isDirectPayment {
                    executeDirectPayment(paymentMethodId: paymentMethods[selectedPaymentMethodIndex].paymentMethodId)
                } else {
                    executePayment(paymentMethodId: paymentMethods[selectedPaymentMethodIndex].paymentMethodId)
                }
            
        }
        
    
    }
    
    func initiatePayment() {
        
        
        let request = generateInitiatePaymentModel()
        SharedManager.showHUD()
        MFPaymentRequest.shared.initiatePayment(request: request, apiLanguage: .english, completion: { [weak self] (result) in
            SharedManager.hideHud()
            switch result {
            case .success(let initiatePaymentResponse):
                self?.paymentMethods = initiatePaymentResponse.paymentMethods
                self?.collectionView.reloadData()
            case .failure(let failError):
                AlertManager.showAlert(message: failError.errorDescription)
                
            }
        })
        

    }
    
    
    private func generateInitiatePaymentModel() -> MFInitiatePaymentRequest {
        // you can create initiate payment request with invoice value and currency
        // let invoiceValue = Double(amountTextField.text ?? "") ?? 0
        // let request = MFInitiatePaymentRequest(invoiceAmount: invoiceValue, currencyIso: .kuwait_KWD)
        // return request
        
        let request = MFInitiatePaymentRequest()
        return request
    }
    
    
    func executeDirectPayment(paymentMethodId: Int) {
        let request = getExecutePaymentRequest(paymentMethodId: paymentMethodId)
        let card = getCardInfo()
        
        MFPaymentRequest.shared.executeDirectPayment(request: request, cardInfo: card, apiLanguage: .english) { [weak self] (response, invoiceId) in
            
            switch response {
            case .success(let directPaymentResponse):
                
                
               
                self?.createInvitationApiCalled(invoiceID: invoiceId ?? "")
                
                
//                if let cardInfoResponse = directPaymentResponse.cardInfoResponse, let card = cardInfoResponse.cardInfo {
//                    AlertManager.showAlertWithOneButton(title: "Success", message: "Status: with card number: \(card.number ?? "")", buttonText: "Next") {
//
//
//                    }
//
//                }
//                if let invoiceId = invoiceId {
//
//
//                    AlertManager.showAlertWithOneButton(title: "Success", message: "Success with invoice id \(invoiceId)", buttonText: "Next") {
//
//
//                    }
//
//
//                } else {
//
//                    AlertManager.showAlertWithOneButton(title: "Success", message: "Success", buttonText: "Next") {
//
//
//                    }
//
//                }
            case .failure(let failError):
                
                
             //   AlertManager.showAlert(message: "Error: \(failError.errorDescription)")
                
                if let invoiceId = invoiceId {
                    
                    AlertManager.showAlert(message: "Fail: \(failError.statusCode) with invoice id \(invoiceId)")
                    
                } else {
                    
                    AlertManager.showAlert(message: "Fail: \(failError.statusCode)")
                    
                }
            }
        }
    }
    
    
    func executeApplePayPayment(paymentMethodId: Int) {
        let request = getExecutePaymentRequest(paymentMethodId: paymentMethodId)
       // startLoading()
        if #available(iOS 13.0, *) {
            MFPaymentRequest.shared.executeApplePayPayment(request: request, apiLanguage: .arabic) { [weak self] (response, invoiceId) in
              //  self?.stopLoading()
                switch response {
                case .success(let executePaymentResponse):
                    if let invoiceStatus = executePaymentResponse.invoiceStatus {
                        
                     
                        self?.dismiss(animated: true, completion: nil)
                        if invoiceStatus == "Paid"
                        {
                            self?.createInvitationApiCalled(invoiceID: invoiceId ?? "")
                       
                        }
                        
                    }
                case .failure(let failError):
                    AlertManager.showAlert(message: failError.errorDescription)
                    self?.dismiss(animated: true, completion: nil)
                  
                }
            }
        } else {
            // Fallback on earlier versions
            let urlScheme = "com.schopfen.bondqr"
            MFPaymentRequest.shared.executeApplePayPayment(request: request, urlScheme: urlScheme, apiLanguage: .arabic) { [weak self] response, invoiceId  in
               // self?.stopLoading()
                switch response {
                case .success(let executePaymentResponse):
                    if let invoiceStatus = executePaymentResponse.invoiceStatus {
                        
                        self?.dismiss(animated: true, completion: nil)
                        
                        if invoiceStatus == "Paid"
                        {
                            self?.createInvitationApiCalled(invoiceID: invoiceId ?? "")
                            
                        }
                     
                    }
                case .failure(let failError):
                    AlertManager.showAlert(message: failError.errorDescription)
                    self?.dismiss(animated: true, completion: nil)
                  
                }
            }
        }
    }
    
    
    func executePayment(paymentMethodId: Int) {
        let request = getExecutePaymentRequest(paymentMethodId: paymentMethodId)
       // startLoading()
        self.colorNavigationColor()
        MFPaymentRequest.shared.executePayment(request: request, apiLanguage: .arabic) { [weak self] response, invoiceId  in
           // self?.stopLoading()
            switch response {
            case .success(let executePaymentResponse):
                if let invoiceStatus = executePaymentResponse.invoiceStatus {
                    
                    //AlertManager.showAlert(message: invoiceStatus)
                    self?.dismiss(animated: true, completion: nil)
                    if invoiceStatus == "Paid"
                    {
                        self?.createInvitationApiCalled(invoiceID: invoiceId ?? "")
                       // self?.createInvitationApiCalled()
                    }
                    
                }
            case .failure(let failError):
                
               
                self?.dismiss(animated: true, completion: {
                    
                    AlertManager.showAlert(message: failError.errorDescription)
                    self?.clearNavigationColor()
                })
               
            
               
            }
        }
    }
    
    
    private func getExecutePaymentRequest(paymentMethodId: Int) -> MFExecutePaymentRequest {
        
        var invoiceValue : Decimal = 0
        if isComingForUpdatePrice
        {
            invoiceValue = Decimal(string: invitationObject.UpgradePrice ) ?? 0
        }
        else
        {
            invoiceValue = Decimal(string: labelTotalAmount.text ?? "0") ?? 0
        }
      
        let request = MFExecutePaymentRequest(invoiceValue: invoiceValue , paymentMethod: paymentMethodId)
        //request.userDefinedField = ""
        request.customerEmail = SharedManager.getUser().Email
      
        
        request.customerCivilId = SharedManager.getUser().UserID
        request.customerName = SharedManager.getUser().FullName
       // request.customerMobile = "+923323911622"
  
        request.customerReference = SharedManager.getUser().UserName
        request.language = .english
        request.mobileCountryCode = MFMobileCountryCodeISO.saudiArabia.rawValue
        request.displayCurrencyIso = .saudiArabia_SAR
    
        return request
    }
    
    private func getCardInfo() -> MFCardInfo {
        let cardNumber = textFieldCardNumber.text!
        let cardExpiryMonth = textFieldExpiry.text!
        let cardExpiryYear = textFieldExpiryYear.text!
        let cardSecureCode = textFieldCVV.text!
        let cardHolderName = textFieldCardHolderName.text!
        
        
        let card = MFCardInfo(cardNumber: cardNumber, cardExpiryMonth: cardExpiryMonth, cardExpiryYear: cardExpiryYear, cardHolderName: cardHolderName, cardSecurityCode: cardSecureCode, saveToken: false)
        card.bypass = false // default is true
        return card
    }
    
}

extension PayWeddingInvitationViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let paymentMethods = paymentMethods else {
            return 0
        }
        return paymentMethods.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! PaymentMethodCollectionViewCell
        if let paymentMethods = paymentMethods, !paymentMethods.isEmpty {
            let selectedIndex = selectedPaymentMethodIndex 
            cell.configure(paymentMethod: paymentMethods[indexPath.row], selected: selectedIndex == indexPath.row)
        }
        return cell
    }
    
}

extension PayWeddingInvitationViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedPaymentMethodIndex = indexPath.row
    //    payButton.isEnabled = true
        
        if let paymentMethods = paymentMethods {
            if paymentMethods[indexPath.row].isDirectPayment {
                
                payNowStackView.isHidden = false
                //hideCardInfoStacksView(isHidden: false)
            } else {
                
                payNowStackView.isHidden = true
                //hideCardInfoStacksView(isHidden: true)
            }
        }
        collectionView.reloadData()
    }
}
