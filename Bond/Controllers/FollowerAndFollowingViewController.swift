//
//  FollowerAndFollowingViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 02/04/2021.
//

import UIKit

class FollowerAndFollowingViewController: ParentViewController {
    
    @IBOutlet weak var containerNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var labelTitle: LabelBoldFont!
    var isFollower = false
    
    var userID =  ""
    
    var userArray = [UserObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerNavBar.backgroundColor = .NAVBARCOLOR()
        
        if isFollower
        {
            labelTitle.text = "Followers"
        }
        else
        {
            labelTitle.text = "Followings"
            
        }
        
        getFollowAndFollowing()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func getFollowAndFollowing()
    {
        
        var  url = BASE_URL
        if isFollower
        {
            url = url + GET_FOLLOWER_API
        }
        else
        {
            url = url + GET_FOLLOWING_API
        }
        
        var parameters = [String : Any]()
        
        if userID == SharedManager.getUser().UserID
        {
           parameters["UserID"] = userID
        }
        else
        {
            parameters["UserID"] = SharedManager.getUser().UserID
            parameters["OtherUserID"] = userID
        }
        
        ApiManager.getOrPostMethod(URLString: url, method: .get, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            
            if let data = response["data"] as? NSDictionary
            {
                if self.isFollower
                {
                    if let followersArray = data["followers"] as? NSArray
                    {
                        self.userArray = UserObject.parseMultipleUserData(array: followersArray)
                    }
                }
                else
                {
                    if let followingArray = data["following"] as? NSArray
                    {
                        self.userArray = UserObject.parseMultipleUserData(array: followingArray)
                    }
                }
               
               
            }
            
           
            self.tableView.reloadData()
           
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

}


extension FollowerAndFollowingViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchPeopleTableViewCell
        cell.mainContainer.setupShadow()
        let object = self.userArray[indexPath.row]
      
        cell.containerImageView.makeViewRound()
        
        cell.userImageView.image = nil
        cell.userImageView.downLoadImageIntoImageView(url: object.Image, placeholder: nil)
        cell.labelFullName.text = object.FullName
        cell.labelUserName.text = "@" + object.UserName
      
        return cell
        
    }
}

extension FollowerAndFollowingViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = self.userArray[indexPath.row]
        
        if object.UserID == SharedManager.getUser().UserID
        {
            return
        }
        if object.IsSocialQr == 1
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "SocialProfileViewController") as? SocialProfileViewController
            {
                vc.invitationID = object.InvitationID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            {
               
                vc.userID = object.UserID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
       
    }
}
