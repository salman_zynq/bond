//
//  FinalizeWeddingInvitationViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit
import MZFormSheetPresentationController

class FinalizeWeddingInvitationViewController: ParentViewController {
    
    @IBOutlet weak var buttonCreateQR: ButtonBold!
    @IBOutlet weak var labelFinalize: LabelBoldFont!
    @IBOutlet weak var labelEventInvitation: LabelBoldFont!
    @IBOutlet weak var labelCreateA: LabelBoldFont!
    var privatePassword = ""
    
    var backGroundImage = UIImage()
    
    var dict = [String : Any]()
    
    
    @IBOutlet weak var converImageView: UIImageView!
    @IBOutlet weak var labelTermsAndConditions: HyperLinkLabel!
    
    @IBOutlet weak var makePrivateSnapChatBox: SnapchatCheckbox!
    @IBOutlet weak var termsAndConditionsSnapChatBox: SnapchatCheckbox!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var containerBackgroundImage: UIView!
    @IBOutlet weak var containerLogo: UIView!
    
    @IBOutlet weak var containerLabelHostName: UIView!
    
    @IBOutlet weak var containerDescription: UIView!
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var labelHostName: LabelBoldFont!
    
    @IBOutlet weak var labelDescription: LabelBoldFont!
    
    @IBOutlet weak var labelMakePrivate: LabelBoldFont!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       

        setUpViews()
        
     
        
    
        
      
        converImageView.image = backGroundImage
      

        
    
    }
    
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mainContainer.roundCorners(corners: [.topLeft, .topRight], radius: 40)
    }
    
  
    
    
    func setUpViews()
    {
        
        
        buttonCreateQR.setTitle(CREATE_QR.localized(), for: .normal)
        labelCreateA.text  = CREATE_A.localized()
        labelEventInvitation.text = EVENT_INVITATION.localized()
        labelFinalize.text = FINALIZE.localized()
        
        labelMakePrivate.text = MAKE_PRIVATE.localized() + " (" + SharedManager.sharedInstance.selectedQrDesignPricingArray.PrivateCharges + " " + "SAR" + " " + ADDITIONAL_CHARGES_APPLY.localized() + ")"
        
        labelHostName.text = dict["HostName"] as? String ?? ""
        labelDescription.text = (dict["Description"] as! String)  + "\n" +  (dict["ContactInfo"] as! String)
        self.logoImage.image = dict["Logo"] as? UIImage
        
        
        
        if labelHostName.text == ""
        {
            containerLabelHostName.isHidden  = true
        }
        
        
        if dict["Description"] as? String == "" &&  dict["ContactInfo"] as? String == ""
        {
            containerDescription.isHidden = true
        }
        
        
        if self.logoImage.image ==  nil
        {
            containerLogo.isHidden = true
        }
        containerLogo.backgroundColor = .white
        containerLogo.alpha = 1.0
        
        containerBackgroundImage.layer.cornerRadius = 15
        containerBackgroundImage.clipsToBounds = true
        
        termsAndConditionsSnapChatBox.layer.cornerRadius = 3
        termsAndConditionsSnapChatBox.configureBox()
        
        makePrivateSnapChatBox.layer.cornerRadius = 3
        makePrivateSnapChatBox.configureBox()
        // Do any additional setup after loading the view.
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.black,
                          NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        let highlightAttributed = [NSAttributedString.Key.foregroundColor: UIColor.GreenColor(),
                                   NSAttributedString.Key.font: UIFont.init(name: SharedManager.boldFont(), size: labelTermsAndConditions.font.pointSize)]
        
        labelTermsAndConditions.linkAttributeDefault = highlightAttributed as [AnyHashable : Any]
        labelTermsAndConditions.linkAttributeHighlight = attributes as [AnyHashable : Any]
        
        labelTermsAndConditions.attributedText = NSAttributedString(string: AGREE_TERMS_AND_CONDITIONS.localized(), attributes: attributes as [NSAttributedString.Key : Any])
        
        let handler = {(hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
//            self.getTermsAndConditions()
            
            //AlertManager.showAlert(message: COMING_SOON)
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        labelTermsAndConditions.setLinksForSubstrings([TERMS_AND_CONDITIONS.localized()], withLinkHandler: handler)
    }
    @IBAction func makePrivate(_ sender: Any)
    {
        if makePrivateSnapChatBox.isChecked
        {
            self.showPassword()
        }
    }
    
    @IBAction func buttonPrivateText(_ sender: Any) {
        
        
        self.getMakePrivateText()
        
       
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonNext(_ sender: Any) {
        
//        if !termsAndConditionsSnapChatBox.isChecked
//        {
//            AlertManager.showAlert(title: ERROR.localized(), message: ACCEPT_TERMS_AND_CONDITIONS.localized())
//            return
//        }
//        else
//        {
            
            var tempDict = self.dict
            if makePrivateSnapChatBox.isChecked
            {
                if privatePassword == ""
                {
                    AlertManager.showAlert(title: ERROR.localized(), message: "Set Password".localized())
                    return
                    
                }
                tempDict["IsPrivate"] = "1"
                tempDict["InvitationPassword"] = self.privatePassword
            }
            else
            {
                tempDict["IsPrivate"] = "0"
            }
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivateAndPublicQRSettingViewController") as? PrivateAndPublicQRSettingViewController
            {
                
                vc.backGroundImage = self.backGroundImage
                vc.dict = tempDict
                self.navigationController?.pushViewController(vc, animated: true)
            }
       // }
        
    }
    
    func showPassword()
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "QRPrivatePasswordViewController") as? QRPrivatePasswordViewController
        {
            
          
            vc.delegate = self
           
            let formSheetController = MZFormSheetPresentationViewController(contentViewController: vc)
            
            
            formSheetController.presentationController?.contentViewSize = CGSize(width:UIScreen.main.bounds.size.width - 30,height:300)
            formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
            
            
            formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = false
            
            
            formSheetController.willPresentContentViewControllerHandler = { vc in
                
                vc.view.superview?.center = self.view.center;
            }
            
            
            self.present(formSheetController, animated: true, completion: nil)
            
            
        }
    }
    
  
}

extension FinalizeWeddingInvitationViewController : PRIVATEPASSWORD
{
    
    func passwordSet(password: String) {
        
        self.privatePassword = password
    }
   
}
