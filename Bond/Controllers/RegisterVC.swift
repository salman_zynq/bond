//
//  RegisterVC.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit
import PhoneNumberKit


let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

class RegisterVC: ParentViewController , UITextFieldDelegate{
    
    
    @IBOutlet weak var labelTitleRegister: LabelBoldFont!
    
    @IBOutlet weak var labelTitleNewAccount: LabelBoldFont!
    
    @IBOutlet weak var containerImageView: UIView!
    
    
    @IBOutlet weak var userImageView: UIImageView!
    
    
    @IBOutlet weak var textFieldName: CustomTextField!
    
    
    @IBOutlet weak var textFieldUserName: CustomTextField!
    
    
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    @IBOutlet weak var textFieldPhoneNumber: NumberTextField!
    
    @IBOutlet weak var textFieldPassword: CustomTextField!
    
    @IBOutlet weak var buttonRegister: ButtonBold!
    
    @IBOutlet weak var termsAndConditionsSnapChatBox: SnapchatCheckbox!
    
    @IBOutlet weak var labelTermsAndConditions: HyperLinkLabel!
    
    var isPickImage = false
    var iconClick = true
    let phoneNumberKit = PhoneNumberKit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        setUpViews()
        self.addPasswordIcon()
        
    }
    
    
    
    func setUpViews()
    {
        
        textFieldUserName.delegate = self
        
        containerImageView.makeViewRound()
        
        termsAndConditionsSnapChatBox.layer.cornerRadius = 3
        termsAndConditionsSnapChatBox.configureBox()
        // Do any additional setup after loading the view.
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                          NSAttributedString.Key.font: UIFont.init(name: SharedManager.mediumFont(), size: labelTermsAndConditions.font.pointSize)]
        
        let highlightAttributed = [NSAttributedString.Key.foregroundColor: UIColor.GreenColor(),
                                   NSAttributedString.Key.font: UIFont.init(name: SharedManager.mediumFont(), size: labelTermsAndConditions.font.pointSize)]
        
        labelTermsAndConditions.linkAttributeDefault = highlightAttributed as [AnyHashable : Any]
        labelTermsAndConditions.linkAttributeHighlight = attributes as [AnyHashable : Any]
        
        labelTermsAndConditions.attributedText = NSAttributedString(string: AGREE_TERMS_AND_CONDITIONS, attributes: attributes as [NSAttributedString.Key : Any])
        
        let handler = {(hyperLabel: FRHyperLabel?, substring: String?) -> Void in
            
            //            self.getTermsAndConditions()
            
            //AlertManager.showAlert(message: COMING_SOON)
            
            self.getTermsAndConditions()
            
            
        }
        
        labelTermsAndConditions.setLinksForSubstrings([TERMS_AND_CONDITIONS], withLinkHandler: handler)
        
        
        labelTitleRegister.text = REGISTER_A.localized()
        labelTitleNewAccount.text = NEW_ACCOUNT.localized()
        textFieldName.placeholder = YOUR_NAME.localized()
        textFieldUserName.placeholder = USERNAME.localized()
        textFieldEmail.placeholder = EMAIL.localized()
        //textFieldPhoneNumber.placeholder = MOBILE_NUMBER_PLACEHOLDER.localized()
        textFieldPassword.placeholder = PASSWORD.localized()
        buttonRegister.setTitle(SUBMIT.localized(), for: .normal)
        
    }
    
    
    func addPasswordIcon()
    {
        
        textFieldPassword.isSecureTextEntry = true
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button.frame = CGRect(x: CGFloat(textFieldPassword.frame.size.width - 8), y: CGFloat(5), width: CGFloat(10), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.passwordIcon(button:)), for: .touchUpInside)
        
        button.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        let image = UIImage(named: "eyehide")
        button.tintColor = UIColor.GreenColor()
        
        if SharedManager.getArabic()
        {
            textFieldPassword.leftView = button
            textFieldPassword.leftViewMode = .always
        }
        else
        {
            textFieldPassword.rightView = button
            textFieldPassword.rightViewMode = .always
        }
        
        
        button.setImage(image, for: .normal)
        
    }
    
    @objc func passwordIcon(button:UIButton){
        if iconClick == true{
            iconClick = false
            textFieldPassword.isSecureTextEntry = false
            button.setImage(UIImage(named: "eyeshow"), for: .normal)
            
        }
        else{
            iconClick = true
            textFieldPassword.isSecureTextEntry = true
            button.setImage(UIImage(named: "eyehide"), for: .normal)
        }
        
    }
    
    
    @IBAction func buttonPickImage(_ sender: Any)
    {
        CameraHandler.shared.selectProfileImage(viewController: self) { (image , url) in
            
            
            self.isPickImage = true
            self.userImageView.image = image
        }
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerNow(_ sender: Any) {
        
        
        if checkAllValidaitonWorks()
        {
            self.registerApiCalled()
        }
        else
        {
            
        }
        
        
    }
    
    
    
    
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.isEqual(textFieldUserName)
        {
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
    
            
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
        
    }
}



extension RegisterVC
{
    
    
    func checkAllValidaitonWorks() -> Bool
    {
        
        if textFieldName.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
        if textFieldUserName.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
        
        
        if !textFieldEmail.isEmailValid()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_EMAIL.localized())
            return false
        }
        
        if SharedManager.getUser().IsAppLive == 1
        {
            
            if !phoneNumberKit.isValidPhoneNumber(textFieldPhoneNumber.text!)
            {
                
                AlertManager.showAlert(title: ERROR.localized(), message: ENTER_VALID_MOBILE.localized())
                return false
            }
            
            
            
        }
        
        
        
        
        if textFieldPassword.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return false
        }
        
        
        if !termsAndConditionsSnapChatBox.isChecked
        {
            AlertManager.showAlert(title: ERROR.localized(), message: ACCEPT_TERMS_AND_CONDITIONS.localized())
            return false
        }
        
        
        return true
        
    }
    
    
    
    
}

extension RegisterVC
{
    
    func getTermsAndConditions()
    {
        
        let parameters: [String: Any] = [ "PageID": "1"]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_PAGE_DETAIL_API, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isLanguageKeySend: false, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            print(response)
            if let dict = response["page_data"] as? NSDictionary
            {
                var title = ""
                var description = ""
                
                if SharedManager.getArabic()
                {
                    title = dict["TitleAr"] as? String ?? ""
                    description = dict["DescriptionAr"] as? String ?? ""
                    
                    
                }
                else
                {
                    
                    title = dict["Title"] as? String ?? ""
                    description = dict["Description"] as? String ?? ""
                    
                }
                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController
                {
                    vc.titleString = title
                    vc.descriptionString = description
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
    }
    
    
    func registerApiCalled()
    {
        
        
        //        FullName,CompanyName,Bio,MerchantName,Email,Password,Image,(IOSAppVersion or AndroidAppVersion),RoleID(2 in case of merchant,3 for customer),Mobile,Location,Lat,Long,Gender(Male or Female),Dob,IDNumber,document_count,license_count,user_document1,user_document2.....,user_license1,user_license2....,CityID
        var mobileNumber = ""
        
        do {
            let phoneNumber = try phoneNumberKit.parse(textFieldPhoneNumber.text!)
            mobileNumber = phoneNumberKit.format(phoneNumber, toType: .e164)
        }
        catch {
            print("Generic parser error")
            
        }
        
        
        
        
        
        
        
        let parameters: [String: Any] = [ "FullName": textFieldName.text!, "UserName" : textFieldUserName.text!.lowercased() ,  "Email": textFieldEmail.text!,"Password": textFieldPassword.text!,"Mobile": mobileNumber, "DeviceType" : DEVICETYPE , "DeviceToken": SharedManager.getFCMTokenFromUserDefaults() , "OS" : SharedManager.getOSInfo() , "RoleID" : "2"]
        
        
        
        
        var imageKey = ""
        var userImage = UIImage()
        
        
        if isPickImage
        {
            imageKey = "Image"
            userImage = self.userImageView.image!
        }
        
        
        ApiManager.getOrPostMethodWithMultiPartsForUpdateProfile(URLString: BASE_URL + SIGN_UP_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: userImage, imageKey: imageKey, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            print(response)
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
            
            AlertManager.showAlertWithOneButton(title: SUCCESS.localized(), message: response["message"] as? String ?? "N/A", buttonText: CLOSE.localized()) {
                self.navigationController?.popViewController(animated: true)
            }
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
    }
}


extension String{
    /// To convert Arabic / Persian numbers to English
    /// - Returns: returns english number
    func changeToEnglish()-> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "EN")
        let engNumber = numberFormatter.number(from: self)
        return "\(engNumber)"
    }
    
    /// To convert English numbers to Arabic / Persian
    /// - Returns: returns Arabic number
    func changeToArabic()-> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "AR")
        let arabicNumber = numberFormatter.number(from: self)
        return "\(arabicNumber!)"
    }
}
