//
//  ChatRoomsViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 30/08/2021.
//

import UIKit

class ChatRoomsViewController: ParentViewController {

    @IBOutlet weak var containerNavBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var chatRoomArray = [MessageObject]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerNavBar.backgroundColor = .NAVBARCOLOR()
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        self.getChatRooms()
    }

    
    func getChatRooms()
    {
        let parameter = ["UserID" : SharedManager.getUser().UserID ] as [String : Any]
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_CHAT_ROOMS, method: .post, parameters: parameter, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.chatRoomArray = MessageObject.chatRoomsData(response: response)
            self.tableView.reloadData()
         
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension ChatRoomsViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatRoomArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatTableViewCell
        cell.mainContainer.setupShadow()
        cell.containerImageView.makeViewRound()
        cell.viewReadUnRead.makeViewRound()
        let object = self.chatRoomArray[indexPath.row]
        
        
        if object.ConversationSenderID ==  SharedManager.getUser().UserID
        {
            cell.userImageView.downLoadImageIntoImageView(url: object.ConversationReceiverImage, placeholder: nil)
            cell.labelFullName.text = object.ConversationReceiverName
        }
        else
        {
            cell.userImageView.downLoadImageIntoImageView(url: object.ConversationSenderImage, placeholder: nil)
            cell.labelFullName.text = object.ConversationSenderName
        }
      
        cell.labelMessage.text = object.Message
        
        cell.labelCreatedAt.text = converTimeStampIntoDate(timeStamp: object.CreatedAt)
     
        
        if object.unreadMessageCount == 0
        {
            cell.viewReadUnRead.isHidden = true
        }
        else
        {
            cell.viewReadUnRead.isHidden = false
        }
        
        
        return cell
        
    }
}

extension ChatRoomsViewController : UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        let object = self.chatRoomArray[indexPath.row]
        
        
        if object.ConversationSenderID ==  SharedManager.getUser().UserID
        {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                
                vc.receiverID = object.ConversationReceiverID
                vc.senderUserName = object.ConversationReceiverName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else
        {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                
                vc.receiverID = object.ConversationSenderID
                vc.senderUserName = object.ConversationSenderName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
