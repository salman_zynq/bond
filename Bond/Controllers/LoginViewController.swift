//
//  LoginViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 25/03/2021.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseAuth
import AuthenticationServices

class LoginViewController: ParentViewController   {
    
    
    
    @IBOutlet weak var textFieldPasswod: CustomTextField!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    
    
    @IBOutlet weak var buttonLogin: ButtonBold!
    
    @IBOutlet weak var buttonChangeLanguage: ButtonBold!
    @IBOutlet weak var buttonViewAsGuest: ButtonBold!
    @IBOutlet weak var buttonRegisterAccount: ButtonBold!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpViews()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if UserDefaults.standard.value(forKey: USERDEFAULTS_USERID) != nil
        {
            getUserData(userID: self.getUserIdFromUserDefualts())
            
        } else {
            generateToken()
        }
        
       
    }
    
    
    
    func setUpViews()
    {
        
        let button = UIButton(type: .custom)
        button.titleLabel?.font = UIFont.init(name: SharedManager.boldFont(), size: 12)
        button.frame = CGRect(x: CGFloat(textFieldPasswod.frame.size.width - 8), y: CGFloat(5), width: CGFloat(50), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.forgetPassword), for: .touchUpInside)
        
        if SharedManager.getArabic()
        {
            buttonChangeLanguage.setTitle(ENGLISH, for: .normal)
            textFieldPasswod.leftView = button
            textFieldPasswod.leftViewMode = .always
        }
        else
        {
            buttonChangeLanguage.setTitle("العربية", for: .normal)
            textFieldPasswod.rightView = button
            textFieldPasswod.rightViewMode = .always
        }
        
        button.setTitleColor( UIColor.gray, for: .normal)
        button.setTitleColor(UIColor.GreenColor(), for: .normal)
        button.setTitle(FORGOT_PASSWORD.localized(), for: .normal)
        
        textFieldEmail.placeholder = EMAIL_ADDRESS.localized()
        textFieldPasswod.placeholder = PASSWORD.localized()
        
        buttonLogin.setTitle(LOGIN.localized(), for: .normal)
        buttonRegisterAccount.setTitle(REGISTER_NEW_ACCOUNT.localized(), for: .normal)
        buttonViewAsGuest.setTitle(VIEW_AS_GUEST.localized(), for: .normal)
        
        
    }
    
    
    
    @objc func forgetPassword()
    {
        //AlertManager.showAlert(message: COMING_SOON)
        
//                if let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC")  as? ForgotPasswordVC
//                {
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
        
    }
    
    @IBAction func buttonChangeLanguage(_ sender: Any)
    {
        self.changeLanague()
    }
    
    
    
    @IBAction func buttonLogin(_ sender: Any) {
        
        
        if textFieldPasswod.isTextFieldEmpty() || textFieldEmail.isTextFieldEmpty()
        {
            
            AlertManager.showAlert(title: ERROR.localized(), message: FILL_EMPTY_FIELDS.localized())
            return
        }
        
        self.signIn()
    }
    
    @IBAction func buttonRegisterAccount(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func buttonViewAsGuest(_ sender: Any)
    {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController
        {
            vc.isGuest = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonTwitterLoggedIn(_ sender: Any)
    {
        AlertManager.showAlert(message: COMING_SOON.localized())
    }
    
    
    @IBAction func buttonGoogleSignIn(_ sender: Any)
    {
        
        let signInConfig = GIDConfiguration.init(clientID: "804026633283-ntud7b34c86so907fu3pc7r0g6pr6og4.apps.googleusercontent.com")
        
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
          // guard error == nil else { return }

            if let error = error {
                if (error as NSError).code == GIDSignInError.hasNoAuthInKeychain.rawValue {
                    print("The user has not signed in before or they have since signed out.")
                } else {
                    print("\(error.localizedDescription)")
                    AlertManager.showAlert(message: error.localizedDescription)
                }
                return
            }
            // Perform any operations on signed in user here.
            let userId = user?.userID  ?? ""            // For client-side use only!
            let idToken = user?.authentication.idToken // Safe to send to the server
            let fullName = user?.profile?.name ?? ""
            let givenName = user?.profile?.givenName
            let familyName = user?.profile?.familyName
            let email = user?.profile?.email ?? ""
            
            self.registerApiCalledForSocial(name: fullName, email: email, socialId: userId)
           // If sign in succeeded, display the app's main content View.
         }
    }
    
    @available(iOS 13.0, *)
    @IBAction func buttonAppleSignIn(_ sender: Any)
    {
        
        self.handleAppleIdRequest()
    }
    
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
//              withError error: Error!) {
//        if let error = error {
//            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                print("The user has not signed in before or they have since signed out.")
//            } else {
//                print("\(error.localizedDescription)")
//            }
//            return
//        }
//        // Perform any operations on signed in user here.
//        let userId = user.userID  ?? ""            // For client-side use only!
//        let idToken = user.authentication.idToken // Safe to send to the server
//        let fullName = user.profile.name ?? ""
//        let givenName = user.profile.givenName
//        let familyName = user.profile.familyName
//        let email = user.profile.email ?? ""
//
//
//        self.registerApiCalledForSocial(name: fullName, email: email, socialId: userId)
//
//    }
//
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    
}


extension LoginViewController
{
    func getUserData(userID: String) {
        
        
        let parameters: [String: Any] = ["UserID": userID]
        
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_USER_DETAILS, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
            
            UserDefaults.standard.set(object.UserID, forKey: USERDEFAULTS_USERID)
            
            
            self.setRootAfterLoggedIn()
            SharedManager.sharedInstance.updateTokenForUser()
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
        
    }
    
    func signIn()
    {
        
        
        self.view.endEditing(true)
        let parameters: [String: Any] = ["Email": textFieldEmail.text!, "Password": textFieldPasswod.text!, "DeviceType" : DEVICETYPE , "DeviceToken": SharedManager.getFCMTokenFromUserDefaults() , "OS" : SharedManager.getOSInfo() , "RoleID" : "2" ]
        
        
        print(parameters)
        ApiManager.getOrPostMethod(URLString: BASE_URL + LOGIN_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
            
            UserDefaults.standard.set(object.UserID, forKey: USERDEFAULTS_USERID)
            UserDefaults.standard.set(object.AuthToken, forKey: USERDEFAULTS_TOKEN)
            
            self.setRootAfterLoggedIn()
            SharedManager.sharedInstance.updateTokenForUser()
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
    }
    
    func registerApiCalledForSocial(name:String , email:String , socialId:String )
    {
        
        
        //        FullName,CompanyName,Bio,MerchantName,Email,Password,Image,(IOSAppVersion or AndroidAppVersion),RoleID(2 in case of merchant,3 for customer),Mobile,Location,Lat,Long,Gender(Male or Female),Dob,IDNumber,document_count,license_count,user_document1,user_document2.....,user_license1,user_license2....,CityID
        
        
        let parameters: [String: Any] = [ "FullName": name,  "Email": email, "DeviceType" : DEVICETYPE , "DeviceToken": SharedManager.getFCMTokenFromUserDefaults() , "OS" : SharedManager.getOSInfo() , "RoleID" : "2" , "SocialID" : socialId]
        
        
        
        
        var imageKey = ""
        var userImage = UIImage()
        
        
        
        
        ApiManager.getOrPostMethodWithMultiPartsForUpdateProfile(URLString: BASE_URL + SIGN_UP_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view, image: userImage, imageKey: imageKey, isLanguageKeySend: true, isHeaderNeeded: true, isReturnErrorInSuccessBlock: false) { (response) in
            
            print(response)
            let object = UserObject.parseData(userDictionary: response)
            SharedManager.setUser(userObj: object)
            
            
            
            UserDefaults.standard.set(object.UserID, forKey: USERDEFAULTS_USERID)
            UserDefaults.standard.set(object.AuthToken, forKey: USERDEFAULTS_TOKEN)
            
            self.setRootAfterLoggedIn()
            SharedManager.sharedInstance.updateTokenForUser()
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
    }
}



extension LoginViewController
{
    @available(iOS 13.0, *)
    func handleAppleIdRequest()
    {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    
    
}

extension LoginViewController : ASAuthorizationControllerDelegate
{
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        print("Authorization Called")
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
          
            
            let appleUserFirstName = appleIDCredential.fullName?.givenName ?? ""

            let appleUserLastName = appleIDCredential.fullName?.familyName ?? ""
            let email = appleIDCredential.email ?? ""
//            print(“User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))”)
       
//
            self.registerApiCalledForSocial(name: appleUserFirstName + " " + appleUserLastName  , email: email, socialId: userIdentifier)
            
        }
    }
        
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
        
        AlertManager.showAlert(message: error.localizedDescription)
    }
}
