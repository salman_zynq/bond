//
//  ProfileViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class ProfileViewController: ParentViewController {
    
    @IBOutlet weak var labelProfileTitle: LabelBoldFont!
    @IBOutlet weak var tableView: UITableView!
    
    var userObject = UserObject()
    var userID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        labelProfileTitle.text = MY_PROFILE.localized()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        if userID == ""
        {
            getUserData(userID: SharedManager.getUser().UserID)
        }
        else
        {
            getUserData(userID: userID)
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func backButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonChatOrEdit(_ sender: Any)
    {
        
        if userID == "" || userID == SharedManager.getUser().UserID
        {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as? EditViewController
            {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else
        {
            if let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController{
                
                vc.receiverID = self.userObject.UserID
                vc.senderUserName = self.userObject.FullName
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    @IBAction func buttonFollowers(_ sender: Any)
    {
        if userID == "" || userID == SharedManager.getUser().UserID
        {
            
            
        }
        else
        {
            if self.userObject.IsPrivate == "1"
            {
                return
            }
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowerAndFollowingViewController") as? FollowerAndFollowingViewController
        {
            vc.userID = self.userObject.UserID
            vc.isFollower = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func buttonFollowing(_ sender: Any)
    {
        if userID == "" || userID == SharedManager.getUser().UserID
        {
            
            
        }
        else
        {
            if self.userObject.IsPrivate == "1"
            {
                return
            }
        }
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowerAndFollowingViewController") as? FollowerAndFollowingViewController
        {
            vc.userID = self.userObject.UserID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func buttonFollowOrUnFollow(_ sender: Any)
    {
        self.followAndUnfollow()
    }
    
    
    func followAndUnfollow()
    {
        
        var parameters = [String : Any]()
        
        parameters["UserID"] = SharedManager.getUser().UserID
        parameters["Following"] = self.userObject.UserID
        
        if self.userObject.IsFollowing == 1
        {
            parameters["Follow"] = "0"
        }
        
        
        print(parameters)
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + FOLLOW_API, method: .post, parameters: parameters, isShowAI: true, mainView: self.view) { (response) in
            
            print(response)
            self.userObject = UserObject.parseData(userDictionary: response)
            
            self.tableView.reloadData()
            
            
        } errorCallBack: { (error, response) in
            
            AlertManager.showAlert(message: error)
        }
        
    }
    
}


extension ProfileViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if userObject.UserID == ""
        {
            return 0
        }
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! ProfileTableViewCell
            
            
            cell.topContainer.clipsToBounds = true
            cell.topContainer.layer.cornerRadius = 20
            DispatchQueue.main.async {
                cell.topContainer.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            }
            
            
            cell.containerImageView.makeViewRound()
            
            cell.labelFullName.text = self.userObject.FullName
            cell.labelUserName.text = "@" + self.userObject.UserName
            
            cell.userImageView.downLoadImageIntoImageView(url: self.userObject.Image, placeholder: nil)
            
            return cell
            
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ProfileTableViewCell
            
            if userID == "" || userID == SharedManager.getUser().UserID
            {
                cell.viewFollow.isHidden = true
                cell.buttonChatOrEdit.setTitle(EDIT_MY_INFORMATION.localized(), for: .normal)
            }
            else
            {
                if userObject.IsFollowing == 1
                {
                    cell.buttonFollow.setTitle(UNFOLLOW.localized(), for: .normal)
                    cell.viewFollow.backgroundColor = .white
                    cell.viewFollow.layer.borderColor = UIColor.GreenColor().cgColor
                    cell.viewFollow.layer.borderWidth = 1.0
                    cell.buttonFollow.setTitleColor(UIColor.GreenColor(), for: .normal)
                }
                else
                {
                    cell.buttonFollow.setTitle(FOLLOW.localized(), for: .normal)
                    cell.viewFollow.backgroundColor = .GreenColor()
                    cell.buttonFollow.setTitleColor(.white, for: .normal)
                }
                
                cell.viewFollow.isHidden = false
                
                cell.buttonChatOrEdit.setTitle(MESSAGE.localized(), for: .normal)
            }
            return cell
        }
        
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath) as! ProfileTableViewCell
            cell.labelBio.text = self.userObject.Bio
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "followCell", for: indexPath) as! ProfileTableViewCell
            
            
            cell.bottonContainer.clipsToBounds = true
            cell.bottonContainer.layer.cornerRadius = 20
            
            DispatchQueue.main.async {
                cell.bottonContainer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ]
            }
            
            
            
            
            cell.labelFollowers.text = self.userObject.TotalFollower
            cell.labelFollowings.text = self.userObject.TotalFollowing
            cell.labelFollowingHeading.text = FOLLOWING.localized()
            cell.labelFollowersHeading.text = FOLLOWERS.localized()
            return cell
        }
    }
}

extension ProfileViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 1
        {
            return 40
        }
        else if indexPath.row == 3
        {
            return 120
        }
        
        return UITableView.automaticDimension
    }
}



extension ProfileViewController
{
    func getUserData(userID: String) {
        
        
        var parameters = [String: Any]() 
        
        if userID == "" || userID == SharedManager.getUser().UserID
        {
            parameters["UserID"] = SharedManager.getUser().UserID
        }
        else
        {
            parameters["UserID"] = SharedManager.getUser().UserID
            parameters["OtherUserID"] = userID
        }
        self.view.endEditing(true)
        
        
        
        
        ApiManager.getOrPostMethod(URLString: BASE_URL + GET_USER_DETAILS, method: .get, parameters: parameters, isShowAI: true, mainView: self.view, isHeaderNeeded: true, successCallback: { (response) in
            
            
            print(response)
            self.userObject = UserObject.parseData(userDictionary: response)
            
            self.tableView.reloadData()
            
            
            
            
        }) { (error , response) in
            
            AlertManager.showAlert(message: error)
        }
        
        
        
    }
    
    
}

