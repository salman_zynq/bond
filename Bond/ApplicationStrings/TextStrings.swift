//
//  TextStrings.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation

let COMING_SOON = "Coming Soon!"

let TERMS_AND_CONDITIONS  = "I accept the Terms and Conditions"
let AGREE_TERMS_AND_CONDITIONS = "I accept the Terms and Conditions"
let SELECT_ALBUM = "Select Album"
let MY_INFORMATION = "My Information"
let CHANGE_LANGUAGE = "Change Language"
let NO_DATA_FOUND = "No Data Found"
let SQUARED = "Squared"
let ROUNDED = "Rounded"
let ENGLISH = "English"
let FORGOT = "Forgot?"
let PASSWORD = "Password"
let EMAIL = "Email"
let LOGIN = "Login"
let REGISTER_NEW_ACCOUNT = "Register New Account"
let YOUR_NAME = "Your Name"
let EMAIL_ADDRESS = "Email Address"
let MOBILE_NUMBER = "Mobile Number"
let MOBILE_NUMBER_PLACEHOLDER = "05X XXX XXXX"
let USERNAME = "Username"
let ACCEPT_TERMS_CONDITIONS = "I accept the Terms and Conditions"
let REGISTER_NOW = "Register Now"
let REGISTER_A = "Register a"
let NEW_ACCOUNT = "New Account"
let HOME = "Home"
let MY_QRS = "My QRs"
let PEOPLE = "People"
let PRICING = "Pricing"
let MORE = "More"
let FILL_EMPTY_FIELDS = "Fill empty fields"
let SEARCH_FRIENDS = "Search Friends"
let NOTIFICATIONS = "Notifications"
let BIOGRAPHY = "Biography"
let UPDATE = "Update"
let DISCARD = "Discard"
let CHANGE_PASSWORD = "Change Password"
let CURRENT_PASSWORD = "Current Password"
let NEW_PASSWORD = "New Password"
let CONFIRM_PASSWORD = "Confirm Password"
let EDIT_PROFILE = "Edit Profile"
let FOLLOWING = "Following"
let UNFOLLOW = "UnFollow"
let FOLLOWERS = "Followers"
let MY_PROFILE = "My Profile"
let PROFILE = "Profile"
let FOLLOW = "Follow"
let MESSAGE = "Message"
let REPORT = "Report"
let BLOCK_USER = "Block User"
let CHAT = "Chat"
let SELECT_BACKGROUND = "Select Background"
let ENTER_DETAILS = "Enter Details"
let DESIGN_THE_QR = "Design the QR"
let FINALIZE = "Finalize"
let CREATE_QR = "Create QR"
let MAKE_PRIVATE = "Make Private?"
let CREATED_ON = "Created On"
let PRIVATE = "Private"
let PUBLIC = "Public"
let FORGOT_PASSWORD = "Forgot Password"
let SUBMIT = "Submit"
let CLOSE = "Close"
let NO_DATE_FOUND = "No Data Found"
let EVENT_INVITATION = "Event Invitation"
let TOTAL_SCANS = "Total Scans"
let DEVICE_INFORMATION = "Device Information"
let SCANS = "Scans"
let ENTER_YOUR_MOBILE_NUMBER = "Enter your Mobile Number"
let IOS_APPLICATION_LINK = "iOS (App Store) Application Link"
let ANDROID_APPLICATION_LINK = "Android (Play Store) Application Link"
let LAST_NAAE = "Last Name"
let FIRST_NAME = "First Name"
let COMPANY = "Company"
let URL_STRING = "URL"

let BIRTHDAY = "Birthday"
let ADDRESS = "Address"
let LOCATIONS = "Locations"
let UPLOAD_LOGO = "Upload Logo"
let UPLOAD_COVER = "Upload Cover"
let IS_PRIVATE_PROFILE = "Is Private Profile?"
let PLEASE_LOGIN_FIRST = "Please Login First."
let LOGOUT = "Logout"
let QR_TITLE = "QR Title"
let WEBSITE = "Website"
let SHARE = "Share"
let UPDATE_QR = "Update QR"
let SHARE_TO = "Share To"
let SET_PASSWORD = "Set Password"
let DONE = "Done"
let PASSWORD_LENGTH_SHOULD_BE_GREATER_THAN_7 = "Password length should be greater than 7"
let PROCEED_NEXT = "Proceed Next"
let TOTAL = "Total"
let QR_LOGIN_USING = "OR Login Using"
let VIEW_AS_GUEST = "View as Guest"
let WHAT_IS_BOND = "What is Bond?"
let PASSWORD_DIDNOT_MATCH = "Passwords didnt match"
let WRITE_A_MESSAGE = "Write a Message"
let CREATE_AN = "Create an"
let APP_LINK_QR = "App Link QR"
let PLEASE_AGREE_TO_TERMS_AND_CONDITIONS = "Please agree to Terms & Conditions"
let SMS_QR = "SMS QR"
let CREATE_A = "Create a"
let EDIT_A = "Edit a"
let SOCIAL_LINK_QR = "Social Link QR"
let VCARD_QR = "VCard QR"
let ADD_YOURS = "Add Your\'s"
let SELECT_BACKGROUND_COLOR = "Select Background Color"
let UPLOAD_BACKGROUND_OR_SELECT_BACKGROUND_COLOR = "Upload Background or Select Background Color"
let SELECT_TEXT_COLOR = "Select Text Color"
let FONT_PREVIEW = "Font Preview"
let UPLOAD_WEDDING_LOGO = "Upload Wedding Logo (Optional)"
let TITLE_OF_INVITATION = "Title of Invitation"
let ENTER_HOST_NAME = "Enter Host Name"
let ENTER_INVITATION_DESCRIPTION_VENUE = "Enter Invitation Description & Venue"
let ENTER_CONTACT_INFORMAATION = "Enter Contact Information"
let AUIDO_MP3 = "Audio Mp3 (Optional)"
let YOUTUBE_LINK = "Youtube Link (Optional)"
let GOOGLE_MAP_LOCATION_SHARE_LINK = "Google Map Location Share Link"
let UPLOAD_BACKGROUND_IMAGE = "Upload Background Image"
let INVALID_EMAIL = "Invalid Email"
let EDIT_MY_INFORMATION = "Edit My Information"
let FOLLOWINGS = "Followings"
let DATES = "Dates"
let CONGRATULATIONS = "Congratulations"
let YOUR_QR_HAS_BEEN_GENERATED_SUCCESSFULLY = "Your QR Has Been Generated Successfully"
let VIEW_MY_PROFILE = "View My Profile"
let ABOUT_BOND = "About Bond"

let HELLO = "Hello"
let CREATE_A_QR_FOR = "Create a QR for"
let PAGE_DESIGN = "Page Design"
let QR_DESIGN = "QR Design"
let DELETE = "Delete"
let EVENTS = "Events"
let INVITATION = "Invitation"
let BONDING = "Bonding"
let ACCOUNT_LINKS = "Account Links"
let PERSONAL = "Personal"
let VCARD = "VCard"
let SMS = "SMS"
let APPLICATIONS = "Applications"
let LINKS = "Links"
let CARD_DESIGN = "Card Design"
let PRIVACY = "Privcay"

let TOTAL_AMOUNT = "Total Amount"
let PAY_NOW = "Pay Now"
let SCANNER = "Scanner"




let VISIT_WEBSITE = "Visit Website"
let FOLLOW_ON_FACEBOOK = "Follow On Facebook"
let FOLLOW_ON_TWITTER =  "Follow On Twitter"
let FOLLOW_ON_INSTAGRAM =  "Follow On Instagram"
let CONNECT_ON_LINKEDIN =  "Connect On LinkedIn"
let FOLLOW_ON_TIKTOK =  "Follow On TikTok"
let SUBSCRIBE_ON_YOUTUBE =  "Subscribe On Youtube"
let PREMIUM =  "Premium"
let FREE =  "Free"
let PACKAGES =  "Packages"
let SCAN_QR =  "Scan QR"
let URL_QR =  "URL QR"
let APPLICATION_LINK_QR =  "Application Link QR"
let BONDING_LINK_QR =  "Bonding Link QR"
let SCAN =  "Scan"
let ADDITIONAL_CHARGES_APPLY =  "Additional Charges Apply"
let FEATURES = "Features"


let EDIT = "Edit"
let SELECT_AN_OPTION = "Select an Option"
let GET_QR = "Get QR"
let DOWNLOAD_QR = "Download QR"
let COPY_LINK = "Copy Link"
let COPIED = "Copied"
let QR_SUCCESSFULLY_SAVED = "QR Successfully saved"
let INVALID_USERNAME_OR_URL = "Invalid Username or URL"
let INVALID_URL = "Invalid URL"
let SOCIAL_ACCOUNTS = "Accounts"
let FOLLOW_ON_SNAPCHAT =  "Follow On Snapchat"
let CHATS = "Chats"
let BIO = "Bio"
let SOCIAL_MEDIA_URL_TWITTER = "Twitter Username"
let SOCIAL_MEDIA_URL_FACEBOOK = "Facebook Username"
let SOCIAL_MEDIA_URL_YOUTUBE = "Youtube Username"
let SOCIAL_MEDIA_URL_INSTAGRAM = "Instagram Username"
let SOCIAL_MEDIA_URL_SNAPCHAT = "Snapchat Username"
let SOCIAL_MEDIA_URL_TIKTOK = "TikTok Username"
let SOCIAL_MEDIA_URL_LINKEDIN = "LinkedIn Username"


let WHATSAPP_NUMBER = "Whatsapp Number"
let CONTACT_ON_WHATSAPP = "Contact On Whatsapp"
let BOND = "Bond"
let LETS_GET_BONDING_BY = "Let's get bonding by making access to your account easier"
let CREATE_YOUR_OWN_CONTACT = "Create your own contact"
let APPS_STORE = "App's store"

let ONE_QR_FOR_YOUR_APP_IN_BOTH_STORES = "One QR for your app in both stores"
let URL_QR_DESCRIPTION = "Website, national address and etc"
let LOCATION = "Location"
let CAMERA = "Camera"
let SELECT_PHOTO = "Select Photo"


let CONTACT_US = "Contact Us"
let FREE_VERSION = "Free Version"
let PAID_VERSION = "Paid Version"


let ENTER_VALID_YOUTUBE_LINK = "Enter Valid Youtube Link"

let ENTER_VALID_LINK = "Enter Valid Link"

let TITLE = "Title"

let REMOVE = "Remove"

let UPLOADED = "Uploaded"


let UPGRADE_PRICE = "Upgrade Price"





















































































































































































































































































































































































































































































































































































































































































































































































































































