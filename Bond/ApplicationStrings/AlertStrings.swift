//
//  AlertStrings.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation

let WORKING_ON_IT = "Coming Soon!"

let ERROR = "Error"
let ALERT = "Alert"
let SUCCESS = "Success"

let UPLOAD_PROFILE_IMAGE = "Upload Profile Image"
let SELECT_IMAGE = "Select Image"
let ENTER_VALID_EMAIL = "Enter Valid Email"
let ENTER_VALID_MOBILE = "Enter Valid Number"
let PASSWORD_STRINGS = "Password should contain at least 8 Characters, at least One Capital Character, One Symbol and One Numeric Character"
let ACCEPT_TERMS_AND_CONDITIONS = "Please Accept terms and conditions"
let PASSWORD_MISMATCH = "Password Mismatch"
let CURRENT_AND_NEWS_PASSWORD_SAME = "Current And New Password Is Same"
let SELECT_ADDRESS_TO_CONTINUE = "Select address to continue"
let SELECT_SERVICE = "Select Service"
let DELETE_ADDRESS_STRING = "Are you sure you want to delete this address?"
let DELETE_ADDRESS_WARNING = "You should have atleast one address"
let SELECT_DAY = "Select Day"
let SELECT_TIME = "Select Time"

let ENGLISH_STRING = "English"
let ARABIC_STRING = "Arabic"
let APP_CLOSED_MESSAGE_WHEN_LANGUAGE_CHANGE = "This will close application and will require a restart"
let CLOSE_APP = "Close App"
let NO_BUTTON = "No"
let ARE_YOU_SURE_YOU_WANT_TO_CONTINUE = "Are you sure?"

//New String

let ARE_YOU_SURE_TO_SEND_THIS_AUDIO_NOTE = "Are you sure want to send this Audio note."
let PHOTO_LIBRARY = "Photo Library"


let STATISTICS_NOT_AVAILABLE_FOR_FREE_QR = "Statistics not available for Free QR"

