//
//  ButtonStrings.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation

let LOGO = "Logo"


let REGISTOR_NOW = "Register Now"


let CANCEL = "Cancel"
let CONFIRM = "Confirm"

let PAY = "Pay"
let SEND = "Send"
let REPORT_A_PROBLEM = "Report a Problem"
let REVIEW = "Review"

let DISCARD_CHANGES = "Discard Changes"
let UPDATE_CHANGES = "Update Changes"
let APPROVE_BUTTON = "Approve"
let REJECT_BUTTON = "Reject"
let ADD_ADDRESS = "Add Address"

let NEXT = "Next"
let VERIFY = "Verify"
let CLICK_TO_RESEND = "Click to Resend"
let PREFFERED_TIME = "Preferred Time"
let SUBMIT_REQUEST = "Submit Request"
let VIEW_PATIENT_HISTORY_BUTTON = "View Patient History"
let VIEW_PATIENT_HISTORY_BUTTON_IF_SEEKER = "View My History"
let VIEW_LOCATION_ON_MAP = "View Location On Map"
let GO_BACK = "Go Back"
let YES = "Yes"
let NO = "No"
let NOTIFY_ARRIVAL = "Notify Arrival"
let SESSION_COMPLETED = "Mark Session Completed"
let SESSION_COMPLETED_FOLLOW_UP = "Mark Follow Up Completed"
let ADD_DIAGNOSIS_COMMENTS = "Add Diagnosis / Comments"
let START_SESSION = "Start Session"
let APPLY_CHANGES = "Apply Changes"
let RESET_CHANGES = "Reset Changes"
let VIEW_DETAIL_BUTTON = "View Detail"

// New String
let PROCEED_BUTTON = "Proceed"
let BOOK_AN_APPOINTMENT = "Book an Appointment"
let VIEW_CERTIFICATES = "View Certificates"
let VIEW_LICENSE = "View Licenses"
let APPLY = "Apply"
let CHANGE_TIME = "Change Time"
let UPCOMING = "Upcoming"
let PENDING = "Pending"
let ADD_EXPERIENCE = "Add Experience"
let UPDATE_EXPERIENCE = "Update Experience"
let ADD_ANOTHER_TEST = "Add Another Test"
let SEARCH = "Search"
let AUTO = "Auto"
let MANUAL = "Manual"
let UNBLOCKED = "Unblock"
let MEDICAL_DOCUMENTS = "Medical Documents"
