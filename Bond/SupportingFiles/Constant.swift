//
//  Constant.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation




//let MY_FATOORAH_URL = "https://api.myfatoorah.com/v2/"



let CHAT_MESSAGE_PUSHER = "chatmessagepusher"
let BOND_CHANNAL_CHAT = "Bond_Channel_Chat_"
let BOND_EVENT_CHAT = "Bond_Event_Chat_"



let PUSHER_APP_ID = "1213257"
let PUSHER_KEY = "e34f469d3b5f2c3662ec"
let PUSHER_SECRET = "1e2280e71ea00149773d"
let PUSHER_CLUSTER = "eu"


// This is live key
let MY_FATOORAH_API_KEY = "yQVs6oa3CWstsmILPDR6b8xNz6Mi6WsYZXY7sNlHYTAYNPYalXFZkjrLIe_RuuuM_SSg54EhgLdNBQCEh4r_IIY9YAVyNJVo7qkuxRBoURICmMfhg_hF-3EIzHpHt1DpfPIobN9AyqPjxHU4tNVJUQh4Sqpu_uZppNfG9jHYpwCmN6pfY8Wo5db-NlL3rtuuUJSuvTdrWQVMsYzpg7DF5MSfbETotvVlbPpDUU6cRqG_-rgWIHKW7pakuQ0Rx-k7EeviYlnE22qDqWfqTo10Wx31CSApF0MJMm9ZT-sRSQrN5QXuCdarqy9QMLXYlbSsLMK1ZKoMBnj2UvNoa1igELdYkk75eZK7KhfXTs1Cry4E2KquXEznWx9w_frs1SCzrxWd8hVHmX0p3PB3vmOmln1G5x0aQyUm4-OcpBO8pUOB9UUAds8c-pxdAzEMHw73sDeOiFkOIjY5POFal2YBCjn86K5pccdQZ8bSv7t_q-Jp14lqjiFqx7UmifX_1Mt4J6It6_7YwmjyT5wyXV8_fb9orDmQvdhg7EUMlIoYVsZC2ulhaeSrmYOLWPyFVbRWFeN20qdnDzAlW_Nc31f8tqpHDWMe9yePc3HVAL4K-04musjKlPPiVo2O4JAF1iNnkmaahs3wevSjlVmAL-L0pLpeF97xsnwDihFHoNlyqbBmLkus"


// This is test key
//let MY_FATOORAH_API_KEY = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL"


//let MY_FATOORAH_URL = "https://apitest.myfatoorah.com"
let MY_FATOORAH_URL = "https://api-sa.myfatoorah.com/"




let FCM_TOKEM = "FCMTOKEN"
let LANGUAGE = "Apple"
let APPLE_LANGUAGES = "AppleLanguages"

let VISUAL_LEAD_API_KEY = "3e647d96-d7ef-7f08-f3fe-00007a86549d"

enum BOOKING_STATUS
{
    static let PENDING  = "Pending"
    static let APPROVED_BY_MERCHANT  = "Approved"
    static let CANCEL_BY_MERCHANT  = "Cancel By Merchant"
    static let CANCEL_BY_CUSTOMER  = "Cancel By Customer"
    static let PAID_BY_CUSTOMER  = "Paid"
    static let START_ACTIVITY  = "Start Activity"
    static let COMPLETED  = "Completed"
    static let UPCOMING  = "Upcoming"
}


let ADVANCE_PAYMENT_RATIO = 0.3

let LENGTH_OF_FULL_NAME = 30
let LENGTH_OF_USER_NAME = 12


let DEVICETYPE    = "iOS"
let IOS_APP_VERSION = "IOSAppVersion"
let LOCATION_RECEIVED    = "location"
let USERDEFAULTS_TOKEN = "TOKEN"
let USERDEFAULTS_USERID = "USERID"


let SERVICE_SEEKER_ROLE_ID    = 2
let SERVICE_PROVIDOR_ROLE_ID = 3



let QR_API_TRACK = "http://api.visualead.com/v3/track"



let QR_API = "http://api.visualead.com/v3/generate"
let QR_COLOR_API = "http://api.visualead.com/v3/generate_color_qr"

let BASE_URL = "https://bondqr.com/api/"
let IMAGE_BASE_URL = "https://bondqr.com/"


let GENERATE_TOKEN = "generateTokenByApi"

let SIGN_UP_API = "signUp"

let UPDATE_PROFILE_API = "updateProfile"

let SEND_OTP_API = "sendOTP"

let VERIFY_OTP_API = "verifyOTP"

let LOGIN_API = "login"

let FORGOT_PASSWORD_API = "forgotPassword"

let GET_PAGE_DETAIL_API = "getPageDetail"

let GET_QR_PAGE_DETAIL_API = "getQrPageDetail"

let GET_USER_DETAILS = "getUserDetail"


let SEARCH_USERS_API = "searchUsers"

let FOLLOW_API = "follow"

let GET_FOLLOWING_API = "getFollowing"

let GET_FOLLOWER_API = "getFollowers"


let GET_PRICING_API = "getPrices"

let GET_BACKGROUND_IMAGES = "getBackgroundImages"


let GET_MOBILE_ADS = "getMobileAds"

let GET_ACTIVITIES = "getActivities"

let GET_TOP_RATED_MERCHANTS = "getTopRatedUsers"

let GET_MERCHANT_ACTIVITIES = "getmerchantActivity"


let CREATE_BOOKING = "createBooking"


let GET_BOOKINGS = "getBookings"


let UPDATE_BOOKING = "updateBooking"

let CITIES = "cities"


let SAVE_FEEDBACK_API = "giveFeedback"

let CHANGE_PASSWORD_API = "changePassword"

let LOGOUT_API = "logout"

let USER_RATING_API = "usersRating"

let GET_USER_REVIEW_API = "getUsersReviews"

let CREATE_INVITATION = "createInvitation"

let UPDATE_INVITATION = "updateInvitation"

let GET_INVITATION = "getInvitation"

let DELETE_QR = "deleteInvitation"

let GET_NOTIFICATIONS = "getNotifications"

let START_CHAT = "startChat"

let SEND_MESSAGE = "sendMessage"

let GET_CHAT_ROOMS = "GetChatRooms"

let CHECK_IF_UNREAD_MESSAGE = "CheckIfUnreadMsgForUser"

let MARK_ALL_MESSAGE_READ = "markAllMsgReadForChat"


let GET_SOCIAL_MEDIA_PLACEHOLDER = "getSocialTypePlaceHolder"
