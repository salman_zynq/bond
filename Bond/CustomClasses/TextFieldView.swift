//
//  TextFieldView.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit

class TextFieldView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
         
         self.layer.cornerRadius = 20
         
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
         
     }

}
