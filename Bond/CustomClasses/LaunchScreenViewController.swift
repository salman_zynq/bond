//
//  LaunchScreenViewController.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3){ // change your delay here
             // redirect to next vc
            
            
            
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogIn") as? UINavigationController
            {
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
            }
           }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
