//
//  SplashGradient.swift
//  Bond
//
//  Created by Muhammad Salman on 19/03/2021.
//

import UIKit

class SplashGradient: UIView {

    override open class var layerClass: AnyClass {
         return CAGradientLayer.classForCoder()
      }

      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors =  [UIColor.init(red: 0.0/255.0, green:  68.0/255.0, blue:  58.0/255.0, alpha: 1.0).cgColor , UIColor.init(red: 0.0/255.0, green:  128.0/255.0, blue:  125.0/255.0, alpha: 1.0).cgColor]
      }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
