//
//  NumberTextField.swift
//  Bond
//
//  Created by Muhammad Salman on 15/10/2021.
//

import UIKit
import PhoneNumberKit
import FlagPhoneNumber


class NumberTextField: FPNTextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.mediumFont(), size: self.font?.pointSize ?? 15)
   
        //   self.withFlag  = true
        //   self.withExamplePlaceholder = true
        //    self.withPrefix = true
        
        self.textAlignment = .left
     
    }

}

