//
//  CustomTextField.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit

class CustomTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.mediumFont(), size: self.font?.pointSize ?? 15)
        
        
    }

}
