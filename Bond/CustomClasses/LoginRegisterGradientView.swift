//
//  LoginRegisterGradientView.swift
//  Bond
//
//  Created by Muhammad Salman on 11/06/2021.
//

import UIKit

class LoginRegisterGradientView: UIView {

    override open class var layerClass: AnyClass {
         return CAGradientLayer.classForCoder()
      }

      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [UIColor.init(red: 0.0/255.0, green:  77.0/255.0, blue:  79.0/255.0, alpha: 1.0).cgColor, UIColor.init(red: 0.0/255.0, green:  74.0/255.0, blue:  72.0/255.0, alpha: 1.0).cgColor]
      }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
