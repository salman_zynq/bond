//
//  ButtonRegular.swift
//  Boaty
//
//  Created by Muhammad Salman on 08/01/2021.
//

import UIKit

class ButtonRegular: UIButton {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font  = UIFont.init(name: SharedManager.regularFont(), size: self.titleLabel?.font.pointSize ?? 15 )
    }

}
