//
//  ButtonCancelView.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class ButtonCancelView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 20
        layer.masksToBounds = true;
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        self.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        layer.shadowRadius = 1.0;
        layer.shadowOpacity = 0.15;
        layer.shadowPath = UIBezierPath (roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath
        let bColor = self.backgroundColor?.cgColor
        layer.backgroundColor  = nil
        layer.backgroundColor = bColor
        
    }

}
