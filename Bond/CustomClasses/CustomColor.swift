//
//  CustomColor.swift
//  Boaty
//
//  Created by Macbook on 15/12/2020.
//

import UIKit

extension UIColor{
   
    
    static func DARKGREEN()  -> UIColor 
    {
        return UIColor(red: 0.0/255.0, green:  77.0/255.0, blue:  79.0/255.0, alpha: 1.0)
    }
    
    static func DarkRedBrown() -> UIColor {
        
        return UIColor(red: 221.0/255.0, green: 94.0/255.0, blue: 50.0/255.0, alpha: 1.0)
    }
    static func GreenColor() -> UIColor {
        
        return UIColor(red: 0.0/255.0, green: 224.0/255.0, blue: 189.0/255.0, alpha: 1.0)
    }
    
    static func TabBarBackgroundColor() -> UIColor {
        
        return UIColor(red: 54/255.0, green: 152.0/255.0, blue: 136.0/255.0, alpha: 1.0)
    }
    static func DarkBlue() -> UIColor {
        
        return UIColor(red: 37.0/255.0, green: 83.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    }
    static func Ferozi() -> UIColor {
        
        return UIColor(red: 106.0/255.0, green: 219.0/255.0, blue: 190.0/255.0, alpha: 1.0)
    }
    
    static func NAVBARCOLOR() -> UIColor {
        
        return UIColor(red: 39.0/255.0, green: 95.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    }

   
    

    
}
