//
//  LabelBoldFont.swift
//  Boaty
//
//  Created by Muhammad Salman on 08/01/2021.
//

import UIKit

class LabelBoldFont: UILabel {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.boldFont(), size: self.font.pointSize)
      }
   
    
    
  

}
