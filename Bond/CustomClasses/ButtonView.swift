//
//  ButtonView.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit

class ButtonView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 20
        layer.masksToBounds = true;
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        self.backgroundColor = UIColor.GreenColor()
        layer.shadowRadius = 1.0;
        layer.shadowOpacity = 0.15;
        layer.shadowPath = UIBezierPath (roundedRect: layer.bounds, cornerRadius: layer.cornerRadius).cgPath
        let bColor = self.backgroundColor?.cgColor
        layer.backgroundColor  = nil
        layer.backgroundColor = bColor
        
    }

}
