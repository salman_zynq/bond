//
//  CustomTextView.swift
//  Bond
//
//  Created by Muhammad Salman on 31/03/2021.
//

import UIKit

class CustomTextView: UITextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.mediumFont(), size: self.font?.pointSize ?? 15)
        
        
    }

}
