//
//  HyperLinkLabel.swift
//  ArtBoard
//
//  Created by Mac on 22/10/2018.
//  Copyright © 2018 Zynq. All rights reserved.
//

import UIKit

class HyperLinkLabel: FRHyperLabel {

 
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.font = UIFont.init(name: SharedManager.boldFont(), size: self.font.pointSize)
        
    }

}
