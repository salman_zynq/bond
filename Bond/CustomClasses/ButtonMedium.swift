//
//  ButtonMedium.swift
//  Boaty
//
//  Created by Muhammad Salman on 08/01/2021.
//

import UIKit

class ButtonMedium: UIButton {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font  = UIFont.init(name: SharedManager.mediumFont(), size: self.titleLabel?.font.pointSize ?? 15 )
    }

}
