//
//  LabelMediumFont.swift
//  Boaty
//
//  Created by Muhammad Salman on 08/01/2021.
//

import UIKit

class LabelMediumFont: UILabel {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.mediumFont(), size: self.font.pointSize)
      }
   

}
