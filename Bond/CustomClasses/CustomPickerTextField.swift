//
//  CustomPickerTextField.swift
//  Bond
//
//  Created by Muhammad Salman on 07/05/2021.
//

import UIKit
import AAPickerView
class CustomPickerTextField: AAPickerView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont.init(name: SharedManager.mediumFont(), size: self.font?.pointSize ?? 15)
        
        
    }

}
