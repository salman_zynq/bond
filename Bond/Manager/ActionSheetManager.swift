//
//  ActionSheetManager.swift
//  Boaty
//
//  Created by Mac on 4/15/19.
//  Copyright © 2019 Zynq. All rights reserved.
//

import UIKit
import Photos

class ActionSheetManager: UIAlertController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

    
    
    class func showActionSheetForQrRead(title: String, message: String, cameraCallBack: @escaping () -> Void , photoCallBack: @escaping () -> Void) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let cameraButton = UIAlertAction(title: CAMERA.localized(), style: .destructive) { (alert) in
            
            cameraCallBack()
            
        }
        actionSheet.addAction(cameraButton)
        
        let libraryButton = UIAlertAction(title: SELECT_PHOTO.localized(), style: .destructive) { (alert) in
            
            photoCallBack()
            
        }
        actionSheet.addAction(libraryButton)
        
      
        
        let cancelButton = UIAlertAction(title: CANCEL.localized(), style: .default) { (alert) in
            print("Cancel is pressed")
        }
        
        actionSheet.addAction(cancelButton)
        
        while ((topController!.presentedViewController) != nil) {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController {
            controller.present(actionSheet, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    class func showActionSheetForQrEditOrDelete(title: String, message: String, invoiceID:String = "" ,  downloadQR: @escaping () -> Void ,   deleteCallBack: @escaping () -> Void , editCallBack: @escaping () -> Void , shareCallBack: @escaping () -> Void , updateQRPrice: @escaping () -> Void) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        
        
        let getQRButton = UIAlertAction(title: GET_QR.localized(), style: .destructive) { (alert) in
            
            downloadQR()
            
        }
        actionSheet.addAction(getQRButton)
        
        let deleteButton = UIAlertAction(title: DELETE.localized(), style: .destructive) { (alert) in
            
            deleteCallBack()
            
        }
        actionSheet.addAction(deleteButton)
        
        let editButton = UIAlertAction(title: EDIT.localized(), style: .destructive) { (alert) in
            
            editCallBack()
            
        }
        actionSheet.addAction(editButton)
        
        let shareButton = UIAlertAction(title: SHARE.localized(), style: .destructive) { (alert) in
            
            shareCallBack()
            
        }
    
        actionSheet.addAction(shareButton)
        
      
        
        if invoiceID  == ""
        {
            let updatePrice = UIAlertAction(title: UPDATE_QR.localized(), style: .destructive) { (alert) in
                
                updateQRPrice()
                
            }
        
            actionSheet.addAction(updatePrice)
        }
        
        let cancelButton = UIAlertAction(title: CANCEL.localized(), style: .default) { (alert) in
            print("Cancel is pressed")
        }
        
        actionSheet.addAction(cancelButton)
        
        while ((topController!.presentedViewController) != nil) {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController {
            controller.present(actionSheet, animated: true, completion: nil)
        }
        
    }
    
    
    
    class func showActionSheetForDowloadQRandCopyLink(title: String, message: String, downloadQCallBack: @escaping () -> Void , copyLinkCallBack: @escaping () -> Void ) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let downloadButton = UIAlertAction(title: DOWNLOAD_QR.localized(), style: .destructive) { (alert) in
            
            downloadQCallBack()
            
        }
        actionSheet.addAction(downloadButton)
        
        let copyButton = UIAlertAction(title: COPY_LINK.localized(), style: .destructive) { (alert) in
            
            copyLinkCallBack()
            
        }
        actionSheet.addAction(copyButton)
        
     
      
        
        let cancelButton = UIAlertAction(title: CANCEL.localized(), style: .default) { (alert) in
            print("Cancel is pressed")
        }
        
        actionSheet.addAction(cancelButton)
        
        while ((topController!.presentedViewController) != nil) {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController {
            controller.present(actionSheet, animated: true, completion: nil)
        }
        
    }
}
