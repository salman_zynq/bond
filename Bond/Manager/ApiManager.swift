//
//  ApiManager.swift
//  Boothy
//
//  Created by Mac on 3/22/19.
//  Copyright © 2019 Zynq. All rights reserved.
//


import UIKit
import Alamofire


class ApiManager: NSObject {
    
    
    
    
    class func getOrPostMethod(URLString: String, method:HTTPMethod, parameters: Parameters?, isShowAI: Bool, mainView: UIView?, isLanguageKeySend : Bool = true , isHeaderNeeded:Bool = true , isReturnErrorInSuccessBlock : Bool = false ,   successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String , NSDictionary) -> Void) {
        
        
        //        if !Reachability.isConnectedToNetwork() {
        //
        //            let msg = FOR_NO_INTERNET
        //            errorCallBack(msg)
        //            return
        //
        //        }
        let url = URLString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        if isShowAI
        { 
            SharedManager.showHUD()
        }
        
        var header : HTTPHeaders = []
        if isHeaderNeeded
        {
            
            let token = UserDefaults.standard.value(forKey: USERDEFAULTS_TOKEN) as? String ?? ""
             header = ["Verifytoken": token]
            
        }
      
        
        
        
        
        
        var params = parameters
        
        params?[IOS_APP_VERSION] = getAppVersion()
        
        
     
        
        
        
        AF.request(url, method: method, parameters: params, headers:header).responseJSON { response in
            
            
            
            
            
            
            
            switch response.result {
                
                
            case .success(let value):
                
                SharedManager.hideHud()
                
                if let JSON = value as? [String: Any] {
                    
                    var message = ERROR
                    if let responseMessage = JSON["message"] as? String {
                        message = responseMessage
                    }
                    
                    if let isSTATUS = JSON["status"] {
                        
                        let STATUS = isSTATUS as? Int
                        if STATUS == 200
                        {
                            
                            successCallback(JSON as NSDictionary)
                           
                            return
                            
                        } else  {
                            
                            //SharedManager.hideHud()
                            if isReturnErrorInSuccessBlock
                            {
                                successCallback(JSON as NSDictionary)
                                
                            }
                            else
                            {
                                errorCallBack(message , JSON as NSDictionary)
                            }
                            
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                
            case .failure(let error):
                
                SharedManager.hideHud()
                print(error.responseCode)
                print(error)
                print(error.isResponseValidationError)
                errorCallBack(error.localizedDescription , [:])

            }
            
            
            
            
        }
        
        
        return
        
    }
    
    
    
    class func getOrPostMethodForGenerateQR(URLString: String, method:HTTPMethod, parameters: Parameters?, isShowAI: Bool, mainView: UIView?, isReturnErrorInSuccessBlock : Bool = false ,   successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String , NSDictionary) -> Void) {
        
        
        //        if !Reachability.isConnectedToNetwork() {
        //
        //            let msg = FOR_NO_INTERNET
        //            errorCallBack(msg)
        //            return
        //
        //        }
        let url = URLString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        if isShowAI
        {
            SharedManager.showHUD()
        }
        
  
        
        
        AF.request(url, method: method, parameters: parameters).responseJSON { response in
            
            
            
            
            
            
            
            switch response.result {
                
                
            case .success(let value):
                
                SharedManager.hideHud()
                if let JSON = value as? [String: Any] {
                    
                    
                   
                    successCallback(JSON as NSDictionary)
                    
              
                    
                    
                    
                }
                
                
            case .failure(let error):
                
                SharedManager.hideHud()
               
                errorCallBack(error.localizedDescription , [:])

            }
            
            
            
            
        }
        
        
        return
        
    }
    
    
    
    class func getOrPostMethodWithMultiPartsForUpdateProfile(URLString: String, method:HTTPMethod, parameters: Parameters?, isShowAI: Bool, mainView: UIView?, image:UIImage? , imageKey:String = "", imageExtension:String = "jpg", isLanguageKeySend : Bool = true , isHeaderNeeded:Bool = true , isReturnErrorInSuccessBlock : Bool = false ,   successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String , NSDictionary) -> Void) {
        
        
        let url = URLString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        if isShowAI
        {
            SharedManager.showHUD()
        }
        
        var header : HTTPHeaders = []
        if isHeaderNeeded
        {
            
            let token = UserDefaults.standard.value(forKey: USERDEFAULTS_TOKEN) as? String ?? ""
             header = ["Verifytoken": token]
            
        }
      
        
        
        
        
        
        var params = parameters
        
        
        params?[IOS_APP_VERSION] = getAppVersion()
        
        
        let URL = try! URLRequest(url: url, method: method, headers: header)
        
        AF.upload(multipartFormData: { multiPart in
            
            if let paramatersDictionary = params
            {
                for (key, value) in paramatersDictionary
                {
                    multiPart.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
             
            }
            
            
            
            
            if imageKey != "" {
                
                
                
                if let singleImageData = image?.pngData()
                {
                    multiPart.append(singleImageData, withName: imageKey, fileName: imageKey + "." + imageExtension, mimeType: "image/\(imageExtension)")
                }
                
                
                
                
            }
            
          
            
            
        }, with: URL)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { response in
                //Do what ever you want to do with response
                
                SharedManager.hideHud()
                switch response.result {
                    
                    
                case .success(let value):
                    
                    
                    if let JSON = value as? [String: Any] {
                        var message = ERROR
                        if let responseMessage = JSON["message"] as? String {
                            message = responseMessage
                        }
                        
                        if let isSTATUS = JSON["status"] {
                            
                            let STATUS = isSTATUS as? Int
                            if STATUS == 200 {
                                
                                successCallback(JSON as NSDictionary)
                                return
                                
                            } else  {
                                
                                
                                SharedManager.hideHud()
                                if isReturnErrorInSuccessBlock
                                {
                                    successCallback(JSON as NSDictionary)
                                    
                                }
                                else
                                {
                                    errorCallBack(message , JSON as NSDictionary)
                                }
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                case .failure(let error):
                    
                    errorCallBack(error.localizedDescription,  [:])
                    
                    
                    
                    
                }
                
                
                
            })
        
        
        
        
        
   
        
    }
    
    
    
    
    class func getOrPostMethodWithMultiPartsForCreateInvitations(URLString: String, method:HTTPMethod, parameters: Parameters?, isShowAI: Bool, mainView: UIView?, image:UIImage? , imageKey:String = "", backgroundImage:UIImage? , backgroundImageKey:String = "" , qrImage:UIImage? , qrImageKey:String = "" , isLanguageKeySend : Bool = true , isHeaderNeeded:Bool = true , isReturnErrorInSuccessBlock : Bool = false ,   successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String , NSDictionary) -> Void) {
        
        
        let url = URLString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        if isShowAI
        {
            SharedManager.showHUD()
        }
        
        var header : HTTPHeaders = []
        if isHeaderNeeded
        {
            
            let token = UserDefaults.standard.value(forKey: USERDEFAULTS_TOKEN) as? String ?? ""
             header = ["Verifytoken": token]
            
        }
      
        
        
        
        
        
        var params = parameters
        
        
        params?[IOS_APP_VERSION] = getAppVersion()
        
        
        let URL = try! URLRequest(url: url, method: method, headers: header)
        
        AF.upload(multipartFormData: { multiPart in
            
            if let paramatersDictionary = params
            {
                for (key, value) in paramatersDictionary
                {
                    multiPart.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
             
            }
            
            
            
            
            if imageKey != "" {
                
                
                
                if let singleImageData = image?.jpegData(compressionQuality: 0.6)
                {
                    multiPart.append(singleImageData, withName: imageKey, fileName: "\(imageKey).jpg", mimeType: "image/jpg")
                }
                
                
                
                
            }
            
            
            if backgroundImageKey != "" {
                

                
                if let singleImageData = backgroundImage?.jpegData(compressionQuality: 0.6)
                {
                    multiPart.append(singleImageData, withName: backgroundImageKey, fileName: "\(backgroundImageKey).jpg", mimeType: "image/jpg")
                }
                
                
                
                
            }
            
            if qrImageKey != "" {
                

                
                if let singleImageData = qrImage?.jpegData(compressionQuality: 0.6)
                {
                    multiPart.append(singleImageData, withName: qrImageKey, fileName: "\(qrImageKey).jpg", mimeType: "image/jpg")
                }
                
                
                
                
            }
            
          
            
            
        }, with: URL)
            .uploadProgress(queue: .main, closure: { progress in
                //Current upload progress of file
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { response in
                //Do what ever you want to do with response
                
                SharedManager.hideHud()
                switch response.result {
                    
                    
                case .success(let value):
                    
                    
                    if let JSON = value as? [String: Any] {
                        var message = ERROR
                        if let responseMessage = JSON["message"] as? String {
                            message = responseMessage
                        }
                        
                        if let isSTATUS = JSON["status"] {
                            
                            let STATUS = isSTATUS as? Int
                            if STATUS == 200 {
                                
                                successCallback(JSON as NSDictionary)
                                return
                                
                            } else  {
                                
                                
                                SharedManager.hideHud()
                                if isReturnErrorInSuccessBlock
                                {
                                    successCallback(JSON as NSDictionary)
                                    
                                }
                                else
                                {
                                    errorCallBack(message , JSON as NSDictionary)
                                }
                                
                            }
                            
                        }
                        
                        
                        
                    }
                    
                    
                case .failure(let error):
                    
                    errorCallBack(error.localizedDescription,  [:])
                    
                    
                    
                    
                }
                
                
                
            })
        
        
        
        
        
   
        
    }
    
    
    
    class func alamofirePostRequestNewToken(parameters: Parameters, isShowAI: Bool, mainView: UIView, successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String) -> Void) {
        
        
        var url = BASE_URL + GENERATE_TOKEN
        url = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        
        if isShowAI
        {
            SharedManager.showHUD()
        }
      
   
        
        let header: HTTPHeaders = ["Newtoken": "true"]
      
        
        
        var params = parameters
        
        params[IOS_APP_VERSION] = getAppVersion()
        
        AF.request(url, method: .post, parameters: params, headers: header).responseJSON { response in
            
            print(response)
       
            
            
            switch response.result {
                
                
            case .success(let value):
                
                
                if let JSON = value as? [String: Any] {
                    var message = ERROR
                    if let responseMessage = JSON["message"] as? String {
                        message = responseMessage
                    }
                    
                    if let isSTATUS = JSON["status"] {
                        
                        let STATUS = isSTATUS as? Int
                        if STATUS == 200
                        {
                            SharedManager.hideHud()
                            successCallback(JSON as NSDictionary)
                           
                            return
                            
                        }
                        else if STATUS == 401 {
                            
                          //  ApiManager.logOutFromDevice()
                            return
                        }
                        
                        else  {
                            
                            SharedManager.hideHud()
                            errorCallBack(message)
                            
                         
                            
                            
                        }
                        
                    }
                    
                    
                    
                }
                
                
            case .failure(let error):
                
                SharedManager.hideHud()
                errorCallBack(error.localizedDescription)

            }
           
            

            
        }
        
    }

    
    class func getAppVersion() -> String
    {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"]!
        return version as! String
    }
    
    
    
    
    class func logOutFromDevice()
    {
        guard let window = UIApplication.shared.keyWindow else { return }
        
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight, animations: {
            
            
          
            UserDefaults.standard.removeObject(forKey: USERDEFAULTS_TOKEN)
            UserDefaults.standard.removeObject(forKey: USERDEFAULTS_USERID)
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logout")
            window.rootViewController = vc
            
        }, completion: nil)
    }
    
    
    
    
    
    
    
    
    
}


