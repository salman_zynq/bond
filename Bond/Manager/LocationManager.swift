//
//  LocationManager.swift
//  Boaty
//
//  Created by Muhammad Salman on 10/03/2021.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance = LocationManager()
    
    let locationManager = CLLocationManager()
    
    var lat: Double = 0.0
    var long: Double = 0.0
    
    override init() {
        super.init()
        print("Called Only One Time")
        
        
        
    }
    
    
    func getUserLocation()
    {
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        
       // hideKeyboard()
        
        (lat, long) = SharedManager.getLocation()

        checkLocation()
        
    }
    
    func checkLocation() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case  .restricted, .denied:
                
                AlertManager.showAlertWithTwoButtons(title: "Alert", message: "App is not able to access your location, give permission to continue", firstButtonTitle: "Settings", successCallBack: {
                    
                    if let url = NSURL(string: UIApplication.openSettingsURLString) as URL? {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                    
                }) {
                    
                }
                
            case .authorizedAlways, .authorizedWhenInUse:
                getCurrentLocation()
            case .notDetermined:
                print("User is thinking")
            @unknown default: break
                
            }
        } else {
            
            AlertManager.showAlertWithOneButton(message: "Your device location is currently disabled, kindly turn on your device's location to continue.") {
                
                
                
            }
           
            
        }
        
    }
    
    func getCurrentLocation() {
        
      
       
        
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
   
        
    
        
     
        
        print(locations)
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        lat = coord.latitude
        long = coord.longitude
        
        SharedManager.setLocation(latitude: lat, longitude: long)
        
        print(lat, long)
  
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        self.mapView.camera = GMSCameraPosition(latitude: self.lat, longitude: self.long, zoom: 15, bearing: 0, viewingAngle: 0)
//    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       // AlertManager.showAlert(title: "Error", message: error.localizedDescription)
    }

}
