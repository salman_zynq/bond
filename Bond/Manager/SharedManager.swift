//
//  SharedManager.swift
//  Bond
//
//  Created by Muhammad Salman on 24/02/2021.
//

import UIKit
import FirebaseMessaging
import FirebaseInstanceID
import PusherSwift


var isArabicLanguage : Bool = false
var pusher: Pusher!

class SharedManager: NSObject {


    
    
    static let sharedInstance = SharedManager()
    
    var openChatRoomId = ""
   
    
    static let hud = LottieHUD("13838-green-share")
    
    var userProfileData = UserObject()
    
    var selectedQrDesignPricingArray = PricingObject()
    var selectedPageDesignPricingArray = PricingObject()
    

    var lat: Double = 0.0
    var long: Double = 0.0
    
    var isRefereshDataForNotificationTab = false
    
    var userTapOnNotification = false
    
    override init() {
        super.init()
        print("Called Only One Time")
        
        
        
    }
    
    
    
    
    class func setArabicPage(isArabic:Bool)
    {
        isArabicLanguage = isArabic
        
        
    }
    
    class  func getArabic()-> Bool
    {
        return isArabicLanguage
        
    }
    
    
    @objc class func regularFont() -> String {
        return "Avenir"
    }
    
    @objc class func mediumFont() -> String {
        return "Avenir-Medium"
    }
    
    @objc class func boldFont() -> String {
        return "Avenir-Heavy"
    }

    class func setUser(userObj: UserObject) {
        SharedManager.sharedInstance.userProfileData = userObj
    }

    class func getUser() -> UserObject {
        return  SharedManager.sharedInstance.userProfileData
    }

    
    
    class func setLocation(latitude: Double, longitude: Double) {
        sharedInstance.lat = latitude
        sharedInstance.long = longitude
    }

    class func getLocation() -> (Double, Double) {
        return (sharedInstance.lat, sharedInstance.long)
    }
    
    
    
    
    
    class func iSDarkModeEnable() -> Bool
    {
        if #available(iOS 13.0, *) {
            
            switch UIScreen.main.traitCollection.userInterfaceStyle {
            case .light , .unspecified :
                
                return false
                
                //light mode
                
            case .dark:
                return true
                
                
            //the user interface style is not specified
            @unknown default:
                
                return false
                
            }
            
        } else {
            
            return false
            // Fallback on earlier versions
        }
    }
    
    class func getOSInfo()->String
    {
        let os = ProcessInfo().operatingSystemVersion
        return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
    }
    
    class func getFCMTokenFromUserDefaults() -> String {
        
        if let fcmToken = UserDefaults.standard.value(forKey: FCM_TOKEM) {
            
            return fcmToken as! String
            
        } else {
            return Messaging.messaging().fcmToken ?? ""
        }
        
    }
    
    class func getToken()  {
        
        InstanceID.instanceID().instanceID { (result, error) in
            
            if let error = error {
                
                
                print("Error fetching remote instance ID: \(error)")
                
            } else if let result = result {
                
                print("Remote instance ID token: \(result.token)")
                
                
                UserDefaults.standard.set(result.token, forKey: FCM_TOKEM)
                UserDefaults.standard.synchronize()
                
            }
            
        }
        
    }
    
    func updateTokenForUser()  {
        
        
        if SharedManager.getUser().UserID != "" && SharedManager.getUser().UserID != "0"
        {
            let parameters: [String: Any] = ["DeviceType" : DEVICETYPE , "DeviceToken": SharedManager.getFCMTokenFromUserDefaults() , "OS" : SharedManager.getOSInfo() , "UserID": String(describing:SharedManager.getUser().UserID)]
          
            
            ApiManager.getOrPostMethod(URLString: BASE_URL + UPDATE_PROFILE_API, method: .post, parameters: parameters, isShowAI: false, mainView: nil , isHeaderNeeded: true , successCallback: { (response) in
                
                
                print(response)
                
                
            }) { (error , response) in
                
                
                //AlertManager.showAlert(message: error)
            }
            
        }
        
    }
    

    class func showHUD()
    {

        CustomLoader.sharedInstance.startAnimation()
        
       // hud.showHUD()
    }


    class func hideHud()
    {
        CustomLoader.sharedInstance.stopAnimation()
       // hud.stopHUD()
    }
    
    
    
    class func showLoginScreen()
    {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "LogIn") as? UINavigationController
        {
            
          
            
            let appsDelegate = UIApplication.shared.delegate
            
            appsDelegate?.window??.rootViewController = nil
            appsDelegate?.window??.rootViewController = vc
            

        }
    }

    
    class func createPusherConnection() {
        
        if pusher != nil {
            
            let connectionState = pusher.connection.connectionState
            if connectionState.rawValue == 1 {
                print("pusher already connected............")
                return
            }
        }
        
        let options = PusherClientOptions(
            host: .cluster(PUSHER_CLUSTER)
        )
        
        pusher = Pusher(key: PUSHER_KEY, options: options)
        pusher.connect()
        
        let channel_chat = BOND_CHANNAL_CHAT + getUser().UserID
        let myChannel = pusher.subscribe(channel_chat)
        
        let event_chat = BOND_EVENT_CHAT + getUser().UserID
        
        let _ = myChannel.bind(eventName: event_chat, callback: { (data: Any?) -> Void in
            
            if let data = data as? [String : AnyObject] {
                
                print(data)
                
                let vc = UIApplication.topViewController()
                
                
                if let messageDictionary = data["ChatMessages"] as? NSDictionary
                {
                    let tempObject = MessageObject.parseSingleData(response: data as NSDictionary)
                    if (vc is ChatViewController)
                    {
                        
                        
                        if tempObject.ChatID == SharedManager.sharedInstance.openChatRoomId
                        {
                            if tempObject.SenderID == SharedManager.getUser().UserID
                            {
                                
                                print("it's my message")
                            }
                            else
                            {
                                NotificationCenter.default.post(name: Notification.Name(CHAT_MESSAGE_PUSHER), object: data)
                            }
                            
                        }
                        
                        
                    }
                    else
                    {
                        if tempObject.SenderID != SharedManager.getUser().UserID
                        {
//                            if SharedManager.getUser().LastState == tempObject.ReceiverType
//                            {
//
//                            }
                          
                            
//                            var string = "Sent an Image"
//                            if tempObject.CompressedMessageImage == ""
//                            {
//                                string = tempObject.Message
//                            }
//                            let banner = GrowingNotificationBanner(title: tempObject.SenderName, subtitle: string, style: .none)
//                            banner.haptic = .light
//                            banner.backgroundColor = UIColor.init(red: 0/255.0, green:  0/255.0, blue:  0/255.0, alpha: 0.5)
//                            banner.titleLabel?.font = UIFont.init(name: SharedManager.heavyFont(), size: 14)
//                            banner.titleLabel?.textColor = .white
//                            banner.subtitleLabel?.font = UIFont.init(name: SharedManager.regularFont(), size: 12)
//                            banner.subtitleLabel?.textColor = .white
//                            banner.duration = 0.5
//                            banner.tag = Int(tempObject.SenderID) ?? 0
//                            banner.show()
//
//                            banner.onTap = {
//                                print("The tag of banner is \(banner.tag)")
//                                print(banner.tag)
//
//                                let chatVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatLogVC") as! chatLogVC
//                                chatVC.receiverID = String(describing:banner.tag)
//                                vc?.navigationController?.pushViewController(chatVC, animated: true)
//                            }
                        }
                        
                        
                    }
                }
                
                
            }
            
        })
        
    }
   
    
}
