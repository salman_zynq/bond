//
//  AlertManager.swift
//  Boaty
//
//  Created by Mac on 10/04/2019.
//  Copyright © 2019 Zynq. All rights reserved.
//

import UIKit
import Localize_Swift
//import Toast_Swift

class AlertManager: UIAlertController {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    class func showAlertWithTwoButtons(title: String, message: String, firstButtonTitle:String, successCallBack: @escaping () -> Void, errorCallBack: @escaping () -> Void ) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let alert = UIAlertController(title: "".localized(), message: message.localized(), preferredStyle: .alert)
        
        let yesButton = UIAlertAction(title: firstButtonTitle, style: .destructive) { (alert) in
            
            successCallBack()
            
        }
        alert.addAction(yesButton)
        
        let noButton = UIAlertAction(title: CLOSE.localized(), style: .default) { (alert) in
            
            errorCallBack()
            
        }
        
        alert.addAction(noButton)
        
        while ((topController!.presentedViewController) != nil) {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController {
            controller.present(alert, animated: true, completion: nil)
        }
        
    }
    
    class func showAlert(title: String = "", message: String , buttonText : String = "Close") {
        
        var topController =  appDelegate.window?.topViewController()
        
        let alert = UIAlertController(title: title.localized(), message: message.localized(), preferredStyle: .alert)
        
        alert.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.boldFont(), size: 15)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        
        alert.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.regularFont(), size: 13)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        
        
        
        
        
        let cancelButton = UIAlertAction(title: CLOSE.localized(), style: .cancel, handler: nil)
        alert.addAction(cancelButton)
        
        while ((topController!.presentedViewController) != nil)
        {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController
        {
            controller.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    class func showAlertWithOneButton(title: String = "", message: String , buttonText : String = "Close" ,  successCallBack: @escaping () -> Void) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let alert = UIAlertController(title: title.localized(), message: message.localized(), preferredStyle: .alert)
        
        alert.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.boldFont(), size: 15)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        
        alert.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.regularFont(), size: 13)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        
        let cancelButton = UIAlertAction(title: CLOSE.localized(), style: .default) { (alert) in
            
            successCallBack()
            
        }
        
        alert.addAction(cancelButton)
        
        while ((topController!.presentedViewController) != nil)
        {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController
        {
            controller.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    
    class func showAlertWithTwoCustomButtons(title: String = "", message: String, firstButtonTitle:String, secondButtonTitle:String, firstButtonAlertStyle : UIAlertAction.Style , secondButtonAlertStyle : UIAlertAction.Style ,  firstButtonCallBack: @escaping () -> Void, secondButtonCallBack: @escaping () -> Void ) {
        
        var topController =  appDelegate.window?.topViewController()
        
        let alert = UIAlertController(title: title.localized(), message: message.localized(), preferredStyle: .alert)
        
        alert.setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.boldFont(), size: 15)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        
        alert.setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.font : UIFont(name: SharedManager.regularFont(), size: 13)!, NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        
        let firstButton = UIAlertAction(title: firstButtonTitle, style: firstButtonAlertStyle) { (alert) in
            
            firstButtonCallBack()
            
        }
        alert.addAction(firstButton)
        
        let secondButton = UIAlertAction(title: secondButtonTitle, style: secondButtonAlertStyle) { (alert) in
            
            secondButtonCallBack()
            
        }
        
        alert.addAction(secondButton)
        
        while ((topController!.presentedViewController) != nil) {
            topController = topController!.presentedViewController!
        }
        
        if let controller = topController {
            controller.present(alert, animated: true, completion: nil)
        }
        
    }
    
//
//        class func showToast(message: String , view:UIView) {
//
//            // create a new style
//            var style = ToastStyle()
//
//            // this is just one of many style options
//            style.messageColor = .white
//            style.messageFont = UIFont.init(name: SharedManager.heavyFont(), size: 16)!
//            style.backgroundColor = UIColor.black
//            style.horizontalPadding = 20
//
//
//            // present the toast with the new style
//            view.makeToast(message, duration: 2.0, position: .bottom, style: style)
//
//
//            // toggle "tap to dismiss" functionality
//            ToastManager.shared.isTapToDismissEnabled = true
//
//            // toggle queueing behavior
//            ToastManager.shared.isQueueEnabled = false
//
//        }
    
}
