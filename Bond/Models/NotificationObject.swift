//
//  NotificationObject.swift
//  Bond
//
//  Created by Muhammad Salman on 07/05/2021.
//

import UIKit

class NotificationObject: NSObject {

    var CreatedAt: String = ""
    var FullName: String = ""
    var InvitationID: String = ""
    var IsRead: String = ""
    var IsSocialQr: Int = 0
    var LoggedInFullName: String = ""
    var LoggedInUserID: String = ""
    var LoggedInUserImage: String = ""
    var NotificationText: String = ""
    var NotificationType: String = ""
    var YoutubeLink: String = ""
    var UserID : String = ""
    var UserImage : String = ""
    var UserNotificationID : String = ""
   
    

    
    
    class func parseMultipleData(userDictionary: NSDictionary) -> [NotificationObject] {
        
        
        
        var tempArray = [NotificationObject]()
       
        
        if let array = userDictionary["notifications"] as? NSArray
        {
            for item in array
            {
                if let dict = item as? NSDictionary
                {
                    let obj = NotificationObject()
                    obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
                    obj.FullName = dict["FullName"] as? String ?? ""
                    obj.InvitationID = dict["InvitationID"] as? String ?? ""
                    obj.IsRead = dict["IsRead"] as? String ?? ""
                    obj.IsSocialQr = dict["IsSocialQr"] as? Int ?? 0
                    obj.LoggedInFullName = dict["LoggedInFullName"] as? String ?? ""
                    obj.LoggedInUserID = dict["LoggedInUserID"] as? String ?? "0"
                    obj.LoggedInUserImage = dict["LoggedInUserImage"] as? String ?? "0"
                    obj.NotificationType = dict["Type"] as? String ?? "0"
                    obj.UserID = dict["UserID"] as? String ?? "0"
                    obj.UserImage = dict["UserImage"] as? String ?? ""
                    obj.UserNotificationID = dict["UserNotificationID"] as? String ?? ""
                    
                    
                    if SharedManager.getArabic()
                    {
                        obj.NotificationText = dict["NotificationTextAr"] as? String ?? ""
                    }
                    else
                    {
                        obj.NotificationText = dict["NotificationTextEn"] as? String ?? ""
                    }
                 
                    
                    tempArray.append(obj)
                }
             
            }
            
         
            
           
          
        }
        
        
        return tempArray
        
    }
    
}
