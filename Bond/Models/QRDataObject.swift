//
//  QRDataObject.swift
//  Bond
//
//  Created by Muhammad Salman on 22/04/2021.
//

import UIKit

class QRDataObject: NSObject {
    
    
    var dateCount = [String:Any]()
    var city : String = ""
    var cityArray = [QRDataObject]()
    var iPhoneCount : Int = 0
    var androidCount : Int = 0
    var totalCount : Int = 0

    class func parseData(response: NSDictionary) -> QRDataObject {
        
        let object = QRDataObject()
        
        if let tempDateCountDict = response["date_counts"] as? NSDictionary
        {
            object.dateCount = tempDateCountDict as! [String : Any]
        }
        
        
        if let tempDeviceCountDict = response["devices"] as? NSDictionary
        {
            object.iPhoneCount = tempDeviceCountDict["iPhone"] as? Int ?? 0
        }
        
        
        if let tempLocationArray = response["geo_location"] as? NSArray
        {
            for item in tempLocationArray
            {
                if let dict = item as? NSDictionary
                {
                    let tempObject = QRDataObject()
                    tempObject.city = dict["city"] as? String ?? ""
                    object.cityArray.append(tempObject)
                }
            }
           
        }
        
        object.totalCount = response["total_count"] as? Int ?? 0
       
        
        return object
        
    }
    
}
