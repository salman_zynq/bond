//
//  BackgroundImageObject.swift
//  Bond
//
//  Created by Muhammad Salman on 28/04/2021.
//

import UIKit

class BackgroundImageObject: NSObject {

    var BackgroundImage: String = ""
    var Background_imagesID: String = ""
   
    
    
    class func parseData(response: NSDictionary) -> [BackgroundImageObject] {
        
        
        var tempArray = [BackgroundImageObject]()
     
        
        if let array = response["background_images"] as? NSArray
        {
            for item in array
            {
                if let dict = item as? NSDictionary
                {
                    let object = BackgroundImageObject()
                    object.BackgroundImage = dict["BackgroundImage"] as? String ?? ""
                    object.Background_imagesID = dict["Background_imagesID"] as? String ?? ""
                    tempArray.append(object)
                }
            }
        }
        
        return tempArray
        
    }
    
}
