//
//  CategoryObject.swift
//  Bond
//
//  Created by Muhammad Salman on 25/03/2021.
//

import UIKit

class CategoryObject: NSObject {
    
    
    var ActivityID: String = ""
    var ActivityImage: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
  
    
    var IsActive: String = ""
    var Hide: String = ""
    var SubTitle: String = "0"
    var Title: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    
  
    
    
    class func parseData(response: NSDictionary) -> [CategoryObject] {
        
        var array = [CategoryObject]()
        if let reponseArray = response["activities"] as? NSArray
        {
            for item in reponseArray
            {
                
                if let dict = item as? NSDictionary
                {
                    let object = CategoryObject()
                    object.ActivityID = dict["ActivityID"] as? String ?? ""
                    object.CreatedAt = dict["CreatedAt"] as? String ?? ""
                    object.CreatedBy = dict["CreatedBy"] as? String ?? ""
                    object.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
                    object.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                    object.ActivityImage = dict["ActivityImage"] as? String ?? ""
                 
                    object.IsActive = dict["IsActive"] as? String ?? ""
                    object.Hide = dict["Hide"] as? String ?? ""
                    
                    
                    if SharedManager.getArabic()
                    {
                        
                        object.Title = dict["TitleAr"] as? String ?? ""
                        object.SubTitle = dict["SubTitleAr"] as? String ?? ""
                       
                    }
                    else
                    {
                       
                        object.Title = dict["Title"] as? String ?? ""
                        object.SubTitle = dict["SubTitle"] as? String ?? ""
                        
                        
                    
                    }
                  
                    
                    array.append(object)
                }
                
            }
        }
        return array
        
    }

}
