//
//  UserObject.swift
//  FybuCare
//
//  Created by Mac on 24/02/2020.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class UserObject: NSObject {
    
    var AppStatus: String = ""
    var AuthToken: String = ""
    var CityTitle: String = ""
    var CountryTitle: String = ""
    var title: String = ""
    
    var CityID: String = ""
    var CompanyName: String = ""
    var CompressedCoverImage: String = ""
    var CoverImage: String = ""
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var DeviceToken: String = ""
    var DeviceType: String = ""
    var Dob: String = ""
    var Email: String = ""
    var FullName: String = ""
    var Bio : String = ""
    var TotalFollower: String = "0"
    var TotalFollowing : String = "0"
    var IsFollowing : Int = 0
    
    
    
    var Gender: String = ""
    var Hide: String = ""
    var HideContactNo: String = ""
    var IDNumber: String = ""
    var Image: String = ""
    var IsActive: String = ""
    var IsEmailVerified: String = ""
    var IsMobileVerified: String = ""
    var IsProfileCustomized: String = ""
    var LastLangState: String = ""
    var LastState: String = ""
    
    
    var Lat: String = ""
    var Long: String = ""
    var MerchantName: String = ""
    var Mobile: String = ""
    var Notification: String = ""
    var OS: String = ""
    var OTP: String = ""
    var OnlineStatus: String = ""
    var PaymentAccountBankBranch: String = ""
    var PaymentAccountHolderName: String = ""
    var PaymentAccountNumber: String = ""
    
    
    var Points: String = ""
    var RoleID: String = ""
    var SortOrder: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    var UserID: String = ""
    var UserName: String = ""
    var Verification: String = ""
    
    
    var Distance: String = ""
    var Rating: String = ""
    var IsPrivate: String = "0"
    var InvitationID: String = "0"
    var IsAppLive: Int = 0
    var IsSocialQr: Int = 0
    
    var CanCreateFreeQr: String = "0"
    
    
    
    class func parseData(userDictionary: NSDictionary) -> UserObject {
        
        let userObj = UserObject()
        
        if let dict = userDictionary["user_info"] as? NSDictionary
        {
            
            
            userObj.AppStatus = dict["AppStatus"] as? String ?? ""
            userObj.AuthToken = dict["AuthToken"] as? String ?? ""
            userObj.CityTitle = dict["CityTitle"] as? String ?? ""
            userObj.CountryTitle = dict["CountryTitle"] as? String ?? ""
            userObj.title = dict["title"] as? String ?? ""
            userObj.CityID = dict["CityID"] as? String ?? ""
            userObj.CompanyName = dict["CompanyName"] as? String ?? ""
            userObj.CompressedCoverImage = dict["CompressedCoverImage"] as? String ?? ""
            userObj.CoverImage = dict["CoverImage"] as? String ?? ""
            userObj.CreatedAt = dict["CreatedAt"] as? String ?? ""
            userObj.CreatedBy = dict["CreatedBy"] as? String ?? ""
            userObj.DeviceToken = dict["DeviceToken"] as? String ?? ""
            userObj.DeviceType = dict["DeviceType"] as? String ?? ""
            userObj.Dob = dict["Dob"] as? String ?? ""
            userObj.Email = dict["Email"] as? String ?? ""
            userObj.FullName = dict["FullName"] as? String ?? ""
            userObj.Bio = dict["Bio"] as? String ?? ""
            userObj.TotalFollower = dict["TotalFollower"] as? String ?? "0"
            userObj.TotalFollowing = dict["TotalFollowing"] as? String ?? "0"
            userObj.IsFollowing = dict["IsFollowing"] as? Int ?? 0
            
            userObj.Gender = dict["Gender"] as? String ?? ""
            userObj.Hide = dict["Hide"] as? String ?? ""
            userObj.HideContactNo = dict["HideContactNo"] as? String ?? ""
            userObj.IDNumber = dict["IDNumber"] as? String ?? ""
            userObj.Image = dict["Image"] as? String ?? ""
            userObj.IsActive = dict["IsActive"] as? String ?? ""
            userObj.IsEmailVerified = dict["IsEmailVerified"] as? String ?? ""
            userObj.IsMobileVerified = dict["IsMobileVerified"] as? String ?? ""
            userObj.IsProfileCustomized = dict["IsProfileCustomized"] as? String ?? ""
            userObj.LastLangState = dict["LastLangState"] as? String ?? ""
            userObj.LastState = dict["LastState"] as? String ?? ""
            userObj.Lat = dict["Lat"] as? String ?? ""
            userObj.Long = dict["Long"] as? String ?? ""
            userObj.MerchantName = dict["MerchantName"] as? String ?? ""
            userObj.Mobile = dict["Mobile"] as? String ?? ""
            userObj.Notification = dict["Notification"] as? String ?? ""
            userObj.OS = dict["OS"] as? String ?? ""
            userObj.OTP = dict["OTP"] as? String ?? ""
            userObj.OnlineStatus = dict["OnlineStatus"] as? String ?? ""
            userObj.PaymentAccountBankBranch = dict["PaymentAccountBankBranch"] as? String ?? ""
            userObj.PaymentAccountHolderName = dict["PaymentAccountHolderName"] as? String ?? ""
            userObj.PaymentAccountNumber = dict["PaymentAccountNumber"] as? String ?? ""
            
            userObj.Points = dict["Points"] as? String ?? ""
            userObj.RoleID = dict["RoleID"] as? String ?? ""
            userObj.SortOrder = dict["SortOrder"] as? String ?? ""
            userObj.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
            userObj.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
            userObj.UserID = dict["UserID"] as? String ?? ""
            userObj.UserName = dict["UserName"] as? String ?? ""
            userObj.Verification = dict["Verification"] as? String ?? ""
            
            
            userObj.Distance = dict["Distance"] as? String ?? "0"
            userObj.Rating = dict["Rating"] as? String ?? "0"
            userObj.IsPrivate = dict["IsPrivate"] as? String ?? "0"
            userObj.IsSocialQr = dict["IsSocialQr"] as? Int ?? 0
            userObj.InvitationID = dict["InvitationID"] as? String ?? "0"
            userObj.IsAppLive = dict["IsAppLive"] as? Int ?? 0
            userObj.CanCreateFreeQr = dict["CanCreateFreeQr"] as? String ?? "0"
            
        }
        
        
        return userObj
        
    }
    
    
    
    class func parseMultipleUserData(array: NSArray) -> [UserObject] {
        
        
        var tempArray = [UserObject]()
        
        
        for item in array
        {
            if let dict = item as? NSDictionary
            {
                let userObj = UserObject()
                userObj.AppStatus = dict["AppStatus"] as? String ?? ""
                userObj.AuthToken = dict["AuthToken"] as? String ?? ""
                userObj.CityTitle = dict["CityTitle"] as? String ?? ""
                userObj.CountryTitle = dict["CountryTitle"] as? String ?? ""
                userObj.title = dict["title"] as? String ?? ""
                userObj.CityID = dict["CityID"] as? String ?? ""
                userObj.CompanyName = dict["CompanyName"] as? String ?? ""
                userObj.CompressedCoverImage = dict["CompressedCoverImage"] as? String ?? ""
                userObj.CoverImage = dict["CoverImage"] as? String ?? ""
                userObj.CreatedAt = dict["CreatedAt"] as? String ?? ""
                userObj.CreatedBy = dict["CreatedBy"] as? String ?? ""
                userObj.DeviceToken = dict["DeviceToken"] as? String ?? ""
                userObj.DeviceType = dict["DeviceType"] as? String ?? ""
                userObj.Dob = dict["Dob"] as? String ?? ""
                userObj.Email = dict["Email"] as? String ?? ""
                userObj.FullName = dict["FullName"] as? String ?? ""
                userObj.Bio = dict["Bio"] as? String ?? ""
                userObj.TotalFollower = dict["TotalFollower"] as? String ?? "0"
                userObj.TotalFollowing = dict["TotalFollowing"] as? String ?? "0"
                userObj.IsFollowing = dict["IsFollowing"] as? Int ?? 0
                
                userObj.Gender = dict["Gender"] as? String ?? ""
                userObj.Hide = dict["Hide"] as? String ?? ""
                userObj.HideContactNo = dict["HideContactNo"] as? String ?? ""
                userObj.IDNumber = dict["IDNumber"] as? String ?? ""
                userObj.Image = dict["Image"] as? String ?? ""
                userObj.IsActive = dict["IsActive"] as? String ?? ""
                userObj.IsEmailVerified = dict["IsEmailVerified"] as? String ?? ""
                userObj.IsMobileVerified = dict["IsMobileVerified"] as? String ?? ""
                userObj.IsProfileCustomized = dict["IsProfileCustomized"] as? String ?? ""
                userObj.LastLangState = dict["LastLangState"] as? String ?? ""
                userObj.LastState = dict["LastState"] as? String ?? ""
                userObj.Lat = dict["Lat"] as? String ?? ""
                userObj.Long = dict["Long"] as? String ?? ""
                userObj.MerchantName = dict["MerchantName"] as? String ?? ""
                userObj.Mobile = dict["Mobile"] as? String ?? ""
                userObj.Notification = dict["Notification"] as? String ?? ""
                userObj.OS = dict["OS"] as? String ?? ""
                userObj.OTP = dict["OTP"] as? String ?? ""
                userObj.OnlineStatus = dict["OnlineStatus"] as? String ?? ""
                userObj.PaymentAccountBankBranch = dict["PaymentAccountBankBranch"] as? String ?? ""
                userObj.PaymentAccountHolderName = dict["PaymentAccountHolderName"] as? String ?? ""
                userObj.PaymentAccountNumber = dict["PaymentAccountNumber"] as? String ?? ""
                
                userObj.Points = dict["Points"] as? String ?? ""
                userObj.RoleID = dict["RoleID"] as? String ?? ""
                userObj.SortOrder = dict["SortOrder"] as? String ?? ""
                userObj.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
                userObj.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                userObj.UserID = dict["UserID"] as? String ?? ""
                userObj.UserName = dict["UserName"] as? String ?? ""
                userObj.Verification = dict["Verification"] as? String ?? ""
                
                
                userObj.Distance = dict["Distance"] as? String ?? "0"
                userObj.Rating = dict["Rating"] as? String ?? "0"
                userObj.IsPrivate = dict["IsPrivate"] as? String ?? "0"
                userObj.IsSocialQr = dict["IsSocialQr"] as? Int ?? 0
                userObj.InvitationID = dict["InvitationID"] as? String ?? "0"
                
                
                if userObj.UserName != ""
                {
                    tempArray.append(userObj)
                }
              
            }
        }
        
        
        
        
        
        
        
        
        
        
        return tempArray
        
    }
    
    
    
 
    
    
    
}


