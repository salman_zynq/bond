//
//  InvitationObject.swift
//  Bond
//
//  Created by Muhammad Salman on 25/03/2021.
//

import UIKit

class InvitationObject: NSObject {
    
    var Address : String = ""
    var AndroidAppLink : String = ""
    var AudioLink : String = ""
    var Background : String = ""
    var Bio : String = ""
    var Birthday : String = ""
    var Company : String = ""
    var ContactInfo : String = ""
    var CreatedAt : String = ""
    var CreatedBy : String = ""
    var InvitationDescription : String = ""
   
    
    var WhatsaAppNumber : String = ""
    var Email : String = ""
    var FacebookLink : String = ""
    var  FirstName : String = ""
    var FullName : String = ""
    var GoogleLink : String = ""
    var Hide : String = ""
    var HostName : String = ""
    var InstagramLink : String = ""
    var InvitationID : String = ""
    var InvitationPassword : String = ""
    var IosAppLink : String = ""
    var IsActive : String = ""
    var IsBackgroundColor : String = ""
    var IsBackgroundImage : String = ""
    var IsPrivate : String = ""
    var LastName : String = ""
    var LinkedinLink : String = ""
    var LocationLink : String = ""
    var Logo : String = ""
    var  LogoImage : String = ""
    var PrivateCharges : String = ""
    var QRID : String = ""
    var QRUrl: String = ""
    var SnapchatLink : String = ""
    var SortOrder : String = ""
    var TextColor : String = ""
    var TiktokLink : String = ""
    var Title : String = ""
    
    var TotalAmount : String = ""
    var TwitterLink : String = ""
    var InvitationType : String = ""
    var UpdatedAt : String = ""
    var UpdatedBy : String = ""
    var  UserID : String = ""
    var UserName: String = ""
    var Website: String = ""
    var YoutubeLink: String = ""
    var QrImage: String = ""

   
    var InvoiceID: String = ""
    var UpgradePrice : String = ""
    
    var Title1 : String = ""
    var Title2 : String = ""
    var  Title3 : String = ""
   
    var LinkDescription1: String = ""
    var LinkDescription2: String = ""
    var LinkDescription3: String = ""
    
    
    class func parseSingleData(userDictionary: NSDictionary) -> InvitationObject {
        
        let obj = InvitationObject()
        
        if let dict = userDictionary["invitation_data"] as? NSDictionary
        {
           
            
            obj.Address = dict["Address"] as? String ?? ""
            obj.AndroidAppLink = dict["AndroidAppLink"] as? String ?? ""
            obj.AudioLink = dict["AudioLink"] as? String ?? ""
            obj.Background = dict["Background"] as? String ?? ""
            obj.Bio = dict["Bio"] as? String ?? ""
            obj.Birthday = dict["Birthday"] as? String ?? ""
            obj.Company = dict["Company"] as? String ?? ""
            obj.ContactInfo = dict["ContactInfo"] as? String ?? ""
            obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
            
            
            obj.Email = dict["Email"] as? String ?? ""
            obj.WhatsaAppNumber = dict["WhatsappNo"] as? String ?? ""
            obj.FacebookLink = dict["FacebookLink"] as? String ?? ""
            obj.FirstName = dict["FirstName"] as? String ?? ""
            obj.FullName = dict["FullName"] as? String ?? ""
            obj.GoogleLink = dict["GoogleLink"] as? String ?? ""
            obj.Hide = dict["Hide"] as? String ?? ""
            obj.HostName = dict["HostName"] as? String ?? ""
            obj.InstagramLink = dict["InstagramLink"] as? String ?? ""
            obj.InvitationID = dict["InvitationID"] as? String ?? ""
            obj.InvitationPassword = dict["InvitationPassword"] as? String ?? ""
            
            obj.IosAppLink = dict["IosAppLink"] as? String ?? ""
            obj.IsActive = dict["IsActive"] as? String ?? ""
            obj.IsBackgroundColor = dict["IsBackgroundColor"] as? String ?? ""
            
            obj.CreatedBy = dict["CreatedBy"] as? String ?? ""
            obj.IsBackgroundImage = dict["IsBackgroundImage"] as? String ?? ""
            obj.IsPrivate = dict["IsPrivate"] as? String ?? ""
            
            obj.LastName = dict["LastName"] as? String ?? ""
            obj.LinkedinLink = dict["LinkedinLink"] as? String ?? ""
            obj.LocationLink = dict["LocationLink"] as? String ?? ""
            obj.Logo = dict["Logo"] as? String ?? ""
            obj.LogoImage = dict["LogoImage"] as? String ?? ""
            obj.PrivateCharges = dict["PrivateCharges"] as? String ?? ""
            obj.QRID = dict["QRID"] as? String ?? ""
            obj.QRUrl = dict["QRUrl"] as? String ?? ""
            obj.SnapchatLink = dict["SnapchatLink"] as? String ?? ""
            obj.SortOrder = dict["SortOrder"] as? String ?? ""
            obj.TextColor = dict["TextColor"] as? String ?? ""
            obj.TiktokLink = dict["TiktokLink"] as? String ?? ""
            
            obj.TotalAmount = dict["TotalAmount"] as? String ?? ""
            obj.TwitterLink = dict["TwitterLink"] as? String ?? ""
            obj.InvitationType = dict["Type"] as? String ?? ""
            obj.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
            obj.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
            obj.UserID = dict["UserID"] as? String ?? ""
            obj.UserName = dict["UserName"] as? String ?? ""
            
            obj.Website = dict["Website"] as? String ?? ""
            obj.YoutubeLink = dict["YoutubeLink"] as? String ?? ""
            obj.QrImage = dict["QrImage"] as? String ?? ""
            obj.InvoiceID = dict["InvoiceID"] as? String ?? ""
            
            obj.UpgradePrice = dict["UpgradePrice"] as? String ?? "0"
            
            
            obj.Title1 = dict["Title1"] as? String ?? ""
            obj.Title2 = dict["Title2"] as? String ?? ""
            obj.Title3 = dict["Title3"] as? String ?? ""
            
            obj.LinkDescription1 = dict["Description1"] as? String ?? ""
            obj.LinkDescription2 = dict["Description2"] as? String ?? ""
            obj.LinkDescription3 = dict["Description3"] as? String ?? ""
         
            if SharedManager.getArabic()
            {
                obj.Title = dict["Title"] as? String ?? ""
                obj.InvitationDescription = dict["Description"] as? String ?? ""
            }
            else
            {
                obj.Title = dict["Title"] as? String ?? ""
                obj.InvitationDescription = dict["Description"] as? String ?? ""
            }

        }
        
        
        return obj
        
    }
    
    
    
    
    class func parseMultipleData(userDictionary: NSDictionary) -> [InvitationObject] {
        
        
        
        var tempArray = [InvitationObject]()
       
        
        if let array = userDictionary["invitation_data"] as? NSArray
        {
            for item in array
            {
                if let dict = item as? NSDictionary
                {
                    let obj = InvitationObject()
                    obj.Address = dict["Address"] as? String ?? ""
                    obj.AndroidAppLink = dict["AndroidAppLink"] as? String ?? ""
                    obj.AudioLink = dict["AudioLink"] as? String ?? ""
                    obj.Background = dict["Background"] as? String ?? ""
                    obj.Bio = dict["Bio"] as? String ?? ""
                    obj.Birthday = dict["Birthday"] as? String ?? ""
                    obj.Company = dict["Company"] as? String ?? ""
                    obj.ContactInfo = dict["ContactInfo"] as? String ?? ""
                    obj.CreatedAt = dict["CreatedAt"] as? String ?? ""
                    
                    
                    obj.Email = dict["Email"] as? String ?? ""
                    obj.WhatsaAppNumber = dict["WhatsappNo"] as? String ?? ""
                    obj.FacebookLink = dict["FacebookLink"] as? String ?? ""
                    obj.FirstName = dict["FirstName"] as? String ?? ""
                    obj.FullName = dict["FullName"] as? String ?? ""
                    obj.GoogleLink = dict["GoogleLink"] as? String ?? ""
                    obj.Hide = dict["Hide"] as? String ?? ""
                    obj.HostName = dict["HostName"] as? String ?? ""
                    obj.InstagramLink = dict["InstagramLink"] as? String ?? ""
                    obj.InvitationID = dict["InvitationID"] as? String ?? ""
                    obj.InvitationPassword = dict["InvitationPassword"] as? String ?? ""
                    
                    obj.IosAppLink = dict["IosAppLink"] as? String ?? ""
                    obj.IsActive = dict["IsActive"] as? String ?? ""
                    obj.IsBackgroundColor = dict["IsBackgroundColor"] as? String ?? ""
                    
                    obj.CreatedBy = dict["CreatedBy"] as? String ?? ""
                    obj.IsBackgroundImage = dict["IsBackgroundImage"] as? String ?? ""
                    obj.IsPrivate = dict["IsPrivate"] as? String ?? ""
                    
                    obj.LastName = dict["LastName"] as? String ?? ""
                    obj.LinkedinLink = dict["LinkedinLink"] as? String ?? ""
                    obj.LocationLink = dict["LocationLink"] as? String ?? ""
                    obj.Logo = dict["Image"] as? String ?? ""
                    obj.LogoImage = dict["LogoImage"] as? String ?? ""
                    obj.PrivateCharges = dict["PrivateCharges"] as? String ?? ""
                    obj.QRID = dict["QRID"] as? String ?? ""
                    obj.QRUrl = dict["QRUrl"] as? String ?? ""
                    obj.SnapchatLink = dict["SnapchatLink"] as? String ?? ""
                    obj.SortOrder = dict["SortOrder"] as? String ?? ""
                    obj.TextColor = dict["TextColor"] as? String ?? ""
                    obj.TiktokLink = dict["TiktokLink"] as? String ?? ""
                    
                    obj.TotalAmount = dict["TotalAmount"] as? String ?? ""
                    obj.TwitterLink = dict["TwitterLink"] as? String ?? ""
                    obj.InvitationType = dict["Type"] as? String ?? ""
                    obj.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
                    obj.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                    obj.UserID = dict["UserID"] as? String ?? ""
                    obj.UserName = dict["UserName"] as? String ?? ""
                    
                    obj.Website = dict["Website"] as? String ?? ""
                    obj.YoutubeLink = dict["YoutubeLink"] as? String ?? ""
                    obj.QrImage = dict["QrImage"] as? String ?? ""
                    obj.InvoiceID = dict["InvoiceID"] as? String ?? ""
                    
                    obj.UpgradePrice = dict["UpgradePrice"] as? String ?? "0"
                    
                    
                    obj.Title1 = dict["Title1"] as? String ?? ""
                    obj.Title2 = dict["Title2"] as? String ?? ""
                    obj.Title3 = dict["Title3"] as? String ?? ""
                    
                    obj.LinkDescription1 = dict["Description1"] as? String ?? ""
                    obj.LinkDescription2 = dict["Description2"] as? String ?? ""
                    obj.LinkDescription3 = dict["Description3"] as? String ?? ""
                    
                    if SharedManager.getArabic()
                    {
                        obj.Title = dict["Title"] as? String ?? ""
                        obj.InvitationDescription = dict["Description"] as? String ?? ""
                    }
                    else
                    {
                        obj.Title = dict["Title"] as? String ?? ""
                        obj.InvitationDescription = dict["Description"] as? String ?? ""
                    }
                    
                    
                    tempArray.append(obj)
                }
             
            }
            
         
            
           
          
        }
        
        
        return tempArray
        
    }
    
}
