//
//  MessageObject.swift
//  Boaty_Merchant
//
//  Created by Muhammad Salman on 15/04/2021.
//

import UIKit

class MessageObject: NSObject {
    
    
    var ChatID: String = ""
    var CompressedImage: String = ""
    var ChatMessageID: String = ""
    var CreatedAt: String = ""
    var CompanyName: String = ""
    
    var Image: String = ""
    var IsReadByReceiver: String = ""
    var IsReadBySender: String = ""
    var Message: String = ""
    var ReceiverID: String = ""
    var SenderID: String = ""
    
    
    
    var ConversationReceiverID: String = ""
    var ConversationReceiverImage: String = ""
    var ConversationReceiverName: String = ""
    var ConversationSenderID: String = ""
    var ConversationSenderImage: String = ""
    var ConversationSenderName: String = ""
    
    
    var unreadMessageCount: Int = 0
    
    class func parseData(response: NSDictionary) -> [MessageObject] {
        
        
        
        var messageArray = [MessageObject]()
        if let array = response["ChatMessages"] as? NSArray
        {
            for item in array
            {
                if let dict = item as? NSDictionary
                {
                    let messageObject = MessageObject()
                    messageObject.ChatID = dict["ChatID"] as? String ?? ""
                    messageObject.ChatMessageID = dict["ChatMessageID"] as? String ?? ""
                    
                    messageObject.CompressedImage = dict["CompressedImage"] as? String ?? ""
                    messageObject.CreatedAt = dict["CreatedAt"] as? String ?? ""
                    messageObject.Image = dict["SenderImage"] as? String ?? ""
                    messageObject.IsReadByReceiver = dict["IsReadByReceiver"] as? String ?? ""
                    messageObject.IsReadBySender = dict["IsReadBySender"] as? String ?? ""
                    messageObject.Message = dict["Message"] as? String ?? ""
                    messageObject.ReceiverID = dict["ReceiverID"] as? String ?? ""
                    messageObject.SenderID = dict["SenderID"] as? String ?? ""
                    
                    messageArray.append(messageObject)
                }
                
            }
        }
        return messageArray
    }
    
    
    class func parseSingleData(response: NSDictionary) -> MessageObject {
        
        
        
        let messageObject = MessageObject()
        if let dict = response["ChatMessages"] as? NSDictionary
        {
            
            
            
            messageObject.ChatID = dict["ChatID"] as? String ?? ""
            messageObject.ChatMessageID = dict["ChatMessageID"] as? String ?? ""
            
            messageObject.CompressedImage = dict["CompressedImage"] as? String ?? ""
            messageObject.CreatedAt = dict["CreatedAt"] as? String ?? ""
            messageObject.Image = dict["SenderImage"] as? String ?? ""
            messageObject.IsReadByReceiver = dict["IsReadByReceiver"] as? String ?? ""
            messageObject.IsReadBySender = dict["IsReadBySender"] as? String ?? ""
            messageObject.Message = dict["Message"] as? String ?? ""
            messageObject.ReceiverID = dict["ReceiverID"] as? String ?? ""
            messageObject.SenderID = dict["SenderID"] as? String ?? ""
            
            
            
            
            
        }
        return messageObject
    }
    
    
    class func chatRoomsData(response: NSDictionary) -> [MessageObject] {
        
        
        
        var messageArray = [MessageObject]()
        if let array = response["ChatRooms"] as? NSArray
        {
            for item in array
            {
                if let dict = item as? NSDictionary
                {
                    let messageObject = MessageObject()
                    messageObject.ChatID = dict["ChatID"] as? String ?? ""
                    messageObject.ChatMessageID = dict["ChatMessageID"] as? String ?? ""
                 
                    messageObject.CreatedAt = dict["CreatedAt"] as? String ?? ""
                 
                    messageObject.Message = dict["Message"] as? String ?? ""
                    
                    messageObject.ConversationSenderID = dict["ConversationSenderID"] as? String ?? ""
                 
                    messageObject.ConversationSenderName = dict["ConversationSenderName"] as? String ?? ""
                 
                    messageObject.ConversationSenderImage = dict["ConversationSenderImage"] as? String ?? ""
                    
                    
                    messageObject.ConversationReceiverID = dict["ConversationReceiverID"] as? String ?? ""
                 
                    messageObject.ConversationReceiverName = dict["ConversationReceiverName"] as? String ?? ""
                 
                    messageObject.ConversationReceiverImage = dict["ConversationReceiverImage"] as? String ?? ""
                    
                    messageObject.unreadMessageCount = dict["UnreadMessageCount"] as? Int ?? 0
                    
                    if messageObject.ChatMessageID != ""
                    {
                        messageArray.append(messageObject)
                    }
                    
                   
                }
                
            }
        }
        return messageArray
    }
}

