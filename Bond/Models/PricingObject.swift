//
//  PricingObject.swift
//  Bond
//
//  Created by Muhammad Salman on 02/04/2021.
//

import UIKit

class PricingObject: NSObject {
    
    
    var CreatedAt: String = ""
    var CreatedBy: String = ""
    var IsPaid: Int = 0
    var IsFree: Int = 0
    var PriceID: String = ""
    var PriceType: String = ""
    
    var PrivateCharges: String = ""
    var QrType: Int = 0
    var Price: String = ""
    var Title: String = ""
    var UpdatedAt: String = ""
    var UpdatedBy: String = ""
    var isHideFreeOption : Bool = false
    
    var freeOptionArray = [PricingObject]()
    var premiumOptionArray = [PricingObject]()
    
    
    class func parseData(response: NSDictionary) -> [PricingObject] {
        
        var tempArray = [PricingObject]()
     
        
        if let pricesArray = response["prices"] as? NSArray
        {
            
            for item in pricesArray
            {
                if let dict = item as? NSDictionary
                {
                    let object = PricingObject()
                    object.CreatedAt = dict["CreatedAt"] as? String ?? ""
                    object.CreatedBy = dict["CreatedBy"] as? String ?? ""
                 
                    object.PriceID = dict["PriceID"] as? String ?? ""
                    object.UpdatedAt = dict["UpdatedAt"] as? String ?? ""
                    object.PriceType = dict["PriceType"] as? String ?? ""
                    object.UpdatedBy = dict["UpdatedBy"] as? String ?? ""
                    
                    object.Price = dict["Price"] as? String ?? ""
                    object.PrivateCharges = dict["PrivateCharges"] as? String ?? ""
                    
                    if let qrType =  dict["QrType"] as? String
                    {
                        object.QrType = Int(qrType) ?? 0
                    }
                    else
                    {
                        object.QrType = dict["QrType"] as? Int ?? 0
                    }
                  
                    
                    print(object.QrType)
                    
                    
                    if SharedManager.getArabic()
                    {
                        object.Title = dict["TitleAr"] as? String ?? ""
                    }
                    else
                    {
                        object.Title = dict["Title"] as? String ?? ""
                    }
                    
                    if let freeOptionArray = dict["FreeOption"] as? NSArray
                    {
                        for freeItem in freeOptionArray
                        {
                            if let freeDict = freeItem as? NSDictionary
                            {
                                let freeObject = PricingObject()
                                freeObject.IsFree = freeDict["Value"] as? Int ?? 0
                               
                                if SharedManager.getArabic()
                                {
                                    freeObject.Title = freeDict["TitleAr"] as? String ?? ""
                                }
                                else
                                {
                                    freeObject.Title = freeDict["Title"] as? String ?? ""
                                }
                                
                               
                                
                                object.freeOptionArray.append(freeObject)
                            }
                            
                            
                            
                        }
                    }
                    
                    if let premiumOptionArray = dict["PremiumOption"] as? NSArray
                    {
                        for premiumItem in premiumOptionArray
                        {
                            if let premiumDict = premiumItem as? NSDictionary
                            {
                                let premiumObject = PricingObject()
                                
                                premiumObject.IsPaid = premiumDict["Value"] as? Int ?? 0
                               
                                if SharedManager.getArabic()
                                {
                                    premiumObject.Title = premiumDict["TitleAr"] as? String ?? ""
                                }
                                else
                                {
                                    premiumObject.Title = premiumDict["Title"] as? String ?? ""
                                }
                                
                                object.premiumOptionArray.append(premiumObject)
                            }
                        }
                    }
                    
                    var isNoFree = false
                    for item in object.freeOptionArray
                    {
                        
                        if item.IsFree == 1
                        {
                            isNoFree = true
                        }
                        
                    }
                    
                    
                    if isNoFree == false
                    {
                        if object.QrType == 1 || object.QrType == 3
                        {
                            object.isHideFreeOption = true
                        }
                       
                    }
                    
                    tempArray.append(object)
                }
                
                
            }
            
            
            
            
            
        }
        return tempArray
        
    }
}
