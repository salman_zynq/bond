//
//  CameraHandler.swift
//
//  Created by Muhammad Salman on  16/03/2020.
//  Copyright © 2020 Muhammad Salman. All rights reserved.
//

import Foundation
import UIKit
import YPImagePicker
import Photos

class CameraHandler: NSObject
{
    static let shared = CameraHandler()
    
    var currentVC = UIViewController()
    
    //MARK: Internal Properties
    
    var imagePickedBlock: ((String) -> Void)?
    
    
      // MARK: - Configuration
    func selectProfileImage(viewController:UIViewController ,  imageSeletionCallBack : @escaping (_ image:UIImage , _ imageUrl:URL?) -> Void) {
            
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined || photos == .authorized {
                PHPhotoLibrary.requestAuthorization({ status in
                    if status == .authorized {
                        
                        print("access granted")
                        
                        
                      
                        
                        var config = YPImagePickerConfiguration()
                        config.library.mediaType = .photo
                        config.shouldSaveNewPicturesToAlbum = false
                        config.startOnScreen = .library
                        config.screens = [.library, .photo]
                        config.wordings.libraryTitle = SELECT_ALBUM.localized()
                        config.hidesStatusBar = false
                        config.showsPhotoFilters = false
                        config.showsCrop = .none
                       // config.onlySquareImagesFromCamera = true
                        
                        DispatchQueue.main.async {
                            
                            let picker = YPImagePicker(configuration: config)
                            
                            
                            picker.didFinishPicking { [unowned picker] items, _ in
                                
                                
                                picker.dismiss(animated: false, completion: nil)
                                if items.count > 0
                                {
                                    
                                    
                                    
                                    
                                   
                                    imageSeletionCallBack(items.singlePhoto!.image , items.singlePhoto?.url)
                                 
                                    
                                }
                                
                                
                            }
                            
                            viewController.present(picker, animated: true, completion: nil)
                            
                        }
                        
                    } else {
                        
                        print("access denied")
                    }
                })
            } else {
    //            AlertManager.showAlertWithTwoButtons(title: "Error".localized(), message: "Allow Booth to access your photos or camera".localized(), firstButtonTitle: "Settings".localized(), successCallBack: {
    //
    //                if let url = NSURL(string: UIApplication.openSettingsURLString) as URL? {
    //                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
    //                }
    //
    //
    //            }) {
    //
    //                print("2")
    //
    //            }
            }
            
        }
    

    func presentCamera(for view: UIViewController)
    {
        currentVC = view
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
             UINavigationBar.appearance().tintColor = .black
            currentVC.present(myPickerController, animated: true, completion: nil)
        }

    }

    func presentPhotoLibrary(for view: UIViewController)
    {
        currentVC = view
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            UINavigationBar.appearance().tintColor = .black
            currentVC.present(myPickerController, animated: true, completion: nil)
        }

    }

    func showActionSheet(vc: UIViewController) {
        currentVC = vc
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)


        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.presentPhotoLibrary(for: vc)
        }))

        actionSheet.addAction(UIAlertAction(title: CLOSE.localized(), style: .cancel, handler: nil))

        vc.present(actionSheet, animated: true, completion: nil)
    }
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        UINavigationBar.appearance().tintColor = .white
        currentVC.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {


        switch picker.sourceType {

        case .photoLibrary:
            if #available(iOS 11.0, *) {
                self.imagePickedBlock?("\(String(describing: info[UIImagePickerController.InfoKey.imageURL]!))")
            } else {

            }
        case .camera:
            guard let image = info[.originalImage] as? UIImage else {
                          print("No image found")
                          return
                      }
                      saveImage(image: image) { (error) in
                          print("saveImage not found")
                      }
        case .savedPhotosAlbum:
            if #available(iOS 11.0, *) {
                self.imagePickedBlock?("\(String(describing: info[UIImagePickerController.InfoKey.imageURL]!))")
            } else {}
        @unknown default:
            break
        }
         UINavigationBar.appearance().tintColor = .white
        currentVC.dismiss(animated: true, completion: nil)
    }




    func saveImage(image: UIImage, completion: @escaping (Error?) -> ()) {
       UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(path:didFinishSavingWithError:contextInfo:)), nil)
    }

    @objc private func image(path: String, didFinishSavingWithError error: NSError?, contextInfo: UnsafeMutableRawPointer?) {
        debugPrint(path) // That's the path you want
    }

}

