 //
//  PaymentMethodCollectionViewCell.swift
//  MFSDKDemo-Swift
//
//  Created by Elsayed Hussein on 8/29/19.
//  Copyright © 2019 Elsayed Hussein. All rights reserved.
//

import UIKit
import MFSDK

class PaymentMethodCollectionViewCell: UICollectionViewCell {
    //MARK: Variables
    
    //MARK: Outlets
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var paymentMethodImageView: UIImageView!
    @IBOutlet weak var paymentMethodNameLabel: LabelBoldFont!
    
    //MARK Methods
    func configure(paymentMethod: MFPaymentMethod, selected: Bool) {
        paymentMethodImageView.image = nil
        selectionView.layer.cornerRadius = 5
        if selected {
            selectionView.layer.borderColor = UIColor.lightGray.cgColor
            selectionView.layer.borderWidth = 1
        } else {
            selectionView.layer.borderColor = UIColor.lightGray.cgColor
            selectionView.layer.borderWidth = 0
        }
        if let imageURL = paymentMethod.imageUrl {
            paymentMethodImageView.downLoadImageIntoImageView(url: imageURL, placeholder: nil , isQRImage:  true)
            
        }
        paymentMethodNameLabel.text = paymentMethod.paymentMethodEn ?? ""
    }
}
