//
//  BackgroundImageCollectionViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 28/04/2021.
//

import UIKit

class BackgroundImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var mainContainer: UIView!
}
