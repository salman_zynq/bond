//
//  NotificationTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    

    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var containerImageView: UIView!
    
    @IBOutlet weak var viewReadUnRead: UIView!
    @IBOutlet weak var labelCreatedAt: LabelRegularFont!
    @IBOutlet weak var labelNotificationText: LabelBoldFont!
    @IBOutlet weak var labelUserName: LabelMediumFont!
    @IBOutlet weak var labelFullName: LabelBoldFont!
    @IBOutlet weak var userImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
