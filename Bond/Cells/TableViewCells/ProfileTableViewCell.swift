//
//  ProfileTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var topContainer: UIView!
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var containerImageView: UIView!
    
    @IBOutlet weak var labelFullName: LabelBoldFont!
    @IBOutlet weak var labelUserName: LabelRegularFont!
    
    
    @IBOutlet weak var buttonStack: UIStackView!
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var viewFollow: ButtonView!
    
    @IBOutlet weak var buttonFollow: ButtonBold!
    
    @IBOutlet weak var viewChatorEdit: ButtonView!
    
    @IBOutlet weak var buttonChatOrEdit: ButtonBold!
    
    @IBOutlet weak var labelBio: LabelRegularFont!
    
    @IBOutlet weak var labelFollowings: LabelBoldFont!
    
    @IBOutlet weak var labelFollowingHeading: LabelRegularFont!
    @IBOutlet weak var labelFollowers: LabelBoldFont!
    @IBOutlet weak var labelFollowersHeading: LabelRegularFont!
    
    @IBOutlet weak var labelAboutHeading: LabelBoldFont!
    @IBOutlet weak var bottonContainer: UIView!
    
    @IBOutlet weak var buttonLocation: ButtonRegular!
    
    @IBOutlet weak var instagramBackgroundImageView: UIImageView!
    @IBOutlet weak var viewButtonLocation: ButtonView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
