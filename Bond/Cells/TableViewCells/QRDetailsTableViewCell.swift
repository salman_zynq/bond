//
//  QRDetailsTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 22/04/2021.
//

import UIKit

class QRDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var topCellView: UIView!
    @IBOutlet weak var totalScanCount: LabelRegularFont!
    @IBOutlet weak var qrImageView: UIImageView!
    

    @IBOutlet weak var labelDeviceName: LabelBoldFont!
    @IBOutlet weak var androidScanCount: LabelBoldFont!
    @IBOutlet weak var iOSScanCount: LabelBoldFont!
    
    
    @IBOutlet weak var labelHeader: LabelBoldFont!
    
    
    @IBOutlet weak var bottomCellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
