//
//  PricingTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class PricingTableViewCell: UITableViewCell {
    @IBOutlet weak var labelHeaderTitle: LabelBoldFont!
    
    @IBOutlet weak var headerContainerView: UIView!
    
    @IBOutlet weak var bottomContainerView: UIView!
    
    @IBOutlet weak var imagePaid: UIImageView!
    @IBOutlet weak var imageFree: UIImageView!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    
    
    @IBOutlet weak var labelPaid: LabelBoldFont!
    @IBOutlet weak var labelFree: LabelBoldFont!
    @IBOutlet weak var labelFeatures: LabelBoldFont!
    
    @IBOutlet weak var labelPrice: LabelBoldFont!
    
    @IBOutlet weak var freeView: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
