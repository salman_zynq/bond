//
//  MyQRTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class MyQRTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var labelQRType: LabelRegularFont!
    @IBOutlet weak var labelCreatedAt: LabelMediumFont!
    @IBOutlet weak var labelQRTitle: LabelBoldFont!
    @IBOutlet weak var mainContainer: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
