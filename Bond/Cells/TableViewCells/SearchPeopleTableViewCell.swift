//
//  SearchPeopleTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 17/03/2021.
//

import UIKit

class SearchPeopleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var containerImageView: UIView!
    @IBOutlet weak var labelUserName: LabelMediumFont!
    @IBOutlet weak var labelFullName: LabelBoldFont!
    @IBOutlet weak var userImageView: UIImageView!

    @IBOutlet weak var buttonMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
