//
//  HomeTableViewCell.swift
//  Bond
//
//  Created by Muhammad Salman on 25/03/2021.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var labelHello: LabelBoldFont!
    @IBOutlet weak var mainContainer: UIView!
    
    @IBOutlet weak var labelCreateAQRFor: LabelBoldFont!
    @IBOutlet weak var labelName: LabelBoldFont!
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var labelSubTitle: LabelRegularFont!
    @IBOutlet weak var labelTitle: LabelBoldFont!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
