//
//  TextFieldExtensions.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation


extension UITextField{
    
    func addLeftAndRightView(image:UIImage = UIImage.init(named: "dropDown")!)
    {
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        let dropDownImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        
        let imageView = UIImageView(image: image)
        dropDownImageView.image = imageView.image
        //dropDownImageView.setImageColor(color: UIColor.AppColor())
        
        
        dropDownImageView.contentMode = UIView.ContentMode.scaleAspectFill
        paddingView.addSubview(dropDownImageView)
        paddingView.isUserInteractionEnabled = false
        dropDownImageView.isUserInteractionEnabled = false
        
        self.rightView = paddingView
        self.rightViewMode = .always
        self.leftView = nil
        
        
        self.rightView = paddingView
        self.rightViewMode = .always
        self.leftView = nil
        self.textAlignment = .left
        
    }
    
    func isTextFieldEmpty() -> Bool
    {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty ?? true
    }
    
    // Email Validation.
    func isEmailValid() -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self.text)
        
    }
    
    // Phonenumber Validation.
    
    func isPhoneValid() -> Bool
    {
        
        let PHONE_REGEX = "^[0-9]{6,10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self.text)
        return result
    }
    
    func isValidPassword() -> Bool {
        
        
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self.text)
    }
    
    
 
}



extension UITextView
{
    func isTextViewEmpty() -> Bool
    {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty ?? true
    }
    
}

