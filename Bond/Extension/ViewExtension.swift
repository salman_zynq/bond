//
//  ViewExtension.swift
//  Boaty
//
//  Created by Macbook on 13/01/2021.
//

import UIKit

extension UIView{
    
    
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
    
    
    func roundCornerOfView(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    
    func setBackgroundColorBorderColorRadiusOfView(borderColor : UIColor = .white , borderWidth : CGFloat = 0.0 , cornerRadus : CGFloat = 0.0 , backgroundColor : UIColor = .white)
    {
        self.layer.cornerRadius = cornerRadus
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.backgroundColor = backgroundColor
        self.clipsToBounds = true
    }
    
    func setCornersOfView(cornerRadus : CGFloat = 10)
    {
        self.layer.cornerRadius = cornerRadus
        
        self.clipsToBounds = true
    }
    
    func makeViewRound()
    {
        self.layer.cornerRadius = self.frame.size.height / 2
        
        self.clipsToBounds = true
    }
    
    func setupShadow() {
        
        self.layer.cornerRadius = 10
        self.layer.shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0).cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 7.0
        self.layer.masksToBounds =  false
        //        self.layer.cornerRadius = 10
        //        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        //        self.layer.shadowRadius = 0.1
        //        self.layer.shadowOpacity = 0.1
        //        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 10, height: 10)).cgPath
        //        self.layer.shouldRasterize = true
        //        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    
    func shadowViewWithCustomCornerRadius(cornerRadius:CGFloat = 0)
    {
        self.layer.cornerRadius = cornerRadius
        
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    
    func layerGradient() {
        
        
        let color0 = UIColor(red:64/255, green:93/255, blue:230/255, alpha:1.0).cgColor
        let color1 = UIColor(red:81/255, green:91/255, blue: 212/255, alpha:1.0).cgColor
        let color2 = UIColor(red:129/255, green:52/255, blue: 175/255, alpha:1.0).cgColor
        let color3 = UIColor(red:221/255, green:42/255, blue: 123/255, alpha:1.0).cgColor
        let color4 = UIColor(red:245/255, green:133/255, blue:41/255, alpha:1.0).cgColor
        let color5 = UIColor(red:254/255, green:218/255, blue:119/255, alpha:1.0).cgColor
        
        if let gradientLayer = layer.sublayers?.first as? CAGradientLayer {
            
            gradientLayer.colors = [color0,color1,color2,color3,color4,color5]
        } else {
            
            let layer : CAGradientLayer = CAGradientLayer()
            layer.frame.size = self.frame.size
            // layer.frame.origin = CGPoint(x: 0.0,y: 0.0)
            layer.startPoint = CGPoint(x: 0.0, y: 1.0)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)
            layer.rasterizationScale = 100
            layer.cornerRadius = CGFloat(frame.width / 20)
            
            
            
            
            
            self.layer.insertSublayer(layer, at: 0)
            
        }
        
    }
    
}
