//
//  ApplicationWindowsExtension.swift
//  Bond
//
//  Created by Muhammad Salman on 24/03/2021.
//

import Foundation

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        return controller
    }
    
}

extension UIWindow {
    func topViewController() -> UIViewController? {
     
            
            var top = self.rootViewController
                 while true {
                     if let presented = top?.presentedViewController {
                         top = presented
                     } else if let nav = top as? UINavigationController {
                         top = nav.visibleViewController
                     } else if let tab = top as? UITabBarController {
                         top = tab.selectedViewController
                     } else {
                         break
                     }
                 }
                 return top
        
     
    }
}


