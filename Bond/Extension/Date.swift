//
//  Date.swift
//  Boaty
//
//  Created by Muhammad Salman on 05/03/2021.
//

import Foundation



func converDateIntoFullFormate(date:Date) -> String
{
    

    
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    if isArabicLanguage
    {
        dateFormatter.locale = NSLocale.init(localeIdentifier: "ar") as Locale
    }
    else
    {
        dateFormatter.locale =  NSLocale.init(localeIdentifier: "en") as Locale
    }
    
    dateFormatter.dateStyle = .full
    
    
    return dateFormatter.string(from: date)
}

func convertDateAndTimeForBookingController(bookingDate:Date , bookingTime:Date) -> Int
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let dateStr : String = dateFormatter.string(from: bookingDate)
    
    
    let timeFormater = DateFormatter()
    timeFormater.dateFormat = "HH:mm"
    let timeStr : String = timeFormater.string(from: bookingTime)
    
    
    print(dateStr)
    print(timeStr)
    
    let dateAndTimeString = dateStr + " " + timeStr
    
    let dateAndTimeFormatter = DateFormatter()
    dateAndTimeFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    let bookingDateAndTime : Date = dateAndTimeFormatter.date(from: dateAndTimeString) ?? Date()
    
    return convertDateIntoTimeStamp(date: bookingDateAndTime)

    
   
}


func convertDateIntoDateString(date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    let dateStr : String = dateFormatter.string(from: date)
    return dateStr
}


func convertDateIntoTimeString(date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    let dateStr : String = dateFormatter.string(from: date)
    return dateStr
}



func convertTimeStampExperienceIntoYear(timeStamp:String) -> String
{
     let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYY"
    let dateStr : String = dateFormatter.string(from: timeStampDate as Date)
    return dateStr
}



func getMonthfromDate(date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMMM"
    let dateStr : String = dateFormatter.string(from: date)
    return dateStr
}


func getDayfromDate(date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd"
    let dateStr : String = dateFormatter.string(from: date)
    return dateStr
}




func convertDateOfBirthIntoTimeStamp(dateString:String) -> TimeInterval
{
    let date =  covertDateOfBirthIntoDate(dateString: dateString)
    let timeStamp = date.timeIntervalSince1970
    return timeStamp
    
}

func convertDateIntoTimeStamp(date:Date) -> Int
{
    let timeStamp = date.timeIntervalSince1970
    print(Int(timeStamp))
    return Int(timeStamp)
}

func convertTimeStampToDate(timeStamp: String) -> String {
    
    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    
    
    
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    dateFormatter.dateStyle = .long
    dateFormatter.timeStyle = .none
    
    return dateFormatter.string(from: timeStampDate as Date)
    
}



func converTimeStampIntoDateOfBirth(timeStamp: String) -> String
{
    

    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    if isArabicLanguage
    {
        dateFormatter.locale = NSLocale.init(localeIdentifier: "ar") as Locale
    }
    else
    {
        dateFormatter.locale =  NSLocale.init(localeIdentifier: "en") as Locale
    }
    
    dateFormatter.dateStyle = .full
    
    
    return dateFormatter.string(from: timeStampDate as Date)
}

func converTimeStampIntoAgeNumber(timeStamp: String) -> String
{
    

    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    let calender = Calendar.current
    let ageComponents = calender.dateComponents([.year], from: timeStampDate as Date, to: Date())
    let age = ageComponents.year

    return String(describing:age!)
}

func covertDateOfBirthIntoDate(dateString:String) -> Date
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    let date = dateFormatter.date(from: dateString) ?? Date()
    return date
}


func converDateIntoTime(date:Date) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = .short
    let dateStr : String = dateFormatter.string(from: date)
    return dateStr
}



func convertTimeStampToShortDate(timeStamp: String) -> String {
    
    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    let date = dateFormatter.string(from: timeStampDate as Date)
    return date
    
   
    
}

func convertTimeStampToDateAndTime(timeStamp: String) -> String {
    
    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    //dateFormatter.locale = NSLocale.current
    if isArabicLanguage
    {
        dateFormatter.locale = NSLocale.init(localeIdentifier: "ar") as Locale
    }
    else
    {
        dateFormatter.locale =  NSLocale.init(localeIdentifier: "en") as Locale
    }
    dateFormatter.dateStyle = .full
    dateFormatter.timeStyle = .none
    
    return dateFormatter.string(from: timeStampDate as Date)
    
}


func convertTimeStampToDateAndReturnAsDate(timeStamp: String) -> Date {
    
    let timeStampDate = Date(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    return timeStampDate
    
}






func productTimeAgo(_ timeStamp:String, numericDates:Bool) -> String {
    
    if timeStamp.contains(" ") || (timeStamp == "") {
        return ""
    }
    
    let date = Date(timeIntervalSince1970: TimeInterval(timeStamp)!)
    
    let calendar = Calendar.current
    let now = Date()
    let earliest = (now as NSDate).earlierDate(date)
    let latest = (earliest == now) ? date : now
    let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
    
    if (components.year! >= 2) {
        return "\(components.year!) years ago"
    } else if (components.year! >= 1){
        if (numericDates)
        {
            return "1 year ago"
        } else {
            return "Last year"
        }
    } else if (components.month! >= 2) {
        return "\(components.month!) months ago"
    } else if (components.month! >= 1){
        if (numericDates){
            return "1 month ago"
        } else {
            return "Last month"
        }
    } else if (components.weekOfYear! >= 2) {
        return "\(components.weekOfYear!) weeks ago"
    } else if (components.weekOfYear! >= 1){
        if (numericDates){
            return "1 week ago"
        } else {
            return "Last week"
        }
    } else if (components.day! >= 2) {
        return "\(components.day!) days ago"
    } else if (components.day! >= 1){
        if (numericDates){
            return "1 day ago"
        } else {
            return "Yesterday"
        }
    } else if (components.hour! >= 2) {
        return "\(components.hour!) hours ago"
    } else if (components.hour! >= 1){
        if (numericDates){
            return "1 hour ago"
        } else {
            return "An hour ago"
        }
    } else if (components.minute! >= 2) {
        return "\(components.minute!) mins ago"
    } else if (components.minute! >= 1){
        if (numericDates){
            return "1 min ago"
        } else {
            return "A min ago"
        }
    } else if (components.second! >= 3)
    {
        return "\(components.second!) secs ago"
    } else {
        return "Just now"
    }
    
}





func converTimeStampIntoDate(timeStamp: String) -> String
{
    

    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    if isArabicLanguage
    {
        dateFormatter.locale = NSLocale.init(localeIdentifier: "ar") as Locale
    }
    else
    {
        dateFormatter.locale =  NSLocale.init(localeIdentifier: "en") as Locale
    }
    
    dateFormatter.dateStyle = .medium
    
    
    return dateFormatter.string(from: timeStampDate as Date)
}


func converTimeStampIntoTime(timeStamp: String) -> String
{
    

    let timeStampDate = NSDate(timeIntervalSince1970: TimeInterval(timeStamp) ?? 0.0)
    let dateFormatter = DateFormatter()
    dateFormatter.doesRelativeDateFormatting = true
    if isArabicLanguage
    {
        dateFormatter.locale = NSLocale.init(localeIdentifier: "ar") as Locale
    }
    else
    {
        dateFormatter.locale =  NSLocale.init(localeIdentifier: "en") as Locale
    }
    
    dateFormatter.timeStyle = .medium
    
    
    return dateFormatter.string(from: timeStampDate as Date)
}

