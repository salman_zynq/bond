//
//  ImageExtension.swift
//  Boaty
//
//  Created by Macbook on 07/01/2021.
//

import UIKit
import Kingfisher

extension UIImageView{
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    func downLoadImageIntoImageView(url:String, placeholder:UIImage? , isQRImage : Bool = false)
    {
        if url == ""
        {
            
            return
        }
        
        var finalUrl = ""
        if isQRImage
        {
            finalUrl = url
        }
        else
        {
            finalUrl =  IMAGE_BASE_URL + url
        }
        
        print(finalUrl)
        
        
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        
        //  keepCurrentImageWhileLoading
        //  fromMemoryCacheOrRefresh
        
        let downloadUrl = URL(string: finalUrl )
        
        
        self.kf.indicatorType = .activity
        
        
        var options: KingfisherOptionsInfo = []
        options.append(.transition(.fade(1)))
        options.append(.loadDiskFileSynchronously)
        
        
        self.kf.setImage(with: downloadUrl, placeholder: nil, options:options) { result in
            //            switch result {
            //            case .success(let value):
            //                // The source object which contains information like `url`.
            //                print(value.source)
            //            case .failure(let error):
            //                print(error) // The error happens
            //            }
        }
        
        
    }
    
    
    func downLoadQRImageAndReturnResult(url:String, placeholder:UIImage?  , returnResult: @escaping (UIImage) -> Void , errorResult: @escaping (String) -> Void )
    {
        if url == ""
        {
            
            return
        }
        
        var finalUrl = ""
        
        finalUrl = url
        
        
        print(finalUrl)
        
        
        
        finalUrl = finalUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!
        
        //  keepCurrentImageWhileLoading
        //  fromMemoryCacheOrRefresh
        
        let downloadUrl = URL(string: finalUrl )
        
        
        self.kf.indicatorType = .activity
        
        
        var options: KingfisherOptionsInfo = []
        options.append(.transition(.fade(1)))
        options.append(.loadDiskFileSynchronously)
        
        
        self.kf.setImage(with: downloadUrl, placeholder: nil, options:options) { result in
            switch result {
            case .success(let value):
                print(value.image)

                returnResult(value.image)
                // The source object which contains information like `url`.
                print(value.source)
            case .failure(let error):
                print(error) // The error happens
                errorResult(error.localizedDescription)
            }
        }
        
        
    }
}


extension UIImage {
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
